--查询字典所有信息
#sql("test")
  SELECT * 
  FROM SYS_MENU 
#end


#sql("test.meta3")
  {
    countSqlSuffix: "countSql",
    rowId:"id",
    colDatas:[
        {field:"menu_name",head:"名称",width:"15%"},
        {field:"comments",head:"描述",width:"25%"},
        {field:"status",head:"状态",width:"10%",filterType:"select",dictCode:"PUB_ENABLE_DISABLE"},
        {field:"menu_url",head:"URL",width:"35%"},
        {field:"created_date",head:"创建日期",width:"15%",filterType:"date"}
    ]
  }
#end

#sql("test.countSql")
  SELECT COUNT(1) 
  FROM SYS_MENU
#end


#sql("testCustomUrl.meta")
  {
    rowId:"id",
    queryUrl: "/sys/test/testCustomUrlPopup",
    colDatas:[
        {field:"menu_name",head:"名称",width:"15%",switchable:"false"},
        {field:"comments",head:"描述",width:"20%"},
        {field:"status",head:"状态",width:"15%",filterType:"select",dictCode:"PUB_ENABLE_DISABLE"},
        {field:"menu_url",head:"url",width:"35%"},
        {field:"menu_icon",head:"图标",width:"15%",body:"<i class='fa ~{value} fa-1' aria-hidden='true'></i>~{value}"}
    ]
  }
#end

#sql("findTestCodeList")
select * from test_code where 1 = 1
#if(test_int??) and test_int	 = #para(test_int) #end
#if(test_varchar??) and test_varchar	 = #para(test_varchar) #end
#if(test_bigint??) and test_bigint	 = #para(test_bigint) #end
#if(test_char??) and test_char	 = #para(test_char) #end
#if(test_date??) and date_format(test_date, '%Y-%m-%d') = #para(test_date) #end
#if(test_double??) and test_double	 = #para(test_double) #end
#if(test_float??) and test_float	 = #para(test_float) #end
#if(test_decimal??) and test_decimal	 = #para(test_decimal) #end
#if(status??) and status	 = #para(status) #end
#end

#sql("findTestCodeInfo")
select * from test_code where ID = ?
#end