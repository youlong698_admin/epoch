#sql("findSysUser")
select * from sys_user u where 1 = 1
#if(account??)
    and u.account=#para(account)
#end
#if(account??)
    or u.email=#para(account)
#end
#end

--查询所有用户
#sql("findSysUserAll")
select s.*,g.company_name as company_name,g.ORG_NAME as dept_name
from sys_user s left join  sys_org g on s.dept_id=g.id where 1=1
  #if(parent_id??)AND (g.parent_id =#para(parent_id) or g.id =#para(parent_id))#end
  #if(account??)AND s.account like '%'||#para(account)||'%'#end
  #if(employee_number??) and INSTR(UPPER(employee_number),UPPER(#para(employee_number))) > 0 #end
  #if(phone_number??)AND s.phone_number like '%'||#para(phone_number)||'%'#end
  #if(position_level??)AND s.position_level = #para(position_level)#end
  #if(is_locked??)AND s.is_locked = #para(is_locked)#end
  #if(is_used??)AND s.is_used = #para(is_used)#end
  #if(status??)AND s.status = #para(status)#end
  #if(user_name??)and s.user_name like '%'||#para(user_name)||'%'#end
  #if(email??)and s.email like '%'||#para(email)||'%'#end
  #if(company_name??)and g.company_name like '%'||#para(company_name)||'%'#end
  #if(dept_name??)and g.ORG_NAME like '%'||#para(dept_name)||'%'#end
  order by #if(sortName??)#(sortName) #else s.update_date #end #if(sortOrder?? && sortOrder=="desc") desc #end
#end

#sql("findSysUserList")
select s.*,g.company_name as company_name,g.ORG_NAME as dept_name
from sys_user s left join  sys_org g on s.dept_id=g.id  left join  sys_org m on m.id=g.parent_id where 1=1
	#if(parent_id??)AND g.parent_id =#para(parent_id) or g.id =#para(parent_id) or m.parent_id =#para(parent_id)#end
	#if(account??) and INSTR(UPPER(s.account),UPPER(#para(account))) > 0 #end
	#if(user_name??) and INSTR(UPPER(s.user_name),UPPER(#para(user_name))) > 0 #end
	#if(employee_number??) and INSTR(UPPER(s.employee_number),UPPER(#para(employee_number))) > 0 #end
	#if(email??) and INSTR(UPPER(s.email),UPPER(#para(email))) > 0 #end
	#if(phone_number??) and INSTR(UPPER(s.phone_number),UPPER(#para(phone_number))) > 0 #end
	#if(status??)AND s.status = #para(status)#end
	#if(company_name??)and g.company_name like '%'||#para(company_name)||'%'#end
  	#if(dept_name??)and g.ORG_NAME like '%'||#para(dept_name)||'%'#end
	order by #if(sortName??)#(sortName) #else s.update_date #end #if(sortOrder?? && sortOrder=="desc") desc #end
#end

#sql("findAllUserList")
select * from sys_user
#end

### ==start== 用户编辑页面分配角色弹出框sqlKey
#sql("findRoleForUser.meta")
{
    rowId:"id",
    colDatas:[
        {field:"role_name",head:"sys_role_label_roleName",width:"25%",switchable:"false"},
        {field:"role_code",head:"sys_role_label_roleCode",width:"25%"},
        {field:"comments",head:"sys_role_label_comment",width:"50%"}
    ]
}
#end
#sql("findRoleForUser")
SELECT id,
       role_name,
       role_code,
       comments
FROM sys_role s
#end
### ===end=== 用户编辑页面分配角色弹出框sqlKey

#sql("findSysUserById")
SELECT su.id,su.ACCOUNT,su.USER_NAME,su.EMAIL,su.PHONE_NUMBER,su.BIRTHDAY,su.EMPLOYEE_NUMBER,su.POSITION_LEVEL,
su.STATUS,su.CREATE_DATE,so.ORG_NAME from sys_user su inner join sys_org  so on su.DEPT_ID = so.ID where su.ID = #para(userId)
#end

#sql("selectUserRoleByUserIdAndRoleType")
SELECT ur.id,
       ur.user_id,
       ur.role_id,
       r.role_name
FROM sys_user_role ur
    INNER JOIN sys_role r ON ur.role_id = r.id
WHERE ur.user_id = #para(userId)
  AND r.role_type = #para(roleType)
ORDER BY ur.id
#end

### ==start== 用户编辑页面分配角色弹出框sqlKey
#sql("selectRoleForUser.meta")
{
    rowId:"id",
    colDatas:[
        {field:"role_name",head:"角色名称",width:"25%",switchable:"false"},
        {field:"role_code",head:"角色编码",width:"25%"},
        {field:"comments",head:"描述",width:"50%"}
    ]
}
#end
#sql("selectRoleForUser")
SELECT r.id,
       r.role_name,
       r.role_code,
       r.comments
FROM sys_role r
WHERE r.status = 'ENABLE'
  AND r.role_type = #para(roleType)
#if(role_name??)  AND INSTR(UPPER(r.role_name),UPPER(#para(role_name))) > 0 #end
#if(role_code??)  AND INSTR(UPPER(r.role_code),UPPER(#para(role_code))) > 0 #end
#if(comments??)  AND INSTR(UPPER(r.comments),UPPER(#para(comments))) > 0 #end
ORDER BY #if(sortName??)#if(sortName != "name") r.#end#(sortName) #if(sortOrder?? == "desc") DESC #end, #end
    r.id #if(sortOrder?? == "desc") DESC #end
#end
### ===end=== 用户编辑页面分配角色弹出框sqlKey

--根据用户ID组织对应关系
#sql("selectUserOrgByUserId")
SELECT * FROM SYS_ORG WHERE ID = ?
#end


###用户角色分配
#define queryUserRoleBase()
SELECT
    ur.id,
    u.employee_number,
    u.USER_NAME,
    u.status,
    (
        SELECT o.org_name
        FROM sys_org o
        WHERE o.id = u.dept_id
    ) AS dept_name,
    (
        SELECT group_concat(r.role_name)
        FROM sys_user_role uur
            INNER JOIN sys_role r ON uur.role_id = r.id AND r.role_type = 'SYSTEM'
        WHERE uur.user_id = u.id
    ) AS sys_role_names
#end

#define queryUserRoleWhereClause(employee_number,account,full_name,status,dept_name,role_names)
WHERE 1 = 1
#if(employee_number??)  AND INSTR(UPPER(t.employee_number),UPPER(#para(employee_number))) > 0 #end
#if(user_name??)  AND INSTR(UPPER(t.user_name),UPPER(#para(user_name))) > 0 #end
#if(status??)  AND t.status = #para(status) #end
#if(dept_name??)  AND INSTR(UPPER(t.dept_name),UPPER(#para(dept_name))) > 0 #end
#if(sys_role_names??)  AND INSTR(UPPER(t.sys_role_names),UPPER(#para(sys_role_names))) > 0 #end
#if(busi_role_names??)  AND INSTR(UPPER(t.busi_role_names),UPPER(#para(busi_role_names))) > 0 #end
#if(dept_role_names??)  AND INSTR(UPPER(t.dept_role_names),UPPER(#para(dept_role_names))) > 0 #end
#end


#define queryUserRoleByRoleIdBase(roleId,employee_number,account,user_name,status,dept_name,role_names)
FROM (
    #@queryUserRoleBase()
    FROM
        sys_user_role ur
        INNER JOIN sys_user u ON ur.user_id = u.id
    WHERE ur.role_id = #(roleId)
) t
#@queryUserRoleWhereClause(employee_number,account,user_name,status,dept_name,role_names)
#end

#define queryUserRoleByDeptRoleBase(deptRole,employee_number,account,user_name,status,dept_name,role_names)
FROM (
    #@queryUserRoleBase()
    FROM
        sys_user_dept_role ur
        INNER JOIN sys_user u ON ur.user_id = u.id
    WHERE ur.dept_role = '#(deptRole)'
) t
#@queryUserRoleWhereClause(employee_number,account,user_name,status,dept_name,role_names)
WHERE 1 = 1
#if(employee_number??)  AND INSTR(UPPER(t.employee_number),UPPER(#para(employee_number))) > 0 #end
#if(user_name??)  AND INSTR(UPPER(t.user_name),UPPER(#para(user_name))) > 0 #end
#if(status??)  AND t.status = #para(status) #end
#if(dept_name??)  AND INSTR(UPPER(t.dept_name),UPPER(#para(dept_name))) > 0 #end
#if(account??)  AND INSTR(UPPER(t.account),UPPER(#para(account))) > 0 #end
#end

#sql("searchUserRoleByRoleId")
SELECT t.*
#@queryUserRoleByRoleIdBase(roleId,employee_number,account,user_name,status,dept_name,role_names)
#end

#sql("countUserRoleByRoleId")
SELECT COUNT(1)
#@queryUserRoleByRoleIdBase(roleId,employee_number,account,user_name,status,dept_name,role_names)
#end

#sql("searchUserRoleByDeptRole")
SELECT t.*
#@queryUserRoleByDeptRoleBase(deptRole,employee_number,account,user_name,status,dept_name,role_names)
#end

#sql("selectUserDeptRoleByUserId")
SELECT udr.id,
       udr.user_id,
       udr.dept_role,
       d.item_value AS role_name
FROM sys_user_dept_role udr
    INNER JOIN sys_dict d ON udr.dept_role = d.item_code AND d.dict_code = 'SYS_DEPT_WF_ROLE' AND d.head_flag = 'N'
WHERE udr.user_id = #para(userId)
ORDER BY udr.id
#end


#sql("countUserRoleByDeptRole")
SELECT COUNT(1)
#@queryUserRoleByDeptRoleBase(deptRole,employee_number,account,user_name,status,dept_name,role_names)
#end


--根据用户ID删除人员和角色对应关系
#sql("deleteUserRoleByUserId")
delete from sys_user_role where user_id = ?
#end

--根据用户ID删除人员和组织对应关系
#sql("deleteUserOrgByUserId")
delete from sys_user_org where user_id = ?
#end