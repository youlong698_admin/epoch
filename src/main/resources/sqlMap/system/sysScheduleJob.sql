
--查询所有角色
#sql("findSysScheduleJob")
select t.ID,t.JOB_NAME,t.JOB_GROUP,t.JOB_STATUS,t.TARGET_CLASS,t.CRON_EXPRESSION,t.JOB_DESC  
from sys_schedule_job t where 1=1
#if(job_name??) and t.job_name like '%'||#para(job_name)||'%'#end
#if(job_group??) and t.job_group like '%'||#para(job_group)||'%'#end
#if(job_status??) and t.job_status=#para(job_status)#end
#if(target_class??) and t.target_class like '%'||#para(target_class)||'%'#end
#if(cron_expression??) and t.cron_expression=#para(cron_expression)#end
#if(tjob_desc??) and t.job_desc=#para(job_desc)#end
order by #if(sortName??)#(sortName) #else t.job_name #end #if(sortOrder?? && sortOrder=="desc")desc#end
#end
