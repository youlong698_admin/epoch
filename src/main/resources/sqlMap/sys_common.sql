#namespace("systemManage")
	#include("system/sysUser.sql")--用户管理
	#include("system/sysDict.sql")--数据字典
	#include("system/sysMenu.sql")--菜单管理
	#include("system/sysOrg.sql")-- 组织管理
	#include("system/sysRole.sql")--角色管理
	#include("system/sysRoleMenu.sql")--角色菜单管理
	#include("system/sysLog.sql")--日志管理
	#include("system/sysInfo.sql")--消息管理
	#include("system/sysSerialNumber.sql")--消息管理
	#include("system/sysScheduleJob.sql")--消息管理
	#include("system/SysGenerator.sql")--消息管理
#end
#namespace("test")
	#include("test/test.sql")--流程管理
#end
#namespace("sysCommon")
  #include("common/sysCommon.sql") --系统公共功能
#end
#namespace("oss")
  #include("oss/sysOss.sql")
#end