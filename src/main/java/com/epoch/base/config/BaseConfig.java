package com.epoch.base.config;

import org.beetl.core.GroupTemplate;
import org.beetl.ext.jfinal3.JFinal3BeetlRenderFactory;

import com.epoch.base.annotation.AutoBindRoutes;
import com.epoch.base.base.BaseController;
import com.epoch.base.db.EpochDataSource;
import com.epoch.base.export.ExportInterceptor;
import com.epoch.base.function.BeetlFunctions;
import com.epoch.base.function.GlobalFunctions;
import com.epoch.base.function.I18nFunction;
import com.epoch.base.function.SetSharedVars;
import com.epoch.base.intercepter.EpochI18nInterceptor;
import com.epoch.base.redis.RedisPlugin;
import com.epoch.base.schedule.QuartzPlugin;
import com.epoch.base.shiro.ShiroInterceptor;
import com.epoch.base.shiro.ShiroPlugin;
import com.epoch.base.util.Config;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.kit.PropKit;
import com.jfinal.log.Log4jLogFactory;
import com.jfinal.plugin.activerecord.tx.TxByMethodRegex;
import com.jfinal.plugin.activerecord.tx.TxByMethods;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.template.Engine;

public class BaseConfig extends JFinalConfig {

	private Routes routes;

	@Override
	public void configConstant(Constants me) {
		PropKit.use("application.properties");
		me.setDevMode(Config.getToBoolean("config.devMode"));
		me.setEncoding("UTF-8");
		me.setLogFactory(new Log4jLogFactory());
		JFinal3BeetlRenderFactory renderFactory = new JFinal3BeetlRenderFactory();
		renderFactory.config();
		// 配置beetl
		me.setRenderFactory(renderFactory);
		GroupTemplate groupTemplate = renderFactory.groupTemplate;
		groupTemplate.registerFunctionPackage("lisFun", BeetlFunctions.class);
		groupTemplate.registerFunctionPackage("global", GlobalFunctions.class);
		groupTemplate.setSharedVars(SetSharedVars.getSharedVars());
		groupTemplate.registerFunction("i18n", new I18nFunction());
		me.setI18nDefaultBaseName("i18n");
		me.setError401View(Config.getStr("PAGES.401"));
		me.setError403View(Config.getStr("PAGES.403"));
		me.setError404View(Config.getStr("PAGES.404"));
		me.setError500View(Config.getStr("PAGES.500"));
	}

	@Override
	public void configRoute(Routes me) {
		this.routes = me;
		AutoBindRoutes autoBindRoutes = new AutoBindRoutes();
		autoBindRoutes.addExcludeClass(BaseController.class);
		me.add(autoBindRoutes);
	}
	@Override
	public void configEngine(Engine me) {
	}

	@Override
	public void configPlugin(Plugins me) {
		new RedisPlugin(me);
		EpochDataSource.initDataSource(me);
		ShiroPlugin shiroPlugin = new ShiroPlugin(this.routes);
		shiroPlugin.setLoginUrl("/");
		shiroPlugin.setSuccessUrl("/index/index");
		shiroPlugin.setUnauthorizedUrl("/");
		//me.add(new EhCachePlugin());
		// new EpochRedisPlugin(me);
		me.add(shiroPlugin);
		//定时任务加载
        me.add(new QuartzPlugin());
	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new ShiroInterceptor());
		me.add(new EpochI18nInterceptor());
		me.add(new TxByMethodRegex("(.*save.*|.*update.*|.*delete.*)"));
		me.add(new TxByMethods("save", "update", "delete"));
		me.add(new ExportInterceptor());
	}

	@Override
	public void configHandler(Handlers me) {
		DruidStatViewHandler dvh = new DruidStatViewHandler("/druid");
		me.add(dvh);
		me.add(new ContextPathHandler("basePath"));
	}
	
	@Override
	public void afterJFinalStart() {
		BeetlFunctions.initMenuCode();
		BeetlFunctions.initSystemInfo();
	}

	public static void main(String[] args) {

		JFinal.start("webapp", 18080, "epoch-weixin", 5);

	}

}
