package com.epoch.base.intercepter;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;
import com.jfinal.i18n.I18n;
import com.jfinal.i18n.I18nInterceptor;
import com.jfinal.i18n.Res;

public class EpochI18nInterceptor extends I18nInterceptor {
	
	private static final ThreadLocal<Map<String, String>> TL_LOCALE = new ThreadLocal<Map<String, String>>();
	
	private static final String LOCALE_IN_REQ_PARAM = "localeInReqParam";
    private static final String LOCALE_IN_COOKIE = "localeInCookie";
    private static final String LOCALE_IN_REQ = "localeInReq";
	
	private boolean isSwitchView = false;
	
	public EpochI18nInterceptor() {
        super();
    }
	
	public EpochI18nInterceptor(boolean isSwitchView) {
        super(isSwitchView);
        this.isSwitchView = isSwitchView;
    }

    public EpochI18nInterceptor(String localeParaName, String resName, boolean isSwitchView) {
        super(localeParaName, resName, isSwitchView);
        this.isSwitchView = isSwitchView;
    }

    public EpochI18nInterceptor(String localeParaName, String resName) {
        super(localeParaName, resName);
    }
    
    public static String getCurLocale() {
        String locale = EpochI18nInterceptor.getCurLocaleFromReqParam();
        if (StringUtils.isEmpty(locale)) {
            locale = EpochI18nInterceptor.getCurLocaleFromCookie();
        }
        if (StringUtils.isEmpty(locale)) {
            locale = EpochI18nInterceptor.getCurLocaleFromReq();
        }
        if (StringUtils.isEmpty(locale)) {
            locale = EpochI18nInterceptor.getCurLocaleFromSys();
        }
        return locale;
    }
    
    public static String getCurLocaleFromReqParam() {
        return EpochI18nInterceptor.getTlData(EpochI18nInterceptor.LOCALE_IN_REQ_PARAM);
    }

    public static String getCurLocaleFromCookie() {
        return EpochI18nInterceptor.getTlData(EpochI18nInterceptor.LOCALE_IN_COOKIE);
    }

    public static String getCurLocaleFromReq() {
        return EpochI18nInterceptor.getTlData(EpochI18nInterceptor.LOCALE_IN_REQ);
    }
    
    public static String getCurLocaleFromSys() {
        Locale sysLocale = Locale.getDefault();
        return I18n.toLocale(sysLocale);
    }
    
    @Override
    public void intercept(Invocation inv) {
        Controller c = inv.getController();
        String locale = getCurLocale();
        inv.invoke();
        if (this.isSwitchView) {
            this.switchView(locale, c);
        } else {
            Res res = I18n.use(this.getBaseName(), locale);
            c.setAttr(this.getResName(), res);
        }
    }
    
    private static String getTlData(String key) {
        return EpochI18nInterceptor.getTlMapWithCreate().get(key);
    }

    private static void setTlData(String key, String val) {
        EpochI18nInterceptor.getTlMapWithCreate().put(key, val);
    }
    
    private static Map<String, String> getTlMapWithCreate() {
        Map<String, String> dataMap = EpochI18nInterceptor.TL_LOCALE.get();
        if (dataMap == null) {
            dataMap = new HashMap<String, String>(3);
            EpochI18nInterceptor.TL_LOCALE.set(dataMap);
        }
        return dataMap;
    }
	
	
}
