package com.epoch.base.base;

import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.core.Injector;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordException;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Table;
import com.jfinal.plugin.activerecord.TableMapping;

public class ExtInjector extends Injector {
    @SuppressWarnings("unchecked")
    public static final <T> T injectModelExt(Class<T> modelClass, String modelName, HttpServletRequest request, boolean skipConvertError) {
        Object temp = createInstance(modelClass);
        if (temp instanceof Model == false) {
            throw new IllegalArgumentException("getModel only support class of Model, using getBean for other class.");
        }
        Model<?> model = (Model<?>)temp;
        Table table = TableMapping.me().getTable(model.getClass());
        if (table == null) {
            throw new ActiveRecordException("The Table mapping of model: " + modelClass.getName() + " not exists or the ActiveRecordPlugin not start.");
        }
        String modelNameAndDot = StrKit.notBlank(modelName) ? modelName + "." : null;
        Map<String, String[]> parasMap = request.getParameterMap();
        // 对 paraMap进行遍历而不是对table.getColumnTypeMapEntrySet()进行遍历，以便支持 CaseInsensitiveContainerFactory
        // 以及支持界面的 attrName有误时可以感知并抛出异常避免出错
        for (Entry<String, String[]> entry : parasMap.entrySet()) {
            String paraName = entry.getKey();
            String attrName;
            if (modelNameAndDot != null) {
                if (paraName.startsWith(modelNameAndDot)) {
                    attrName = paraName.substring(modelNameAndDot.length());
                } else {
                    continue;
                }
            } else {
                attrName = paraName;
            }
            Class<?> colType = table.getColumnType(attrName);
            try {
                String[] paraValueArray = entry.getValue();
                String paraValue = (paraValueArray != null && paraValueArray.length > 0) ? paraValueArray[0] : null;
                if (colType == null) {
                    Object value = "".equals(paraValue) ? null : paraValue; // 用户在表单域中没有输入内容时将提交过来 "", 因为没有输入,所以要转成 null.
                    model.put(attrName, value);
                } else {
                    Object value = paraValue != null ? TypeConverter.convert(colType, paraValue) : null;
                    model.set(attrName, value);
                }
            } catch (Exception e) {
                if (skipConvertError == false) {
                    throw new RuntimeException("Can not convert parameter: " + paraName, e);
                }
            }
        }
        return (T)model;
    }

    public static <T> T createInstance(Class<T> objClass) {
        try {
            return objClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
