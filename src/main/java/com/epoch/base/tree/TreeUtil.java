package com.epoch.base.tree;

import java.util.ArrayList;
import java.util.List;

public class TreeUtil {
	
	public static List<ZrTreeBean>  getTreeList(List<ZrTreeBean> allTreeList){
		return getTreeList(allTreeList,null);
	}
	
	public static List<ZrTreeBean>  getTreeList(List<ZrTreeBean> allTreeList,String parentId){
		List<ZrTreeBean> treeList = new ArrayList<ZrTreeBean>();
		for (ZrTreeBean treeBean : allTreeList) {
			if(treeBean.getParentId() == parentId) {
				treeBean.setOpen(true);
				treeList.add(treeBean);
			}
		}
		for (ZrTreeBean treeBean : treeList) {
			List<ZrTreeBean> childTreeList = getChildTree(treeBean.getId(),allTreeList);
			if(childTreeList.size()>0) {
				treeBean.setChildren(getChild(childTreeList,allTreeList));
			}
		}
		return treeList;
	}
	
	public static List<ZrTreeBean> getChild(List<ZrTreeBean> childTreeList,List<ZrTreeBean> allTreeList){
		for (ZrTreeBean treeBean : childTreeList) {
			List<ZrTreeBean> childTreeList2 = getChildTree(treeBean.getId(),allTreeList);
			if(childTreeList2.size()>0) {
				treeBean.setChildren(getChild(childTreeList2,allTreeList));
			}
		}
		return childTreeList;
	}
	
	public static List<ZrTreeBean> getChildTree(String id,List<ZrTreeBean> allTreeList){
		List<ZrTreeBean> childList = new ArrayList<ZrTreeBean>();
		for (ZrTreeBean treeBean : allTreeList) {
			 if (null != treeBean.getParentId() && treeBean.getParentId().equals(id)) {
				 childList.add(treeBean);
			 }
		}
		return childList;
	}
}
