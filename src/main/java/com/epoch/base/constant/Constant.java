package com.epoch.base.constant;

public interface Constant {
	
	int INT0 = 0;
    int INT1 = 1;
    int INT2 = 2;
    int INT3 = 3;
    int INT4 = 4;
    int INT5 = 5;
    int INT6 = 6;
    int INT7 = 7;
    int INT8 = 8;
    int INT9 = 9;
    int INT10 = 10;
    int INT11 = 11;
    int INT12 = 12;
	
	String PROPERTIES_SNOW_DATA = "application.properties";
	
	String EPOCH_INIT = "application.properties";
	
	String CONFIG_SCAN_JAR = "config.scan.jar";
	
	String STR0 = "0";
    String STR1 = "1";
    String STR_EMPTY = "";
    String COMIC_TYPE = "01";
    
    char SPACE = ' ';
    char COMMA = ',';
    char DOT = '.';
    char COLON = ':';
    char SEMICOLON = ';';
    char QMARK = '?';
    char EXCLAM = '!';
    char QUOT = '"';
    char APOS = '\'';
    char PLUS = '+';
    char MINUS = '-';
    char EQUAL = '=';
    char LT = '<';
    char GT = '>';
    char PARENLEFT = '(';
    char PARENRIGHT = ')';
    char BRACKETLEFT = '[';
    char BRACKETRIGHT = ']';
    char BRACELEFT = '{';
    char BRACERIGHT = '}';
    char UNDERSCORE = '_';
    char SHARP = '#';
    char STAR = '*';
    char PERCENT = '%';
    char AT = '@';
    char AMP = '&';
    char DOLLAR = '$';
    char CARET = '^';
    char SLASH = '/';
    char BSLASH = '\\';
    char TILDE = '~';
    char BQUOT = '`';
    char BAR = '|';
    String MENU_CODE = "menu_code";
    String MENU_NAME = "menu_name";
    
    /**
     * 启用
     */
    String JOB_STATUS_0 = "0";

    /**
     * 暂停
     */
    String JOB_STATUS_1 = "1";

    /**
     * 禁用
     */
    String JOB_STATUS_2 = "2";
}
