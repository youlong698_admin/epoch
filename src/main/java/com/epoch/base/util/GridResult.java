package com.epoch.base.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GridResult{
	
	public static Map getResult(List<?> rows,Integer total) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("rows", rows);
		map.put("total", total);
		return map;
	}
}
