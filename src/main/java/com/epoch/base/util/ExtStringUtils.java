package com.epoch.base.util;

import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashSet;
import java.util.Set;

public class ExtStringUtils {
    private static final char ESC = '\\';
    private static final char TOKEN_START = '{';
    private static final char TOKEN_END = '}';
    private static final byte MODE_TXT = 0;
    private static final byte MODE_TOKEN = 1;
    private static final byte MODE_ESC = 2;

    /**
     * 将字符串转义以适用于直接以字面量拼接到Javascript字符串中（以<code>"</code>作为Javascript使用的引号字符）<br>
     * 例如：<br>
     * <code>{account:"domain\Jack"}</code><br>
     * 以上字符串在转义后将会得到：<br>
     * <code>{account:\"domain\\Jack\"}</code>
     * @param str 要转义的字符串
     * @return 转义后的字符串
     */
    public static String escapeForJsStr(String str) {
        return escapeForJsStr(str, "\"");
    }

    /**
     * 将字符串转义以适用于直接以字面量拼接到Javascript字符串中（以指定的<code>quot</code>字符作为Javascript使用的引号字符）<br>
     * 例如：<br>
     * <code>{account:"domain\Jack"}</code><br>
     * 以上字符串在使用<code>"</code>作为引号符转义后将会得到：<br>
     * <code>{account:\"domain\\Jack\"}</code>
     * @param str 要转义的字符串
     * @param quot Javascript使用的引号字符
     * @return 转义后的字符串
     */
    public static String escapeForJsStr(String str, String quot) {
        String result = null;
        if (str != null) {
            result = str.replace("\\", "\\\\");
            result = result.replace(quot, "\\" + quot);
            result = result.replace("\r\n", "\\n");
            result = result.replace("\n", "\\n");
            result = result.replace("\r", "\\r");
            result = result.replace("/", "\\/");
        }
        return result;
    }

    /**
     * 将字符串转义以适用于直接输出在html页面中<br>
     * 例如：<br>
     * <code>String flag = num1 &lt; 100 &amp;&amp; num2 &gt;= 0 ? "ok" : "err: &nbsp; &nbsp;number out of range.";</code><br>
     * 转义后变为：<br>
     * <code>String flag = num1 &amp;lt; 100 &amp;amp;&amp;amp; num2 &amp;gt;= 0 ? "ok" : "err: &amp;nbsp; &amp;nbsp;number out of range.";</code>
     * @param str 要转义的字符串
     * @return 转义后的字符串
     */
    public static String escapeHtml(String str) {
        String result = str;
        if (result != null) {
            result = result.replace("&", "&amp;");
            result = result.replace("<", "&lt;");
            result = result.replace(">", "&gt;");
            result = result.replace("\r\n", "<br/>");
            result = result.replace("\n", "<br/>");
            result = result.replace("\r", "<br/>");
            result = result.replaceAll("\\s{2}", " &nbsp;");
        }
        return result;
    }

    /**
     * 提取异常对象中的stack trace内容并拼为字符串<br>
     * 提取的内容包括指定Throwable对象及其所有cause的stack trace内容，结果类似于printStackTrace()方法输出的异常信息内容，便于传递到其它地方以展示异常详情
     * @param t 要提取stack trace的异常对象
     * @return 提取的所有stack trace拼接字符串
     */
    public static String excpStackTraceStr(final Throwable t) {
        String result = null;
        if (t != null) {
            StringWriter writer = new StringWriter();
            PrintWriter printer = new PrintWriter(new BufferedWriter(writer));
            Set<Throwable> causeSet = new HashSet<Throwable>();
            Throwable e = t;
            boolean first = true;
            while (e != null) {
                causeSet.add(e);
                if (first) {
                    printer.print("Exception: ");
                    first = false;
                } else {
                    printer.print("Cause: ");
                }
                printer.print(e.getClass().getName());
                printer.print(": ");
                printer.println(e.getMessage());
                StackTraceElement[] traces = e.getStackTrace();
                if (traces != null && traces.length > 0) {
                    for (StackTraceElement trace : traces) {
                        printer.print("\tat ");
                        printer.println(trace.toString());
                    }
                }
                Throwable cause = e.getCause();
                e = cause;
                if (causeSet.contains(cause)) {
                    printer.print("Cause[Recursively]: ");
                    printer.print(e.getClass().getName());
                    printer.print(": ");
                    printer.println(e.getMessage());
                    e = null;
                }
            }
            printer.flush();
            printer.close();
            result = writer.toString();
        }
        return result;
    }

    public static String format(String template, Object... params) {
        String result = template;
        if (result != null) {
            int len = result.length();
            int i = 0;
            byte mode = MODE_TXT;
            StringBuilder buff = new StringBuilder();
            StringBuilder tokenBuff = new StringBuilder();
            for (; i < len; i++) {
                char c = result.charAt(i);
                switch (mode) {
                case MODE_TXT:
                    switch (c) {
                    case TOKEN_START:
                        mode = MODE_TOKEN;
                        break;
                    case ESC:
                        mode = MODE_ESC;
                        break;
                    default:
                        buff.append(c);
                        break;
                    }
                    break;
                case MODE_TOKEN:
                    switch (c) {
                    case TOKEN_END:
                        String token = tokenBuff.toString();
                        if (token != null && token.matches("^[0-9]{1,9}$")) {
                            int tokenIdx = Integer.parseInt(token);
                            if (tokenIdx < params.length) {
                                Object param = params[tokenIdx];
                                if (param != null) {
                                    buff.append(param.toString());
                                }
                            }
                        } else {
                            buff.append(TOKEN_START);
                            buff.append(token == null ? "" : token);
                            buff.append(TOKEN_END);
                        }
                        tokenBuff = new StringBuilder();
                        mode = MODE_TXT;
                        break;
                    default:
                        tokenBuff.append(c);
                        break;
                    }
                    break;
                case MODE_ESC:
                    buff.append(c);
                    mode = MODE_TXT;
                    break;
                default:
                    break;
                }
            }
            result = buff.toString();
        }
        return result;
    }

    public ExtStringUtils() {
        // 禁止实例化工具类
    }
}
