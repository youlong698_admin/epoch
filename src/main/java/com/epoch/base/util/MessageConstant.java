package com.epoch.base.util;

public interface MessageConstant {
	/**
	 * 操作成功！
	 */
	public static final String MESSAGE_SUCCESS="操作成功";
	/**
	 * 操作失败！
	 */
	public static final String MESSAGE_ERROR="操作失败";
	
}
