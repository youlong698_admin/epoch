package com.epoch.base.annotation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.epoch.base.constant.Constant;
import com.epoch.base.util.Config;
import com.jfinal.config.Routes;
import com.jfinal.core.Controller;
import com.jfinal.kit.StrKit;

public class AutoBindRoutes extends Routes{
	
	private final Logger logger = LoggerFactory.getLogger(AutoBindModels.class);
	
	private List<Class<? extends Controller>> excludeClasses =  new ArrayList<Class<? extends Controller>>();

	private List<String> includeJars = new ArrayList<String>();

	private boolean autoScan = true;

	private String suffix = "Controller";

	public AutoBindRoutes() {
	}

	public AutoBindRoutes(boolean autoScan) {
		this.autoScan = autoScan;
	}

	public AutoBindRoutes addJar(String jarName) {
		if (StrKit.notBlank(jarName)) {
			includeJars.add(jarName);
		}
		return this;
	}

	public AutoBindRoutes addJars(String jarNames) {
		if (StrKit.notBlank(jarNames)) {
			addJars(jarNames.split(","));
		}
		return this;
	}

	public AutoBindRoutes addJars(String[] jarsName) {
		includeJars.addAll(Arrays.asList(jarsName));
		return this;
	}

	public AutoBindRoutes addJars(List<String> jarsName) {
		includeJars.addAll(jarsName);
		return this;
	}

	public AutoBindRoutes addExcludeClass(Class<? extends Controller> clazz) {
		if (clazz != null) {
			excludeClasses.add(clazz);
		}
		return this;
	}

	public AutoBindRoutes addExcludeClasses(Class<? extends Controller>[] clazzes) {
		excludeClasses.addAll(Arrays.asList(clazzes));
		return this;
	}

	public AutoBindRoutes addExcludeClasses(List<Class<? extends Controller>> clazzes) {
		excludeClasses.addAll(clazzes);
		return this;
	}

	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public void config() {
		List<Class<? extends Controller>> controllerClasses = ClassSearcher.findInClasspathAndJars(Controller.class,
				includeJars);
		ControllerBind controllerBind = null;
		for (Class controller : controllerClasses) {
			if (excludeClasses.contains(controller)) {
				continue;
			}
			controllerBind = (ControllerBind) controller.getAnnotation(ControllerBind.class);
			if (controllerBind == null) {
				if (!autoScan) {
					continue;
				}
				if (!controller.getSimpleName().endsWith(suffix)) {
					logger.info("routes.add " + controller.getName() + " is suffix not " + suffix);
					continue;
				}
				
				this.add(controllerKey(controller), controller);
				logger.info("routes.add(" + controllerKey(controller) + ", " + controller.getName() + ")");
			} else if (StrKit.isBlank(controllerBind.viewPath())) {
				this.add(controllerBind.controllerKey(), controller);
				logger.info("routes.add(" + controllerBind.controllerKey() + ", " + controller.getName() + ")");
			} else {
				this.add(controllerBind.controllerKey(), controller, controllerBind.viewPath());
				logger.info("routes.add(" + controllerBind.controllerKey() + ", " + controller + ","
						+ controllerBind.viewPath() + ")");
			}
		}
	}

	private String controllerKey(Class<Controller> clazz) {
		String controllerKey = "/" + StrKit.firstCharToLowerCase(clazz.getSimpleName());
		controllerKey = controllerKey.substring(0, controllerKey.indexOf(suffix));
		return controllerKey;
	}

	public List<Class<? extends Controller>> getExcludeClasses() {
		return excludeClasses;
	}

	public void setExcludeClasses(List<Class<? extends Controller>> excludeClasses) {
		this.excludeClasses = excludeClasses;
	}

	public List<String> getIncludeJars() {
		return includeJars;
	}

	public void setIncludeJars(List<String> includeJars) {
		this.includeJars = includeJars;
	}

	public void setAutoScan(boolean autoScan) {
		this.autoScan = autoScan;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
}
