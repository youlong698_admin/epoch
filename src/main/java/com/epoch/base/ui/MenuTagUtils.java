package com.epoch.base.ui;

import java.util.List;
import org.apache.shiro.SecurityUtils;
import com.epoch.base.tree.ZrTreeBean;

public class MenuTagUtils {

	public static String getEnd() {
		List<ZrTreeBean> menuList = (List<ZrTreeBean>) SecurityUtils.getSubject().getSession().getAttribute("menuTreeOpen");
		String style = (String) SecurityUtils.getSubject().getSession().getAttribute("EPOCHSTYLE");
		StringBuffer sb = new StringBuffer();
		if("adminlte".equalsIgnoreCase(style)) {
			sb.append("<ul class=\"sidebar-menu\" id=\"sidebarnav\" >");
			sb.append(getTreeMenu(menuList));
			sb.append("</ul>");
		}else if("hplus".equalsIgnoreCase(style)) {
			sb.append(getHplusMenu(menuList));
		}else {
			sb.append(getAceMultistageTree(menuList));
		}
		return sb.toString();
		
	}
	
	private static Object getHplusMenu(List<ZrTreeBean> menuList) {
		if(null == menuList ||  menuList.size()==0){
			return "不具有任何权限,\n请找管理员分配权限";
		}
		StringBuffer menuString = new StringBuffer();
		for (ZrTreeBean tree : menuList) {
			if(tree.getChildren()!=null && tree.getChildren().size()!=0) {
				menuString.append("<li>");
				menuString.append("<a href=\"#\" class=\"epochMenuParent\"><i class=\""+tree.getIcon()+"\"></i> <span class=\"nav-label\">");
				menuString.append(tree.getName());
				menuString.append("</span><span class=\"fa arrow\"></span></a>");
				menuString.append("<ul class=\"nav nav-second-level\">");
				menuString.append(getHplusChildOfTree(tree,tree.getChildren()));
				menuString.append("</ul>");
				menuString.append("</li>");
			}else {
				menuString.append(getHplusLeafOfTree(tree));
			}
		}
		return menuString.toString();
	}

	private static Object getHplusChildOfTree(ZrTreeBean parent,List<ZrTreeBean> menuList){
		StringBuffer menuString = new StringBuffer();
		for (ZrTreeBean tree : menuList) {
			if(tree.getChildren()!=null && tree.getChildren().size()!=0) {
				menuString.append("<li>");
				menuString.append("<a href=\"#\" class=\"epochMenuParent\"><i class=\""+tree.getIcon()+"\"></i> <span class=\"nav-label\">");
				menuString.append(tree.getName());
				menuString.append("</span><span class=\"fa arrow\"></span></a>");
				menuString.append("<ul class=\"nav nav-second-level\">");
				menuString.append(getHplusChildOfTree(tree,tree.getChildren()));
				menuString.append("</ul>");
				menuString.append("</li>");
			}else {
				menuString.append(getHplusLeafOfTree(tree));
			}
		}
		return menuString.toString();
	}

	private static Object getHplusLeafOfTree(ZrTreeBean tree) {
		StringBuffer menuString = new StringBuffer();
		menuString.append("<li>");
		menuString.append("<a class=\"J_menuItem epochMenu\" href=\"javascript:void(0)\" data-href=\""+tree.getLink()+"\""+"data-code=\""+tree.getCode()+ "\">");
		menuString.append("<i class=\""+tree.getIcon()+"\"></i>");
		menuString.append(tree.getName());
		menuString.append("</a></li>");
		return menuString.toString();
	}

	private static String getTreeMenu(List<ZrTreeBean> menuList) {
		if(null == menuList ||  menuList.size()==0){
			return "不具有任何权限,\n请找管理员分配权限";
		}
		StringBuffer menuString = new StringBuffer();
		for (ZrTreeBean tree : menuList) {
			System.out.println(tree.getName());
			if(tree.getChildren()!=null && tree.getChildren().size()!=0) {
				menuString.append("<li class=\"treeview\">");
				menuString.append("<a href=\"#\" class=\"epochMenuParent\"><i class=\""+tree.getIcon()+"\"></i> <span>");
				menuString.append(tree.getName());
				menuString.append("</span><span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span></a>");
				menuString.append("<ul class=\"treeview-menu\">");
				menuString.append(getChildOfTree(tree,tree.getChildren()));
				menuString.append("</ul>");
			}else {
				menuString.append(getLeafOfTree(tree));
			}
			menuString.append("</li>");
		}
		return menuString.toString();
	}
	
	private static String getChildOfTree(ZrTreeBean parent,List<ZrTreeBean> menuList){
		StringBuffer menuString = new StringBuffer();
		for (ZrTreeBean tree : menuList) {
			System.out.println(tree.getName());
			if(tree.getChildren()!=null && tree.getChildren().size()!=0) {
				menuString.append("<li class=\"treeview\">");
				menuString.append("<a href=\"#\" class=\"epochMenuParent\"><i class=\""+tree.getIcon()+"\"></i> <span>");
				menuString.append(tree.getName());
				menuString.append("</span><span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span></a>");
				menuString.append("<ul class=\"treeview-menu\">");
				menuString.append(getChildOfTree(tree,tree.getChildren()));
				menuString.append("</ul>");
			}else {
				menuString.append(getLeafOfTree(tree));
			}
		}
		return menuString.toString();
	}
	
	private static String getLeafOfTree(ZrTreeBean tree){
		StringBuffer menuString = new StringBuffer();
		menuString.append("<li><a class=\"epochMenu\" href=\"javascript:void(0)\" data-href=\""+tree.getLink()+"\""+"data-code=\""+tree.getCode()+ "\">");
		menuString.append("<i class=\""+tree.getIcon()+"\"></i>");
		menuString.append(tree.getName());
		menuString.append("</a></li>");
		return menuString.toString();
	}
	
	private static String getAceMultistageTree(List<ZrTreeBean> menuList) {
		StringBuffer menuString = new StringBuffer();
		for (ZrTreeBean tree : menuList) {
			menuString.append("<li>");
			if(tree.getChildren()!=null && tree.getChildren().size()!=0) {
				menuString.append("<a href=\"javascript:void(0)\" class=\"dropdown-toggle epochMenuParent\" ><i class=\"menu-icon "+tree.getIcon()+"\"></i>");
			}else {
				menuString.append("<a href=\"javascript:void(0)\" class=\"epochMenu\" data-href=\""+tree.getLink()+"\""+"data-code=\""+tree.getCode()+ "\"><i class=\"menu-icon "+tree.getIcon()+"\"></i>");
			}
			menuString.append("<span class=\"menu-text\">");
            menuString.append(tree.getName());
            menuString.append("</span>");
            if(tree.getChildren()!=null && tree.getChildren().size()!=0 ) {
            	menuString.append("<b class=\"arrow fa fa-angle-down\"></b></a><ul  class=\"submenu\" >");
            	menuString.append(getChildACEMenu(tree,tree.getChildren()));
            	menuString.append("</ul></li>");
            }else {
            	menuString.append("</a></li>");
            }
		}
		return menuString.toString();
	}
	
	private static Object getChildACEMenu(ZrTreeBean parent,List<ZrTreeBean> menuList){
		StringBuffer menuString = new StringBuffer();
		for (ZrTreeBean tree : menuList) {
			if(tree.getChildren()!=null && tree.getChildren().size()!=0) {
				menuString.append("<li>");
				menuString.append("<a href=\"#\" class=\"dropdown-toggle epochMenuParent\" ><i class=\""+tree.getIcon()+"\"></i>");
				menuString.append("<span class=\"menu-text\">");
				menuString.append(tree.getName());
				menuString.append("</span>");
				menuString.append("<b class=\"arrow fa fa-angle-down\"></b></a><ul  class=\"submenu\" >");
				menuString.append(getChildACEMenu(tree,tree.getChildren()));
				menuString.append("</ul></li>");
			}else {
				menuString.append(getLeafOfAceTree(tree));
			}
		}
		return menuString.toString();
	}
	
	private static String getLeafOfAceTree(ZrTreeBean tree){
		StringBuffer menuString = new StringBuffer();
		String icon = "default";
		menuString.append("<li iconCls=\"");
		menuString.append(icon);
		menuString.append("\"> <a class=\"epochMenu\" href=\"javascript:void(0)\" data-href=\""+tree.getLink()+"\""+"data-code=\""+tree.getCode()+ "\">");
		menuString.append("<span class=\"nav\" >");
		menuString.append(tree.getName());
		menuString.append("</span></a></li>");
		return menuString.toString();
	}
}
