package com.epoch.base.export;

import java.util.Map;

import com.epoch.base.function.BeetlFunctions;

public class ExportDictConvertor implements IExportConvertor {
    private Map<String, String> dictCodeMap;

    @Override
    public void init(String initParamStr) {
        this.dictCodeMap = BeetlFunctions.getDictCode(initParamStr);
    }

    @Override
    public Object convertFieldVal(Object row, String field, int rowIdx, int colIdx, Object val) {
        Object result = val;
        if (result != null && this.dictCodeMap != null) {
            result = this.dictCodeMap.get(result.toString());
            if (result == null) {
                result = val;
            }
        }
        return result;
    }
}
