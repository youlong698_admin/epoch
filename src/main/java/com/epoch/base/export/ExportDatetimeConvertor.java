package com.epoch.base.export;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ExportDatetimeConvertor implements IExportConvertor {
    private SimpleDateFormat format;

    @Override
    public void init(String initParamStr) {
        this.format = new SimpleDateFormat(initParamStr);
    }

    @Override
    public Object convertFieldVal(Object row, String field, int rowIdx, int colIdx, Object val) {
        Object result = val;
        if (result instanceof Date) {
            result = this.format.format((Date)val);
        }
        return result;
    }
}
