package com.epoch.base.export;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.epoch.base.util.JSONUtils;


public class ExportSelectDatetimeConvertor implements IExportConvertor {
    private static final String SELECT_CVT_PARAM = "selectCvtParam";
    private static final String DATETIME_CVT_PARAM = "datetimeCvtParam";
    private IExportConvertor selectCvt;
    private IExportConvertor datetimeCvt;

    @Override
    public void init(String initParamStr) {
        if (StringUtils.isNotEmpty(initParamStr)) {
            @SuppressWarnings("unchecked")
            Map<String, Object> params = JSONUtils.stringToCollect(initParamStr);
            String selectParam = (String)params.get(SELECT_CVT_PARAM);
            if (StringUtils.isNotEmpty(selectParam)) {
                this.selectCvt = new ExportSelectConvertor();
                this.selectCvt.init(selectParam);
            }
            String datetimeParam = (String)params.get(DATETIME_CVT_PARAM);
            if (StringUtils.isNotEmpty(datetimeParam)) {
                this.datetimeCvt = new ExportDatetimeConvertor();
                this.datetimeCvt.init(datetimeParam);
            }
        }
    }

    @Override
    public Object convertFieldVal(Object row, String field, int rowIdx, int colIdx, Object val) {
        Object result = val;
        if (this.selectCvt != null) {
            result = this.selectCvt.convertFieldVal(row, field, rowIdx, colIdx, result);
        }
        if (this.datetimeCvt != null) {
            result = this.datetimeCvt.convertFieldVal(row, field, rowIdx, colIdx, result);
        }
        return result;
    }
}
