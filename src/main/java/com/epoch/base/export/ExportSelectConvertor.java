package com.epoch.base.export;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.epoch.base.util.JSONUtils;

public class ExportSelectConvertor implements IExportConvertor {
    private Map<String, String> optionMap;

    @Override
    public void init(String initParamStr) {
        if (StringUtils.isNotEmpty(initParamStr)) {
            List<Select2Option> options = JSONUtils.toList(initParamStr, Select2Option.class);
            this.optionMap = new HashMap<String, String>(options.size());
            for (Select2Option opt : options) {
                this.optionMap.put(opt.getId(), opt.getText());
            }
        }
    }

    @Override
    public Object convertFieldVal(Object row, String field, int rowIdx, int colIdx, Object val) {
        Object result = val;
        if (result != null && this.optionMap != null) {
            result = this.optionMap.get(result.toString());
            if (result == null) {
                result = val;
            }
        }
        return result;
    }
}
