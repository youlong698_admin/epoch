package com.epoch.base.export;

import java.io.Serializable;

public class HeadMeta implements Serializable {
    private static final long serialVersionUID = 1L;
    private String field;
    private String title;
    private String exportCvt;
    private String cvtInitParam;

    public String getField() {
        return this.field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getExportCvt() {
        return this.exportCvt;
    }

    public void setExportCvt(String exportCvt) {
        this.exportCvt = exportCvt;
    }

    public String getCvtInitParam() {
        return this.cvtInitParam;
    }

    public void setCvtInitParam(String cvtInitParam) {
        this.cvtInitParam = cvtInitParam;
    }
}
