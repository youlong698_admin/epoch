package com.epoch.base.export;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;

public class XlsExporter extends ExcelExporter {
    @Override
    protected Workbook buildWorkbook() {
        return new HSSFWorkbook();
    }

    @Override
    public String getFileNameExt() {
        return ".xls";
    }

    @Override
    protected int maxRowCnt() {
        return 65536;
    }

    @Override
    protected int maxColCnt() {
        return 256;
    }
}
