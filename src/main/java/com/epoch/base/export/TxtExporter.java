package com.epoch.base.export;

public class TxtExporter extends CsvExporter {
    public TxtExporter() {
        super(",", false);
    }

    @Override
    public String getFileNameExt() {
        return ".txt";
    }
}
