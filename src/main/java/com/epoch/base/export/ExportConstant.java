package com.epoch.base.export;

import java.text.SimpleDateFormat;

public interface ExportConstant {
    public static final String FILE_TYPE_JSON = "json";
    public static final String FILE_TYPE_XML = "xml";
    public static final String FILE_TYPE_CSV = "csv";
    public static final String FILE_TYPE_TXT = "txt";
    public static final String FILE_TYPE_SQL = "sql";
    public static final String FILE_TYPE_DOC = "doc";
    public static final String FILE_TYPE_EXCEL = "excel";
    public static final String FILE_TYPE_XLSX = "xlsx";
    public static final String SERVER_EXPORT = "_serverExport";
    public static final String NO_PAGINATION = "_noPagination";
    public static final String EXPORT_RANGE = "_exportRange";
    public static final String EXPORT_FILE_TYPE = "_exportFileType";
    public static final String EXPORT_FILE_NAME = "_exportFileName";
    public static final String EXPORT_SHEET_NAME = "_exportSheetName";
    public static final String EXPORT_TABLE_NAME = "_exportTableName";
    public static final String EXPORT_HEAD_META = "_exportHeadMeta";
    public static final String EXPORT_DATA = "_exportData";
    public static final String EXPORT_CVTS = "_exportCvts";
    public static final String FILE_NAME_PRE = "export_";
    public static final String DEF_FILE_NAME = "export_result";
    public static final String DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final SimpleDateFormat DATETIME_FORMAT = new SimpleDateFormat(DATETIME_PATTERN);
}
