package com.epoch.base.export;

import java.io.Serializable;

public class Select2Option implements Serializable {
    private static final long serialVersionUID = 1L;
    private String id;
    private String text;

    public String getId() {
        return this.id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
