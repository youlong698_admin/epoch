package com.epoch.base.export;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;

public class DocExporter extends XmlExporter {
    private static final String TAG_HTML = "html";
    private static final String PROP_NSO = "xmlns:o";
    private static final String VAL_NSO = "urn:schemas-microsoft-com:office:office";
    private static final String PROP_NSX = "xmlns:x";
    private static final String VAL_NSX = "urn:schemas-microsoft-com:office:word";
    private static final String PROP_NS = "xmlns";
    private static final String VAL_NS = "http://www.w3.org/TR/REC-html40";
    private static final String TAG_META = "meta";
    private static final String PROP_HTTP_EQUIV = "http-equiv";
    private static final String VAL_HTTP_EQUIV = "content-type";
    private static final String PROP_CTNT = "content";
    private static final String VAL_CTNT = "application/vnd.ms-word; charset=UTF-8";
    private static final String TAG_HEAD = "head";
    private static final String TAG_STYLE = "style";
    private static final String TXT_STYLE = "br {mso-data-placement:same-cell;}";
    private static final String TAG_BODY = "body";
    private static final String TAG_TABLE = "table";
    private static final String TAG_THEAD = "thead";
    private static final String TAG_TBODY = "tbody";
    private static final String TAG_TR = "tr";
    private static final String TAG_TH = "th";
    private static final String TAG_TD = "td";

    @Override
    protected void doTextExport(BufferedWriter writer, Collection<?> data, List<HeadMeta> headMetas,
        Map<Integer, IExportConvertor> indexCvtorMap, Map<String, IExportConvertor> fieldCvtorMap,
        Map<String, Object> params) throws IOException, ExportException {
        writer.write(this.buildXmlTagStart(TAG_HTML, PROP_NSO, VAL_NSO, PROP_NSX, VAL_NSX, PROP_NS, VAL_NS));
        writer.write(this.buildXmlTagStart(TAG_HEAD));
        writer.write(this.buildXmlTagStart(TAG_META, PROP_HTTP_EQUIV, VAL_HTTP_EQUIV, PROP_CTNT, VAL_CTNT));
        writer.write(this.buildXmlElement(TAG_STYLE, TXT_STYLE));
        writer.write(this.buildXmlTagEnd(TAG_HEAD));
        writer.write(this.buildXmlTagStart(TAG_BODY));
        if (CollectionUtils.isNotEmpty(headMetas)) {
            writer.write(this.buildXmlTagStart(TAG_TABLE));
            writer.write(this.buildXmlTagStart(TAG_THEAD));
            writer.write(this.buildXmlTagStart(TAG_TR));
            for (HeadMeta meta : headMetas) {
                writer.write(this.buildXmlElement(TAG_TH, meta.getTitle()));
            }
            writer.write(this.buildXmlTagEnd(TAG_TR));
            writer.write(this.buildXmlTagEnd(TAG_THEAD));
            if (data != null) {
                writer.write(this.buildXmlTagStart(TAG_TBODY));
                int r = 0;
                for (Object rowData : data) {
                    writer.write(this.buildXmlTagStart(TAG_TR));
                    if (rowData != null) {
                        int cols = headMetas.size();
                        for (int c = 0; c < cols; c++) {
                            HeadMeta meta = headMetas.get(c);
                            String field = meta.getField();
                            Object val = this.obtainFieldVal(rowData, indexCvtorMap, fieldCvtorMap, field, r, c);
                            String valStr = null;
                            if (val instanceof Date) {
                                valStr = ExportConstant.DATETIME_FORMAT.format((Date)val);
                            } else if (val != null) {
                                valStr = val.toString();
                            }
                            writer.write(this.buildXmlElement(TAG_TD, valStr));
                        }
                    }
                    writer.write(this.buildXmlTagEnd(TAG_TR));
                    r++;
                }
                writer.write(this.buildXmlTagEnd(TAG_TBODY));
            }
            writer.write(this.buildXmlTagEnd(TAG_TABLE));
        }
        writer.write(this.buildXmlTagEnd(TAG_BODY));
        writer.write(this.buildXmlTagEnd(TAG_HTML));
    }

    @Override
    public String getFileNameExt() {
        return ".doc";
    }
}
