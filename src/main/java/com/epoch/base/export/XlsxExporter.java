package com.epoch.base.export;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class XlsxExporter extends ExcelExporter {
    @Override
    protected Workbook buildWorkbook() {
        return new XSSFWorkbook();
    }

    @Override
    public String getFileNameExt() {
        return ".xlsx";
    }

    @Override
    protected int maxRowCnt() {
        return 1048576;
    }

    @Override
    protected int maxColCnt() {
        return 16384;
    }
}
