package com.epoch.platform.sys.role.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "sys_role")
public class SysRole extends BaseModel<SysRole> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6069587822771851138L;

	public static final SysRole dao = new SysRole();
	
	public static final String ID = "ID"; // 主键id
	public static final String ROLE_CODE = "ROLE_CODE"; // 
	public static final String ROLE_NAME = "ROLE_NAME"; // 
	public static final String COMMENTS = "COMMENTS"; //  
	public static final String STATUS = "STATUS"; // 
	public static final String ROLE_TYPE = "ROLE_TYPE"; //
	
}
