package com.epoch.platform.sys.index;

import java.util.List;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.base.BaseController;
import com.epoch.base.enums.SysThemesEnum;
import com.epoch.base.enums.SysThemesUtil;
import com.epoch.base.shiro.ShiroKit;
import com.epoch.base.util.AjaxResult;
import com.epoch.platform.sys.info.dao.SysInfoDao;
import com.epoch.platform.sys.info.service.SysInfoService;
import com.epoch.platform.sys.menu.service.SysMenuService;

@ControllerBind(controllerKey = "/index")
public class IndexController extends BaseController{
	
	SysMenuService sysMenuService = new SysMenuService();
	
    public void index() {
    	String account = ShiroKit.getAccount();
    	String userName = ShiroKit.getUserName();
    	setAttr("userName", userName);
    	setAttr("account", account);
    	SysThemesEnum sysTheme = SysThemesUtil.getSysTheme(getRequest());
    	List<SysInfoDao> infoList = SysInfoService.me.getSysInfoListTwo();
    	setAttr("infoList", infoList);
    	setAttr("infoCount", SysInfoService.me.getSysInfoCount());
        this.render(sysTheme.getIndexPath());
    }
    
    public void main() {
    	this.render("/sys/main.html");
    }
    
    public void home() {
        this.render("/sys/main/home.html");
    }
    
    public void homePage() {
    	this.render("/sys/homePage.html");
    }
    
    
    public void pageIndexUI() {
        this.render("/index_v1.html");
    }
    
    public void changeStyle() {
    	String style = getPara("style");
    	setSessionAttr("EPOCHSTYLE", style);
    	setCookie("EPOCHSTYLE",style,3600*24*30);
    	renderJson(new AjaxResult().success("切换风格成功"));
    }
}
