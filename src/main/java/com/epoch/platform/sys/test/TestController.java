package com.epoch.platform.sys.test;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.base.BaseController;
import com.epoch.base.tree.ZrTreeBean;
import com.epoch.base.util.JSONUtils;
import com.epoch.platform.common.dao.SysAttachmentFiles;
import com.epoch.platform.common.service.CommonAttachService;
import com.epoch.platform.sys.menu.dao.SysMenu;
import com.epoch.platform.sys.org.service.SysOrgService;
import com.jfinal.core.JFinal;
import com.jfinal.kit.Kv;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.upload.UploadFile;

@ControllerBind(controllerKey = "/sys/test")
public class TestController extends BaseController{
	
	private SysOrgService sysOrgService = new SysOrgService();
	private CommonAttachService commonAttachService = new CommonAttachService();
	
	private static final String FILE_KEY_PRE = "_file_";
    private static final String COMMON_ATTACH_BUSINESS_ID = "99001";
    private static final String COMMON_ATTACH_BUSINESS_ID_A1 = "TEST_A1";
    private static final String COMMON_ATTACH_BUSINESS_ID_A2 = "TEST_A2";
    private static final String COMMON_ATTACH_BUSINESS_ID_B = "TEST_B";
    private static final String COMMON_ATTACH_BUSINESS_TYPE_B1 = "TEST_TYPE_B1";
    private static final String COMMON_ATTACH_BUSINESS_TYPE_B2 = "TEST_TYPE_B2";
	
	public void index() {
        this.render("/sys/test/test.html");
    }

    public void viewCommonIcon() {
        this.render("/sys/test/test_commonIcon.html");
    }

    public void viewTableTab() {
    	initTestDynaCols();
        this.render("/sys/test/test_tableTab.html");
    }

    public void viewTableEditTab() {
        this.render("/sys/test/test_tableEditTab.html");
    }
    
    public void viewUeditor() {
    	this.render("/sys/test/test_ueditor.html");
    }
    
    public void viewPublicTab() {
        this.initTestCommonTreePopup();
        this.initTestCommonAttach();
        this.initTestSelect2();
        this.initTestCommonAttachMore();
        this.render("/sys/test/test_publicTab.html");
    }
    
    public void viewUMeditor() {
    	this.render("/sys/test/test_umeditor.html");
    }
    
    private void initTestCommonTreePopup() {
        List<ZrTreeBean> orgList = this.sysOrgService.getAllTreeOrgList();
        this.setAttr("testTreeData", JSONUtils.toJSONString(orgList));
    }
    
    private void initTestCommonAttach() {
        this.setAttr("commonAttachBusinessId", TestController.COMMON_ATTACH_BUSINESS_ID);
    }
    
    private void initTestSelect2() {
        //本地组装数据
        List<Map<String, Object>> select = new ArrayList<>();
        Map<String, Object> map = new HashMap<>();
        map.put("id", "1");
        map.put("text", "我是第一个");
        select.add(map);
        Map<String, Object> map1 = new HashMap<>();
        map1.put("id", "2");
        map1.put("text", "我是第二个");
        select.add(map1);
        this.setAttr("selectData", JSONUtils.toJSONNoFeatures(select));
    }
    
    private void initTestCommonAttachMore() {
        this.setAttr("commonAttachBusinessIdA1", TestController.COMMON_ATTACH_BUSINESS_ID_A1);
        this.setAttr("commonAttachBusinessIdA2", TestController.COMMON_ATTACH_BUSINESS_ID_A2);
        this.setAttr("commonAttachBusinessIdB", TestController.COMMON_ATTACH_BUSINESS_ID_B);
        this.setAttr("commonAttachBusinessTypeB1", TestController.COMMON_ATTACH_BUSINESS_TYPE_B1);
        this.setAttr("commonAttachBusinessTypeB2", TestController.COMMON_ATTACH_BUSINESS_TYPE_B2);
    }
    
    public void select2() {
        List<SysMenu> sysMenulist = SysMenu.dao.find("select id,menu_name from sys_menu where parent_id= '0'");
        List<Map<String, Object>> select = new ArrayList<>();
        for (SysMenu sysMenu : sysMenulist) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", sysMenu.get(SysMenu.ID));
            map.put("text", sysMenu.get(SysMenu.MENU_NAME));
            select.add(map);
        }
        this.renderJson(select);
    }
    
    public void testTableNewSearchAjax() {
        Kv param = this.commonParams();
        SqlPara sqlPara = Db.getSqlPara("test.test", param);
        Page<SysMenu> sysMenuList = SysMenu.dao.paginate(sqlPara, param);
        this.renderJson(sysMenuList);
    }
    
    public void initTestDynaCols() {
        List<Map<String, String>> cols = new ArrayList<Map<String, String>>(8);
        Map<String, String> col = null;
        for (int c = 0; c < 8; c++) {
            String cnum = String.valueOf(c + 1);
            col = new HashMap<String, String>();
            col.put("title", "Col Head " + cnum);
            col.put("field", "fieldname" + cnum);
            cols.add(col);
        }
        this.setAttr("dynaCols", cols);
    }
    
    public void testDynaColsTableAjax() {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>(2);
        Map<String, String> row = null;
        for (int r = 0; r < 2; r++) {
            String rnum = String.valueOf(r + 1);
            row = new HashMap<String, String>(8);
            for (int c = 0; c < 8; c++) {
                String cnum = String.valueOf(c + 1);
                row.put("rowId", rnum);
                row.put("fieldname" + cnum, "field value " + rnum + " - " + cnum);
            }
            list.add(row);
        }
        Page<Map<String, String>> page = new Page<Map<String, String>>(list, 1, 10, 1, 2);
        this.renderJson(page);
    }
    
    public void wfCreate() {
        this.render("/sys/test/test_wf.html");
    }
    
    public void testUpload() {
        Map<String, Object> result = new HashMap<String, Object>();
        UploadFile file = null;
        try {
            file = this.getFile();
        } catch (RuntimeException e) {
            //TestController.LOGGER.error("get upload file failed", e);
            throw e;
        }
        if (file != null) {
            String fileName = file.getFileName();
            try {
                String fileKey = TestController.FILE_KEY_PRE + URLEncoder.encode(fileName, "UTF-8");
                //TestController.LOGGER.debug("fileKey: {}", fileKey);
                String downloadUri = "/sys/test/testDownload?fileKey=" + fileKey;
                result.put("downloadUri", downloadUri);
            } catch (UnsupportedEncodingException e) {
                //TestController.LOGGER.error("encode file name failed", e);
            }
        }
        this.renderJson(result);
    }
    
    public void testDownload() {
        String webRoot = PathKit.getWebRootPath();
        String baseUploadPath = JFinal.me().getConstants().getBaseUploadPath();
        String fileKey = this.getPara("fileKey");
        if (StringUtils.isNotEmpty(fileKey)) {
            String fileName = fileKey.substring(TestController.FILE_KEY_PRE.length());
            String filePath = webRoot + File.separator + baseUploadPath + File.separator + fileName;
            //TestController.LOGGER.debug("filePath: {}", filePath);
            this.renderFile(new File(filePath), fileName);
            return;
        }
        this.renderError(404);
    }
    
    public void testCommonAttachSave() {
        String attachJson = this.getPara("attachJson");
        //TestController.LOGGER.debug(attachJson);
        Map<String, Object> result = new HashMap<String, Object>();
        try {
            List<SysAttachmentFiles> list = this.commonAttachService.saveAttachments(TestController.COMMON_ATTACH_BUSINESS_ID, attachJson);
            result.put("list", list);
        } catch (IOException e) {
            //TestController.LOGGER.error("failed to save attachment.", e);
        }
        this.renderJson(result);
    }
    
    public void testCustomUrlPopup() {
        Kv popupParams = this.commonPopupParams();
        // 此参数代表commonPopup使用方设置的paginate参数，标明是否启用table的分页功能
        // 若此popup公共弹窗可支持分页开关控制，则需要根据此参数分别查询分页数据或全部数据
        // 若不允许使用popup的开发者控制是否分页，则可以不考虑此参数直接进行对应查询
        Boolean paginate = this.getParaToBoolean("_cmnPop_paginate");
        // 自由查询数据，此处可以调用接口或其它任意方式查询数据
        Page<SysMenu> result = this.searchData(popupParams, Boolean.TRUE.equals(paginate));
        this.renderJson(result);
    }
    
    private Page<SysMenu> searchData(Kv popupParams, boolean paginate) {
        // 此方法调用sql查询本地数据，用来模拟调用接口查询数据方式
        Page<SysMenu> result = null;
        if (paginate) {
            result = SysMenu.dao.paginate(Db.getSqlPara("test.test", popupParams), popupParams);
        } else {
            List<SysMenu> list = SysMenu.dao.find(Db.getSqlPara("test.test", popupParams));
            int rows = list == null ? 0 : list.size();
            result = new Page<SysMenu>(list, 1, rows, 1, rows);
        }
        return result;
    }
}
