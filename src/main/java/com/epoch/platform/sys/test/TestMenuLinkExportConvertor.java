package com.epoch.platform.sys.test;

import org.apache.commons.lang3.StringUtils;

import com.epoch.base.export.IExportConvertor;
import com.epoch.platform.sys.menu.dao.SysMenu;

public class TestMenuLinkExportConvertor implements IExportConvertor {
    @Override
    public void init(String initParamStr) {
    }

    @Override
    public Object convertFieldVal(Object row, String field, int rowIdx, int colIdx, Object val) {
        Object result = val;
        if (row instanceof SysMenu) {
            String menuType = ((SysMenu)row).get(SysMenu.MENU_TYPE);
            result = ("outurl".equals(menuType) ? "外部: " : "url".equals(menuType) ? "系统: " : "")
                + StringUtils.trimToEmpty((String)val);
        }
        return result;
    }
}
