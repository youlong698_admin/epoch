package com.epoch.platform.sys.serialnumber.dao;

import java.io.Serializable;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "SYS_SERIAL_NUMBER")
public class SerialNumber extends BaseModel<SerialNumber> implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final SerialNumber dao = new SerialNumber();
	
	public static final String ID = "ID"; // 主键id
	public static final String RULE_CODE = "RULE_CODE"; // 规则编码
	public static final String RULE_NAME = "RULE_NAME"; // 规则名称
	public static final String RULE_TYPE = "RULE_TYPE"; // 流水号重置规则0,永远都不重置1,按年重置,2,按月重置,3按日重置（使用率最高，放在最前）,4按小时重置,5按分重置,6按秒重置
	public static final String CUR_TIME = "CUR_TIME"; // 数据库中当前时间
	public static final String SERIAL_LENGTH = "SERIAL_LENGTH"; // 流水号长度
	public static final String CURRENT_VALUE = "CURRENT_VALUE"; // 当前值
	public static final String INIT_VALUE = "INIT_VALUE"; // 初始值
	public static final String STEP = "STEP"; // 步长，增长幅度

	public static final String SYSTEM_TIME = "SYSTEM_TIME"; // 系统当前时间
	public static final String PREFIX = "PREFIX"; // 前缀
	public static final String SPLIT = "SPLIT"; // 分割符
}
