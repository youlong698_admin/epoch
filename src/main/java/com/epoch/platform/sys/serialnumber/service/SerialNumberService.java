package com.epoch.platform.sys.serialnumber.service;

import com.epoch.platform.sys.serialnumber.dao.SerialNumber;
import com.jfinal.aop.Before;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.plugin.activerecord.tx.Tx;

public class SerialNumberService {
	
	@Before(Tx.class)
    public boolean saveSeriNum(SerialNumber model) {
        return model.saveOrUpdate();
    }

    @Before(Tx.class)
    public boolean delSeriNums(String[] ids) {
        boolean success = false;
        if (ids.length == 1) {
            success = SerialNumber.dao.deleteById(ids[0]);
        } else {
            SqlPara sqlPara = Db.getSqlPara("systemManage.delSerialNumbers", Kv.by("ids", ids));
            int row = Db.update(sqlPara.getSql(), sqlPara.getPara());
            success = row > 0;
        }
        return success;
    }
}
