package com.epoch.platform.sys.rolemenu.controller;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.base.BaseController;
import com.epoch.base.tree.ZrTreeBean;
import com.epoch.base.util.AjaxResult;
import com.epoch.base.util.JSONUtils;
import com.epoch.platform.sys.menu.dao.SysMenu;
import com.epoch.platform.sys.menu.service.SysMenuService;
import com.epoch.platform.sys.role.dao.SysRole;
import com.epoch.platform.sys.rolemenu.dao.SysRoleMenu;
import com.jfinal.aop.Before;
import com.jfinal.kit.JsonKit;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.plugin.activerecord.tx.Tx;

@ControllerBind(controllerKey = "/sys/rolemenu")
public class SysRoleMenuController extends BaseController{
	
	private AjaxResult ajaxResult = new AjaxResult();
	
	SysMenuService sysMenuService = new SysMenuService();
	
	public void index() {
		// 获取父类节点parentId
        String parentId = getPara("parentId");
        setAttr("parentId", parentId);
        String parentName = getPara("parentName");
        if(null != parentName) {
        	setAttr("parentName", parentName);
        }else {
        	setAttr("parentName", "菜单组织树");
        }
        // 保存一个AjaxResult对象进入域中
        setAttr("treeNodeJson",
                JSONUtils.toJSONString(sysMenuService.getAllTreeMenuList()));
        roleMenu();
        render("/sys/rolemenu/sys_role_menu.html");
    }
	
	public void roleMenu() {
		Kv param = Kv.create();
		param.put("role_type", "SYSTEM");
		SqlPara sqlPara = Db.getSqlPara("systemManage.findSysRole", param);
		List<SysRole> parentList = SysRole.dao.find(sqlPara);
		List<ZrTreeBean> roleTree = new ArrayList<ZrTreeBean>();
		if (CollectionUtils.isNotEmpty(parentList)) {
            for (SysRole role : parentList) {
                ZrTreeBean tree = new ZrTreeBean();
                tree.setId(role.getStr("id"));
                tree.setParentId(role.getStr("parent_id"));
                tree.setName(role.getStr("role_name"));
                tree.setComments(role.getStr("comments"));
                roleTree.add(tree);
            }
        }
		
		// 获取父类节点parentId
        String parentId = getPara("parentId");
        setAttr("parentId", parentId);
        String parentName = getPara("parentName", "角色树");
        setAttr("parentName", parentName);
        setAttr("roleTree", roleTree);
        String json = JsonKit.toJson(roleTree);
        setAttr("jsonStr", json);
	}
	
	@Before(Tx.class)
    public void save() {
        try {
            String treeRoleId = getPara("treeIdRole").equals("null") ? null
                    : getPara("treeIdRole");
            List<String> menuIds = JSONUtils.toList(getPara("menuIds"), String.class);
            if (treeRoleId != null) {
                Db.update("DELETE FROM SYS_ROLE_MENU WHERE ROLE_ID =  '" + treeRoleId+"'");
                for (int i = 0; i < menuIds.size(); i++) {
                    String menuId = menuIds.get(i).toString();
                    SysMenu sysMenu = SysMenu.dao
                            .findFirst("SELECT * FROM SYS_MENU WHERE ID=  '" + menuId+"'");
                    if (sysMenu != null) {
                        SysRoleMenu roleMenu = new SysRoleMenu();
                        roleMenu.set(SysRoleMenu.ROLE_ID, treeRoleId);
                        roleMenu.set(SysRoleMenu.MENU_ID, menuId);
                        roleMenu.saveOrUpdate();
                    }
                }
            }
            ajaxResult.success("保存成功!");
           
        } catch (Exception e) {
            ajaxResult.addError("保存失败!");
        }
        renderJson(ajaxResult);
    }
	
	public void getMenuIdByRoleId() {
        String treeRoleId = getPara("treeIdRole").equals("null") ? null : getPara("treeIdRole");
        List<SysRoleMenu> roleMenuList = SysRoleMenu.dao
                .find("SELECT MENU_ID FROM SYS_ROLE_MENU WHERE ROLE_ID= '"
                        + treeRoleId+"'");
        if (roleMenuList != null && !roleMenuList.isEmpty()) {
            renderText(JsonKit.toJson(roleMenuList));
        } else {
            renderText("");
        }
    }
}
