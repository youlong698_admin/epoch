package com.epoch.platform.sys.rolemenu.service;

import java.util.ArrayList;
import java.util.List;

import com.epoch.platform.sys.menu.dao.SysMenu;
import com.epoch.platform.sys.rolemenu.dao.SysRoleMenu;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;

public class SysRoleMenuService {
	
	public List<Integer> getSysMenuIdList(String roleId){
		List<Integer> roleIdList = new ArrayList<Integer>();
		Kv params = Kv.create();
		params.set("roleId",Integer.parseInt(roleId));
		SqlPara sqlPara = Db.getSqlPara("systemManage.findSysRoleMenuList",params);
		List<SysRoleMenu> sysRoleMenuList = SysRoleMenu.dao.find(sqlPara);
		for (SysRoleMenu sysRoleMenu : sysRoleMenuList) {
			roleIdList.add(sysRoleMenu.getInt(SysRoleMenu.MENU_ID));
		}
		return roleIdList;
	}
	
	public SysRoleMenu findRoleParentMenu(Integer parentId) {
		Kv params = Kv.create();
		params.set("parent_id",parentId);
		SqlPara sqlPara = Db.getSqlPara("systemManage.findRoleParentMenu",params);
		SysRoleMenu sysMenu = SysRoleMenu.dao.findFirst(sqlPara);
		return sysMenu;
	}
}
