package com.epoch.platform.sys.rolemenu.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "sys_role_menu")
public class SysRoleMenu extends BaseModel<SysRoleMenu>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final SysRoleMenu dao = new SysRoleMenu();
	
	public static final String ID = "ID";
	public static final String ROLE_ID = "ROLE_ID";
	public static final String MENU_ID = "MENU_ID";
}
