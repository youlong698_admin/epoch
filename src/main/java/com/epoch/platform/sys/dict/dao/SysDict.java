package com.epoch.platform.sys.dict.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "sys_dict")
public class SysDict extends BaseModel<SysDict>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final SysDict dao = new SysDict();
	
	public static final String ID = "ID";
	public static final String DICT_CODE = "DICT_CODE";//字典编码
	public static final String DICT_NAME = "DICT_NAME";//字典名称
	public static final String DICT_COMMENTS = "DICT_COMMENTS";//字典描述
	public static final String ENABLED = "ENABLED";//是否启用
	public static final String STATUS="STATUS";//状态
	
	public static final String ITEM_CODE = "ITEM_CODE"; // 字典项编码
	public static final String ITEM_VALUE = "ITEM_VALUE"; // 字典项值
	
	public static final String ITEM_FLAG = "ITEM_FLAG"; // 字典项是否启用 Y/N
	
	public static final String HEAD_FLAG = "HEAD_FLAG"; // 是否为头  Y/N
}
