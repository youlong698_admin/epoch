package com.epoch.platform.sys.info.controller;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.base.BaseController;
import com.epoch.base.util.AjaxResult;
import com.epoch.platform.sys.info.dao.SysInfoDao;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

@ControllerBind(controllerKey = "/sys/info")
public class SysInfoController extends BaseController {

    private AjaxResult ajaxResult = new AjaxResult();

    public void index() {
        render("/sys/info/sys_info_tab.html");
    }

    public void listUI() {
        render("/sys/info/sys_info_list.html");
    }

    public void suvUI() {
        String id = getPara("ID");
        SysInfoDao sysInfoDao = new SysInfoDao();
        if (StringUtils.isNotBlank(id)) {
            sysInfoDao = SysInfoDao.dao.findFirst(Db.getSql("systemManage.findInfo"), id);
        }
        setAttr("sysInfoDao", sysInfoDao);
        render("/sys/info/sys_info_edit.html");
    }

    public void findInfoListAjax() {
        Kv param = commonParams();
        SqlPara sqlPara = Db.getSqlPara("systemManage.findInfoList", param);
        Page<SysInfoDao> infoList = SysInfoDao.dao.paginate(sqlPara, param);
        renderJson(infoList);
    }
    
    public void findInfoListNew() {
        Kv param = commonParams();
        SqlPara sqlPara = Db.getSqlPara("systemManage.findInfoListTwo", param);
        Page<SysInfoDao> infoList = SysInfoDao.dao.paginate(sqlPara, param);
        renderJson(infoList);
    }

    public void saveInfo() {
        try {
            AjaxResult ajaxResult = new AjaxResult();
            SysInfoDao sysInfoDao = getModel(SysInfoDao.class, "sysInfoDao");
            if (sysInfoDao.saveOrUpdate()) {
                ajaxResult.setData(sysInfoDao);
            }
        } catch (Exception e) {
        }
        renderJson(ajaxResult.success("保存成功"));
    }

    public void deleteInfo() {
        String[] ids = getParaValues("ids[]");
        try {
            if (ArrayUtils.isNotEmpty(ids)) {
                for (String id : ids) {
                	SysInfoDao.dao.deleteById(id);
                }
            }
            ajaxResult.success("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        renderJson(ajaxResult);

    }
}
