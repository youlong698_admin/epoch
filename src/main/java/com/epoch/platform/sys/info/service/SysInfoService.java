package com.epoch.platform.sys.info.service;

import java.util.List;

import com.epoch.platform.sys.info.dao.SysInfoDao;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.SqlPara;

public class SysInfoService {
	
	public static final SysInfoService me =  new SysInfoService();
	
	public List<SysInfoDao> getSysInfoListTwo() {
		Kv param = Kv.create();
        SqlPara sqlPara = Db.getSqlPara("systemManage.findInfoListTwo", param);
        List<SysInfoDao> infoList = SysInfoDao.dao.find(sqlPara);
        return infoList;  
	}
	
	public Integer getSysInfoCount() {
		Kv param = Kv.create();
        SqlPara sqlPara = Db.getSqlPara("systemManage.findInfoListCount", param);
        SysInfoDao info = SysInfoDao.dao.findFirst(sqlPara);
        return info.getInt("count(*)");
	}
}
