package com.epoch.platform.sys.info.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(key="ID", table="SYS_INFO")
public class SysInfoDao extends BaseModel<SysInfoDao>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static SysInfoDao dao = new SysInfoDao();
	 
	public static final String ID = "ID"; // 
	public static final String INFO_TITLE = "INFO_TITLE"; // 消息标题
	public static final String INFO_TYPE = "INFO_TYPE"; // 消息类型  1公告
	public static final String INFO_DESC = "INFO_DESC"; // 消息描述
	public static final String STATUS = "STATUS"; //消息状态，可用，不可用
}
