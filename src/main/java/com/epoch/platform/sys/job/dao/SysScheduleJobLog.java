package com.epoch.platform.sys.job.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "SYS_SCHEDULE_JOB_LOG", key = "ID")
public class SysScheduleJobLog extends BaseModel<SysScheduleJobLog>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final SysScheduleJobLog dao = new SysScheduleJobLog();
	
	public static final String ID = "ID"; // 任务ID(主键)
    public static final String LAST_SUCCESS_PLAN_TIME = "LAST_SUCCESS_PLAN_TIME"; // 上次成功执行的计划时间
    public static final String LAST_SUCCESS_ACT_TIME = "LAST_SUCCESS_ACT_TIME"; // 上次成功执行的触发时间
    public static final String CREATED_BY = "CREATED_BY"; // 创建人ID
    public static final String CREATED_DATE = "CREATED_DATE"; // 创建时间
    public static final String CREATED_DEPT = "CREATED_DEPT"; // 创建部门ID
    public static final String UPDATED_BY = "UPDATED_BY"; // 更新人ID
    public static final String UPDATED_DATE = "UPDATED_DATE"; // 更新时间
}
