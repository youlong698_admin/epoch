package com.epoch.platform.sys.log.controller;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.base.BaseController;
import com.epoch.platform.sys.log.dao.SysLog;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;

@ControllerBind(controllerKey = "/sys/log")
public class SysLogController extends BaseController {
	
	public void index() {
        this.render("/sys/log/sys_log_tab.html");
    }
	
	public void listUI() {
        this.render("/sys/log/sys_log_list.html");
    }
	
	public void findListAjax() {
        Kv param = commonParams();
        SqlPara sqlPara = Db.getSqlPara("systemManage.findSysLogByParam", param);
        Page<SysLog> sysLogPage = SysLog.dao.paginate(sqlPara, param);
        renderJson(sysLogPage);
    }
}
