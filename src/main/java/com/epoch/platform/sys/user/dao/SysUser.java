package com.epoch.platform.sys.user.dao;

import com.epoch.base.annotation.ModelBind;
import com.epoch.base.model.BaseModel;

@ModelBind(table = "sys_user")
public class SysUser extends BaseModel<SysUser> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final SysUser dao = new SysUser();
	
	public static final String ID = "ID"; // 主键id
	public static final String ACCOUNT = "ACCOUNT"; // 用户名
	public static final String PASSWORD = "PASSWORD"; // 密码
	public static final String USER_NAME = "USER_NAME"; // 姓名
	public static final String EMAIL = "EMAIL"; // 邮箱
	public static final String EMPLOYEE_NUMBER = "EMPLOYEE_NUMBER"; // 员工编号
	public static final String PHONE_NUMBER = "PHONE_NUMBER"; // 联系方式
	public static final String POSITION_LEVEL = "POSITION_LEVEL"; // 职级
	public static final String TITLE = "TITLE"; // 职位
	public static final String COMPANY_ID = "COMPANY_ID"; // 所属公司id
	public static final String BIRTHDAY = "BIRTHDAY"; // 生日
	public static final String IS_LOCKED = "IS_LOCKED"; // 是否锁定 Y/N
	public static final String IS_USED = "IS_USED"; // 是否过期
	public static final String DEPT_ID = "DEPT_ID"; // 更新时间 yyyy-MM-dd HH:mm:ss
	public static final String DEPT_NAME = "DEPT_NAME"; // 更新时间 yyyy-MM-dd HH:mm:ss
	public static final String COMPANY_NAME = "COMPANY_NAME"; // 更新时间 yyyy-MM-dd HH:mm:ss
	
	public static final String MANAGER_CODE = "MANAGER_CODE";
	public static final String DEPT_WF_ROLES = "DEPT_WF_ROLES"; // 部门流程角色
	public static final String USER_TYPE = "USER_TYPE"; // 用户类型
}
