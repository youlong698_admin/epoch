package com.epoch.platform.oss.controller;

import java.io.File;

import org.apache.commons.lang3.ArrayUtils;

import com.epoch.base.annotation.ControllerBind;
import com.epoch.base.base.BaseController;
import com.epoch.base.util.AjaxResult;
import com.epoch.platform.oss.cloud.OSSFactory;
import com.epoch.platform.oss.dao.SysOss;
import com.epoch.platform.sys.info.dao.SysInfoDao;
import com.jfinal.kit.Kv;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.SqlPara;
import com.jfinal.upload.UploadFile;
/**
 * 
* <p>Title: SysConfigController.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月15日
 */
@ControllerBind(controllerKey = "/sys/oss")
public class SysOssController extends BaseController{
	
	private AjaxResult ajaxResult = new AjaxResult();
	
	public void index() {
		render("/sys/oss/sys_oss_tab.html");
	}
	
	public void listUI() {
		render("/sys/oss/sys_oss_list.html");
	}
	
	public void findSysOSSListAjax() {
		Kv param = commonParams();
		SqlPara sqlPara = Db.getSqlPara("oss.findSysOssFileList", param);
		Page<SysOss> infoList = SysOss.dao.paginate(sqlPara, param);
		renderJson(infoList);
	}
	
	public void upload() {
		UploadFile uploadFile = getFile();
		File file = uploadFile.getFile();
		if(null == file) {
			renderJson(ajaxResult.addError("上传文件不能为空"));
		}
		//上传文件
        String url = OSSFactory.build().upload(file);
        SysOss sysOss = new SysOss();
        sysOss.set(SysOss.URL, url);
        sysOss.save();
        renderJson(ajaxResult.success(url));
	}
	
	public void delete() {
        String[] ids = getParaValues("ids[]");
        try {
            if (ArrayUtils.isNotEmpty(ids)) {
                for (String id : ids) {
                	SysOss.dao.deleteById(id);
                }
            }
            ajaxResult.success("删除成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
        renderJson(ajaxResult);

    }
}
