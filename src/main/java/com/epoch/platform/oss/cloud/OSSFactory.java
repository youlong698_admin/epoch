package com.epoch.platform.oss.cloud;
/**
 * 
* <p>Title: OSSFactory.java</p>  
* <p>Description: </p>  
* @author 张清磊
* @QQ 544188838
* @date 2018年9月15日
 */
public final class OSSFactory {
	
	public static CloudStorageService build(){
		return new AliyunCloudStorageService();
	}
}
