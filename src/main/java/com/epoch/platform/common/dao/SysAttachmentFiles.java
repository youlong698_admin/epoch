package com.epoch.platform.common.dao;

import com.epoch.base.model.BaseModel;
import com.epoch.base.annotation.ModelBind;

@ModelBind(table = "SYS_ATTACHMENT_FILES", key = "ATTACHMENT_FILE_ID")
public class SysAttachmentFiles extends BaseModel<SysAttachmentFiles> {
	
    private static final long serialVersionUID = 1L;
    public static final SysAttachmentFiles dao = new SysAttachmentFiles();
    public static final String ATTACHMENT_FILE_ID = "ATTACHMENT_FILE_ID"; // 附件ID
    public static final String BUSINESS_ID = "BUSINESS_ID"; // 业务ID
    public static final String BUSINESS_TYPE = "BUSINESS_TYPE"; // 业务子类型
    public static final String LOCAL_UPLOAD_PATH = "LOCAL_UPLOAD_PATH"; // 本地上传路径
    public static final String SERVER_UPLOAD_PATH = "SERVER_UPLOAD_PATH"; // 服务器上传路径
    public static final String FILE_ORIGINAL_NAME = "FILE_ORIGINAL_NAME"; // 原始文件名
    public static final String FILE_NEW_NAME = "FILE_NEW_NAME"; // 新文件名
    public static final String FILE_SIZE = "FILE_SIZE"; // 文件大小
    public static final String FILE_EXT_NAME = "FILE_EXT_NAME"; // 文件扩展名
    public static final String DESCRIPTION = "DESCRIPTION"; // 描述
    public static final String CUST_TEXT01 = "CUST_TEXT01"; // 扩展文本01
    public static final String CUST_TEXT02 = "CUST_TEXT02"; // 扩展文本02
    public static final String CUST_TEXT03 = "CUST_TEXT03"; // 扩展文本03
    public static final String CUST_TEXT04 = "CUST_TEXT04"; // 扩展文本04
    public static final String CUST_TEXT05 = "CUST_TEXT05"; // 扩展文本05
    public static final String CUST_TEXT06 = "CUST_TEXT06"; // 扩展文本06
    public static final String CUST_TEXT07 = "CUST_TEXT07"; // 扩展文本07
    public static final String CUST_TEXT08 = "CUST_TEXT08"; // 扩展文本08
    public static final String CUST_TEXT09 = "CUST_TEXT09"; // 扩展文本09
    public static final String CUST_TEXT10 = "CUST_TEXT10"; // 扩展文本10
    public static final String CUST_NUMBER01 = "CUST_NUMBER01"; // 扩展数值1
    public static final String CUST_NUMBER02 = "CUST_NUMBER02"; // 扩展数值2
    public static final String CUST_DATE01 = "CUST_DATE01"; // 扩展日期1
    public static final String CUST_DATE02 = "CUST_DATE02"; // 扩展日期2
    public static final String CREATE_BY = "CREATE_BY"; // 创建人ID
    public static final String CREATE_DATE = "CREATE_DATE"; // 创建时间
    public static final String CREATE_DEPT = "CREATE_DEPT"; // 创建部门ID
    public static final String UPDATE_BY = "UPDATE_BY"; // 更新人ID
    public static final String UPDATE_DATE = "UPDATE_DATE"; // 更新时间
}
