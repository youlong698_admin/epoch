@var id = tag.id;
@var queryUrl = tag.queryUrl!"";
@var rowId = tag.rowId!"id";
@var tableName = tag.tableName!"";
@var height = tag.height!"undefined";
@var editable = tag.editable!"false";
@var filter = tag.filter!("true" == editable ? "false" : "true");
@var paginate = tag.paginate!("true" == editable ? "false" : "true");
@var showRefresh = tag.showRefresh!"true";
@var showToggle = tag.showToggle!"true";
@var showClearFilters = tag.showClearFilters!("true" == editable ? "false" : "true");
@var showColumns = tag.showColumns!("true" == editable ? "false" : "true");
@var minColumnCnt = tag.minColumnCnt!"1";
@var autoAdjustWidth = tag.autoAdjustWidth!"true";
@var autoInit = tag.autoInit!"true";
@var showExport = tag.showExport!("true" == editable ? "false" : "true"); 
@var exportMode = tag.exportMode!"auto";
@var exportDataRange = tag.exportDataRange!"['basic','all','selected']"; 
@var exportTypes = tag.exportTypes!"['excel','csv']";
@var exportIgnoreCols = tag.exportIgnoreCols!"[]";
@var exportFileName = tag.exportFileName!"result";
@var exportSheetName = tag.exportSheetName!"Sheet1";
@var exportTableName = tag.exportTableName!"myTableName";
@var exportBasicText = tag.exportBasicText!'导出当前页';
@var exportAllText = tag.exportAllText!'导出全部';
@var exportSelectedText = tag.exportSelectedText!'导出选中行';
@var exportOptions = tag.exportOptions!"{}";

@var defTipColHead = tag.defTipColHead!"true";
@var selection = tag.selection!"";
@var queryParam = tag.queryParam!"undefined";

@var rowStyle = tag.rowStyle!"undefined";
@var rowAttributes = tag.rowAttributes!"undefined";

@var onBeforeExport = tag.onBeforeExport!"undefined";
@var onBeforeDelRows = tag.onBeforeDelRows!"undefined";
@var onBeforeDelRow = tag.onBeforeDelRow!"undefined";
@var onDelRow = tag.onDelRow!"undefined";
@var onDelRows = tag.onDelRows!"undefined";
@var onFirstLoadSuccess = tag.onFirstLoadSuccess!"undefined";
@var onLoadSuccess = tag.onLoadSuccess!"undefined";
@var onLoadError = tag.onLoadError!"undefined";
@var onRowEditing = tag.onRowEditing!"undefined";
@var onRowConfirmEdit = tag.onRowConfirmEdit!"undefined";
@var onRowCancelEdit = tag.onRowCancelEdit!"undefined";
@var onBeforeAddRow = tag.onBeforeAddRow!"undefined";
@var onAddRow = tag.onAddRow!"undefined";

@//toolbar按钮
@var newBtn = tag.newBtn!"false";
@var editBtn = tag.editBtn! "false";
@var delBtn = tag.delBtn!"false";
@var advBtn = tag.advBtn!"false";
@var saveBtn = tag.saveBtn!"false";
@var cancelBtn = tag.cancelBtn!"false";
@var addRowBtn = tag.addRowBtn!("true" == editable ? "true" : "false");
@var delRowBtn = tag.delRowBtn!("true" == editable ? "true" : "false");
@var newBtnText = tag.newBtnText!'新增';
@var editBtnText = tag.editBtnText!'编辑';
@var delBtnText = tag.delBtnText!"批量删除";
@var advBtnText = tag.advBtnText!"高级查询";
@var saveBtnText = tag.saveBtnText!"保存";
@var cancelBtnText = tag.cancelBtnText!"取消"; 
@var addRowBtnText = tag.addRowBtnText!"添加行";
@var delRowBtnText = tag.delRowBtnText!"删除行";

@var onDelClick = tag.onDelClick!"undefined";
@var onAddClick = tag.onAddClick!"undefined";

@var alterTrs = tag.alterTrs; //表格行
@var alterMoreBtns = tag.alterMoreBtns; //更多按钮
@var alterEditDetail = tag.alterEditDetail; //详细编辑域

@var children = tag.children;
@var childrenMap = tag.childrenMap; //页面标签组获取
@var trs = alterTrs!childrenMap['tr']; //行信息
@var moreBtns = alterMoreBtns!childrenMap['moreBtn']; //设置更多按键
@var editDetails = alterEditDetail!childrenMap['editDetail'];
@var selections = tag.selections!"undefined";//初始化选中的ID,可以接受数组或者字符串
@var selectText = tag.selectText!"undefined";//初始化选中的文本,可以接受数组或者字符串
@var selectName = tag.selectName!"undefined";//要显示的列名
@var selectRowJson = tag.selectRowJson!"undefined";//要显示的列名
@var checkType = tag.checkType!"checkbox";//勾选类型
@var saveId = tag.saveId!"undefined";
<input id="${id}_queryUrl" type="hidden" value="${queryUrl}" />
<div id="${id}_toolbar" class="row visa-row">
	<div class="col-md-12">
		@if(tableName != ''){
	    	<span class="tablename">${tableName}</span>
	    @}
		<div class="globalbtn">
			@var moreBtnGrp = null;
		    @var moreBtnMap = lisFun.buildMoreBtnMap(moreBtns, 7);
		    @moreBtnGrp = moreBtnMap["0"];
		    @if(moreBtnGrp != null){
		        @for(moreBtn in moreBtnGrp){
		            ${moreBtn.body}
		        @}
    		@}
			@if("true" == newBtn){
			    <button id="${id}_table_add" type="button" class="btn btn-primary btn-sm">
			        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>${newBtnText}
			    </button>
		    @}
		    @moreBtnGrp = moreBtnMap["1"];
		    @if(moreBtnGrp != null){
		        @for(moreBtn in moreBtnGrp){
		            ${moreBtn.body}
		        @}
		    @}
		    @if("true" == editBtn){
			    <button id="${id}_table_edit" type="button" class="btn btn-success btn-sm">
			        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>${editBtnText}
			    </button>
		    @}
		    @moreBtnGrp = moreBtnMap["2"];
		    @if(moreBtnGrp != null){
		        @for(moreBtn in moreBtnGrp){
		            ${moreBtn.body}
		        @}
		    @}
		    @if("true" == delBtn){
		    	<button id="${id}_table_delete" type="button" class="btn btn-danger btn-sm">
			        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>${delBtnText}
			    </button>
		    @}
		    @moreBtnGrp = moreBtnMap["3"];
		    @if(moreBtnGrp != null){
		        @for(moreBtn in moreBtnGrp){
		            ${moreBtn.body}
		        @}
		    @}
		    @if("true" == addRowBtn){
			    <button id="${id}_dyn_add" type="button" class="btn btn-primary btn-sm">
			        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>${addRowBtnText}
			    </button>
		    @}
		    @moreBtnGrp = moreBtnMap["4"];
		    @if(moreBtnGrp != null){
		        @for(moreBtn in moreBtnGrp){
		        ${moreBtn.body}
		        @}
		    @}
		    @if("true" == delRowBtn){
			    <button id="${id}_dyn_del" type="button" class="btn btn-primary btn-sm">
			        <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span>${delRowBtnText}
			    </button>
		    @}
		</div>
	</div>
</div>
<table id="${id}"></table>
<input type="hidden" id="${id}_select_ids" >
<input type="hidden" id="${id}_select_text" >
<input type="hidden" id="${id}_select_rows_json" >
<script type="text/javascript">
$(function() {
	var $table = $("#${id}");
	var selections = [];
	var selectText = [];
	@if(selections != "undefined"){
    var _beetl_selections = ${selections};
    if ($.isArray(_beetl_selections)) {
        selections = _beetl_selections;
    }else{
        var temp = _beetl_selections;
        if(temp.length>0){
            selections = temp.split(",");
            selections = selections.map(Number);
        }
    }
    @}
    
    @if(selectText != "undefined"){

    var _beetl_selectText = ${selectText};
    if ($.isArray(_beetl_selectText)) {
        selectText = _beetl_selectText;
    }else{
        var temp = _beetl_selectText;
        if(temp.length>0){
            selectText = temp.split(",");
        }
    }
    @}
    
    var selectRowJson = [];
    // @if(selectRowJson != "undefined"){

    var _beetl_selectRowJson = '${selectRowJson}';
    selectRowJson = JSON.parse(_beetl_selectRowJson);
    $("#${id}_select_rows_json").val(JSON.stringify(selectRowJson));
    // @}

    function getSelectText(){
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row['${selectName}']
        });
    }
    
    function responseHandler1(res) {
        $.each(res.rows, function (i, row) {
            row["${id}".toLowerCase()+"_check_state"] = $.inArray(row[idField], selections) !== -1;

        });
        $("#${id}_select_ids").val(selections);
        $("#${id}_select_text").val(selectText);
        return res;
    }
    function getIdSelections() {
        return $.map($table.bootstrapTable('getSelections'), function (row) {
            return row[idField]
        });
    }
    var arrayUnique = function(arr){
        var newArr = [];
        if(arr != null){
            for(var i=0;i<arr.length;i++){
                if($.inArray(arr[i],newArr) == -1){
                    newArr.push(arr[i]);
                }
            }
        }
        return newArr;
    }
	
	var idField = "${rowId}".toLowerCase();
	var queryUrl = $("#${id}_queryUrl").val();
	var toolbar = "#${id}_toolbar";
	var paginate = ${paginate};
	var editable = ${editable};
	var autoAdjustWidth = ${autoAdjustWidth};
	var showClearFilters = ${showClearFilters};
	if(editable){
        //编辑模式下，不显示清除过滤条件
        showClearFilters = false;
    }
	var filter = ${filter};
	var showRefresh = ${showRefresh};
	var showToggle = ${showToggle};
	var showColumns = ${showColumns};
	var autoInit = ${autoInit};
	var showExport = ${showExport};
	var minCols = ${minColumnCnt};
    var exportMode = "${exportMode}";
    var exportDataRange = ${exportDataRange};
    var exportTypes = ${exportTypes};
    var exportIgnoreCols = ${exportIgnoreCols};
    var exportFileName = "${exportFileName}";
    var exportSheetName = "${exportSheetName}";
    var exportTableName = "${exportTableName}";
    var exportBasicText = "${exportBasicText}";
    var exportAllText = "${exportAllText}";
    var exportSelectedText = "${exportSelectedText}";
    var exportOptions = ${exportOptions};
    var finalExportOpts = $.extend({
        ignoreColumn: exportIgnoreCols,
        fileName: exportFileName,
        worksheetName: exportSheetName,
        tableName: exportTableName
    },exportOptions);
	var columns = [];
    var col = null;
    var check = null;
    var trBody = null;
    var colSelectData = [];
    var colDatetimePattern = null;
    var columns = [];
    var col = null;
    var check = null;
    var trBody = null;
    var colSelectData = [];
    var colDatetimePattern = null;
    // @if (trs != null) {
    // @for (tr in trs) {
        // @var trCheck = tr.check!"";
        // @var trField = tr.formName!"";
        // @var trTitle = tr.name!"";
        // @var trTitleTip = tr.titleTip!(defTipColHead=="true" ? trTitle : "");
        // @var trWidth = tr.width!"";
        // @var trClass = tr.cssClass!"";
        // @var trSortable = tr.sortable!"true";
        // @var trOrder = tr.order!"";
        // @var trFilterType = tr.filterType!"input";
        // @var trFilterData = tr.filterData!"[]";
        // @var trFilterDateType = tr.filterDateType!"date";
        // @var trFilterDateFormat = tr.filterDateFormat!"yyyy-MM-dd";
        // @var trEditorType = tr.editorType!trFilterType;
        // @var trEditorData = tr.editorData!trFilterData;
        // @var trEditorDateType = tr.editorDateType!trFilterDateType;
        // @var trEditorDateFormat = tr.editorDateFormat!trFilterDateFormat;
        // @var trBodyEsc = @com.epoch.base.util.ExtStringUtils.escapeForJsStr(tr.body);
        // @var trParseSelect = tr.parseSelect!"true";
        // @var trFormatDatetime = tr.formatDatetime!"true";
        // @var trDefShowValue = tr.defShowValue!"true";
        // @var trFormatter = tr.formatter!"";
        // @var trSwitchable = tr.switchable!"true";
        // @var trVisible = tr.visible!"true";
        // @var trExport = tr.export!(trField == "" || trCheck == "checkbox" || trCheck == "radio" ? "false" : "true");
        // @var exportCvt = tr.exportCvt;
        // @var cvtInitParam = tr.cvtInitParam;
        // @var actParseSelect = (trFilterType == "select" || trEditorType == "select") && trParseSelect == "true";
        // @var actFormatDatetime = (trFilterType == "date" || trEditorType == "date") && trFormatDatetime == "true";
        // @var actResolveTrBody = trBodyEsc != "" || trDefShowValue != "true";

    col = {};
    col.visible = ${trVisible};
    col.switchable = ${trSwitchable};
    col.titleTooltip = "${trTitleTip}";
    col.enableExport = ${trExport};
        // @if(trWidth != ""){

    col.width = "${trWidth}";
        // @}
        // @if(trClass != ""){

    col.class = "${trClass}";
        // @}
        // @if(trCheck == "checkbox"){

    col.checkbox = true;
    col.field = "${id}".toLowerCase()+"_check_state";
        // @}else if(trCheck == "radio"){

    col.radio = true;
    col.field = "${id}".toLowerCase()+"_check_state";
        // @}else{

    col.title = "${trTitle}";
            // @if(trField != ""){

    col.field = "${trField}".toLowerCase();
    col.sortable = ${trSortable};
    col.order = "${trOrder}";
    // @if(trFilterType == "input" || trFilterType == "select" || trFilterType == "date"){

    col.filter = {};
    col.filter.type = "${trFilterType}";
                    // @if(trFilterType == "select"){

    col.filter.data = ${trFilterData};
                    // @}else if(trFilterType == "date"){

    col.filter.dateType = "${trFilterDateType}";
    col.filter.dateFormat = "${trFilterDateFormat}";
                    // @}
                // @}
                // @if(trEditorType == "input" || trEditorType == "select" || trEditorType == "date" || trEditorType == "custom" || trEditorType == "custom-simple"){

    col.editor = {};
    col.editor.type = "${trEditorType}";
                    // @if(trEditorType == "select"){

    col.editor.data = ${trEditorData};
                    // @}else if(trEditorType == "date"){

    col.editor.dateType = "${trEditorDateType}";
    col.editor.dateFormat = "${trEditorDateFormat}";
                    // @}
                // @}
                // @if(actParseSelect){
                    // @var selDataPrimary = trFilterData;
                    // @var selDataSecondary = trEditorData;
                    // @var selDataExpPrimary = "$.merge([],col.filter.data)";
                    // @var selDataExpSecondary = "$.merge([],col.editor.data)";
                    // @if(editable == "true" && trEditorType == "select"){
                        // @selDataPrimary = trEditorData;
                        // @selDataSecondary = trFilterData;
                        // @selDataExpPrimary = "$.merge([],col.editor.data)";
                        // @selDataExpSecondary = "$.merge([],col.filter.data)";
                    // @}
                    // @if(selDataPrimary == "[]" && selDataSecondary != "[]"){
                        // @selDataExpPrimary = selDataExpSecondary;
                    // @}

    col.selectData = ${selDataExpPrimary};
                // @}
                // @if(actFormatDatetime){
                    // @var datetimePatternPrimary = trFilterDateFormat;
                    // @var datetimePatternSecondary = trEditorDateFormat;
                    // @var datetimePatternExpPrimary = "col.filter.dateFormat";
                    // @var datetimePatternExpSecondary = "col.editor.dateFormat";
                    // @if(editable == "true" && trEditorType == "date"){
                        // @datetimePatternPrimary = trEditorDateFormat;
                        // @datetimePatternSecondary = trFilterDateFormat;
                        // @datetimePatternExpPrimary = "col.editor.dateFormat";
                        // @datetimePatternExpSecondary = "col.filter.dateFormat";
                    // @}
                    // @if(datetimePatternPrimary == "" && datetimePatternSecondary != ""){
                        // @datetimePatternExpPrimary = datetimePatternExpSecondary;
                    // @}

    col.datetimePattern = ${datetimePatternExpPrimary};
                // @}
            // @}
            // @if(trFormatter != ""){

    col.formatter = window.${trFormatter};
            // @}else if(actResolveTrBody || actParseSelect || actFormatDatetime){

    col.formatter = function(value, row, index){
           var result = value;
                   // @if(actParseSelect){

           var selData = this.selectData;
           result = _tag_table.parseTrValueForSelect(result,selData);
                   // @}
                   // @if(actFormatDatetime){

           var datetimePattern = this.datetimePattern;
           result = _tag_table.formatDatetimeStr(value,"yyyy-MM-dd HH:mm:ss",datetimePattern);
                   // @}
                   // @if(actResolveTrBody){

           var trBody = "${trBodyEsc}";
           result = _tag_table.resolveTrBody(trBody, result, row, index);
                   // @}

           if (result == null) {
               result = "";
           }
           return result;
    };
            // @}
        // @}
    // @if(exportCvt != null){

    col.exportCvt = "${exportCvt}";
    // @}else if(actParseSelect && actFormatDatetime){

    col.exportCvt = "com.epoch.base.export.ExportSelectDatetimeConvertor";
    // @}else if(actParseSelect){

    col.exportCvt = "com.epoch.base.export.ExportSelectConvertor";
    // @}else if(actFormatDatetime){

    col.exportCvt = "com.epoch.base.export.ExportDatetimeConvertor";
    // @}
    // @if(cvtInitParam != null){

    col.cvtInitParam = "${cvtInitParam}";
    // @}else if(actParseSelect && actFormatDatetime){

    col.cvtInitParam = JSON.stringify({selectCvtParam:col.selectData,datetimeCvtParam:col.datetimePattern});
    // @}else if(actParseSelect){

    col.cvtInitParam = JSON.stringify(col.selectData);
    // @}else if(actFormatDatetime){

    col.cvtInitParam = col.datetimePattern;
    // @}

    columns.push(col);
    // @}
    // @}

	$table.bootstrapTable({
	    url: queryUrl,
	    cache: false,
	    toolbar: toolbar,
	    method: "POST",
	    sortable: true,
	    classes: "table table-hover table-striped",
	    ajax: function ajaxRequest(request) {
            if(autoInit){
            	 if(request.data._serverExport == true){
                     // 创建iframe提交数据以实现后台导出
                     var form = $('<form method="post" target="_blank"></form>');
                     var params = request.data;
                     $.each(params,function(key,val){
                         var ipt = $('<input type="hidden"/>');
                         form.append(ipt);
                         ipt.attr("name",key);
                         if(!isEmpty(val)){
                             ipt.val(val);
                         }
                     });
                     form.attr("action",request.url);
                     form.appendTo($(document.body));
                     form.submit();
                     form.submit(); //解决第二次导出无效的问题，临时form第一次提交成功后，第二次提交无效，原因未知
                     form.remove();
                 }else{
                	 $.ajax({
                         type:request.type,
                         url:request.url,
                         timeout:0,
                         data:request.data,
                         cache:request.cache,
                         success:function(result,status,xhr){
                             var obj = {};
                             obj.total = result.totalRow;
                             obj.rows = result.list;
                             request.success(obj);
                         },
                         error:function(xhr,status,err){
                             console.warn("ajax response error: {status:'" + status + "',exception:'" + err + "'}");
                             request.error({status:status,resp:xhr});
                         }
                     });
                 }
            }else{
                request.success({total:0,rows:[]});
                autoInit = true;
            }
	    },
	    search: false,
        sortOrder: "desc",
	    striped: true,
	    pagination: paginate, //启动分页
	    autoAdjustWidth: autoAdjustWidth,
        paginationLoop: false,
        queryParamsType: "undefined",
        sidePagination: "server", //表示服务端请求
        editable: editable, //是否开启编辑模式(true|false)，默认false
        showExport: showExport,
        exportMode: exportMode,
        idField: idField, //指定主键列
        uniqueId: idField, //指定行的唯一键
        exportDataRange: exportDataRange,
        exportTypes: exportTypes,
        exportBasicText: exportBasicText,
        exportAllText: exportAllText,
        exportSelectedText: exportSelectedText,
        exportOptions: finalExportOpts,
        onBeforeExport: function(exportRange,fileType){
            var result = true;
            var onBeforeCbk = window.${onBeforeExport};
            if(onBeforeCbk){
                result = onBeforeCbk.apply(this,exportRange,fileType);
            }
            return result;
        },
        rowStyle:function(row, index){
            var rowStyle = window.${rowStyle};
            if (!isEmpty(rowStyle)) {
                return rowStyle(row, index);
            }
            return {};
        },
        pageSize: 10,
        showRefresh: showRefresh,
        showClearFilters: showClearFilters,
        showToggle:showToggle,
        showColumns:showColumns,
        searchOnEnterKey: true,
        filter: filter,
        cardView: false,
        pageNumber: 1,
        pageList: [10, 15, 20, 25, 50],
        minimumCountColumns: minCols,
	    columns: columns,
	    rowAttributes: function (row, index){
            var rowAttributes = window.${rowAttributes};
            if (!isEmpty(rowAttributes)) {
                return rowAttributes(row, index);
            }
            return {};
        },
        onLoadSuccess: function(data) {
            // @if(onLoadSuccess != "undefined"){

            var onLoadSuccess = window.${onLoadSuccess};
            onLoadSuccess(data);
            // @}

        },
        onLoadError: function(status, resp) {
            // @if(onLoadError != "undefined"){

            var onLoadError = window.${onLoadError};
            onLoadError(status, resp);
            // @}else{

            console.error(status);
            console.error(resp);
            // @}

        },
        onRowEditing: function(row,index){
            var onRowEditing = window.${onRowEditing};
            if(onRowEditing){
                onRowEditing(row,index);
            }
        },
        onRowConfirmEdit: function(row,index){
            var onRowConfirmEdit = window.${onRowConfirmEdit};
            if(onRowConfirmEdit){
                onRowConfirmEdit(row,index);
            }
        },
        onRowCancelEdit: function(row,index){
            var onRowCancelEdit = window.${onRowCancelEdit};
            if(onRowCancelEdit){
                onRowCancelEdit(row,index);
            }
        },
        queryParams: function queryParams(params){
            //设置查询参数
            var extParams = null;
            var customParamFunc = window.${queryParam};
            if(!isEmpty(customParamFunc)){
                var customParams = customParamFunc();
                extParams = $.extend(extParams == null ? {} : extParams,customParams);
            }
            var finalParams = params;
            if(extParams != null){
                finalParams = $.extend({},params,extParams);
            }
            return finalParams;
        },
        responseHandler:responseHandler1
	});
	
	// @if("true" == delBtn){

    //删除方法
    $("#${id}_table_delete").on("click", function(){
        var onDelClick = window.${onDelClick};
        if (!isEmpty(onDelClick)) {
            var selected = $table.bootstrapTable("getAllSelections");
            onDelClick(selected);
        } else {
            console.warn("未启用onDelClick删除回调方法！");
        }
    });
    // @}
	
    // @if("true" == newBtn){
    	
	    $("#${id}_table_add").on("click", function(){
	        var onAddClick = window.${onAddClick};
	        if (!isEmpty(onAddClick)) {
	            onAddClick();
	        } else {
	            console.warn("未启用onAddClick新增回调方法！");
	        }
	    });
    // @}
	
    // @if("true" == addRowBtn){

    // 添加行
    $("#${id}_dyn_add").on("click", function(){
        // @if("true" == editable){

        var row = {};
        var cols = $table.bootstrapTable("getOptions").columns[0];
        var colCnt = cols.length;
        for (var i = 0;i < colCnt;i++) {
            var col = cols[i];
            if(col.field){
                row[col.field] = "";
            }
        }
        var onBeforeAddRow = window.${onBeforeAddRow};
        if(onBeforeAddRow){
            var initRow = onBeforeAddRow();
            if(initRow == false){
                return;
            }
            if(initRow != null){
                $.extend(row,initRow);
            }
        }
        var addedRows = $table.bootstrapTable("cer_appendRows",row,true);
        var onAddRow = window.${onAddRow};
        if(onAddRow){
            var addedRow = null;
            if(addedRows != null && addedRows.length > 0){
                addedRow = addedRows[0];
            }
            onAddRow(addedRow);
        }
        // @}else{

        console.warn("未启用editable表格编辑属性！");
        // @}

    });
    // @}
	
	// @if("true" == delRowBtn){

    // 删除行
    $("#${id}_dyn_del").on("click", function(){
        // @if("true" == editable){
		
        var selRows = $table.bootstrapTable("getSelections");
        var onBeforeDelRows = window.${onBeforeDelRows};
        if(onBeforeDelRows){
            var result = onBeforeDelRows(selRows);
            if(result == false){
                return;
            }
        }
        var rowCnt = selRows.length;
        if (rowCnt == 0) {
            MessageBox.error("${lisFun.escapeForQuot(i18n('cmn_msg_err_pleaseSelectOne'))}");
            return;
        }
        var deletedRows = [];
        var i = 0;
        var onBeforeDelRow = window.${onBeforeDelRow};
        var onDelRow = window.${onDelRow};
        for(;i < rowCnt;i++){
            var row = selRows[i];
            if(onBeforeDelRow){
                var result = onBeforeDelRow(row);
                if(result == false){
                    continue;
                }
            }
            var rowId = row[idField];
            $table.bootstrapTable("remove",{
                field:idField,
                values:[rowId]
            });
            var rowDeleted = $table.bootstrapTable("getRowByUniqueId",rowId);
            if(rowDeleted == null){
                deletedRows.push(row);
                if(onDelRow){
                    onDelRow(row);
                }
            }else{
                console.warn("删除行失败：rowId: " + rowId);
            }
        }
        var onDelRows = window.${onDelRows};
        if(onDelRows && deletedRows.length > 0){
            onDelRows(deletedRows);
        }
        // @}else{

        console.warn("未启用editable表格编辑属性！");
        // @}

    });
    // @}
	
    // 初始加载数据回调
    var firstLoadFunc = function(event, data) {
        // @if("" != selection){

        if (data != null && data.hasOwnProperty("rows") && data.rows != null && data.rows.length > 0) {
            var sels = ${selection == "ALL" ? "'ALL'" : selection};
            if (sels != null) {
                var params = null;
                if ($.isArray(sels)) {
                    params = {};
                    params["field"] = idField;
                    params["values"] = sels;
                } else if (sels.hasOwnProperty("field") && sels.hasOwnProperty("values")) {
                    params = sels;
                }
                if (params != null) {
                    $table.bootstrapTable("checkBy", params);
                } else if (sels == "ALL") {
                    $table.bootstrapTable("checkAll");
                }
            }
        }
        // @}
        // @if(onFirstLoadSuccess != "undefined"){

        var onFirstLoadSuccess = window.${onFirstLoadSuccess};
        onFirstLoadSuccess(data);
        // @}

        $table.off("load-success.bs.table", firstLoadFunc);
        
     // @if(saveId != "undefined"){

        $table.on('dbl-click-row.bs.table', function (e,rows) {
            var size = $table.bootstrapTable("getOptions").pageSize;
            var index = (rows.rownum_-1)%size;
            $table.bootstrapTable("check",index);
            $('#${saveId}', parent.document).click();
        })
        // @}

        $table.on('check.bs.table ' + 'check-all.bs.table ', function (e,rows) {
            if('${checkType}'=='radio'){
                selections=[rows];
                $("#${id}_select_ids").val(rows[idField]);
                $("#${id}_select_text").val(getSelectText());
                $("#${id}_select_rows_json").val(JSON.stringify(selections));
                return;
            }
            selections = arrayUnique(selections.concat(getIdSelections()));
            $("#${id}_select_ids").val(selections);
            selectText = arrayUnique(selectText.concat(getSelectText()));
            $("#${id}_select_text").val(selectText);
            var tempJson = selectRowJson;
            $.each(!$.isArray(rows) ? [rows] : rows,function(i,e){
                var flag=false;
                $.each(selectRowJson,function(i1,e1){
                    if(e[idField]==e1[idField]){
                        flag = true;
                        return false;
                    }
                })
                if(!flag){
                    tempJson.push(e);
                }
            })
            selectRowJson=tempJson;
            $("#${id}_select_rows_json").val(JSON.stringify(selectRowJson));
        });
        $table.on('uncheck.bs.table ' + ' uncheck-all.bs.table', function (e,rows) {
            var ids = $.map(!$.isArray(rows) ? [rows] : rows, function (row) {
                return row[idField];
            })
            var text = $.map(!$.isArray(rows) ? [rows] : rows, function (row) {
                return row['${selectName}'];
            })
            ids.forEach(function(items, j){
                var index = selections.indexOf(items);
                if(index>=0){
                    selections.splice(index,1)
                }
            });
            text.forEach(function(items, j){
                var index = selectText.indexOf(items);
                if(index>=0){
                    selectText.splice(index,1)
                }
            });
            selections = arrayUnique(selections.concat(getIdSelections()));
            $("#${id}_select_ids").val(selections);
            selectText = arrayUnique(selectText.concat(getSelectText()));
            $("#${id}_select_text").val(selectText);
            var tempJson = $.map(selectRowJson, function (row) {
                var flag=false;
                $.each(!$.isArray(rows) ? [rows] : rows,function(i,e){
                    if(row[idField]==e[idField]){
                        flag = true;
                        return false;
                    }
                })
                if(!flag){
                    return row;
                }
            });
            selectRowJson=tempJson;
            $("#${id}_select_rows_json").val(JSON.stringify(selectRowJson));
        });
    };
    $table.on("load-success.bs.table", firstLoadFunc);
});
</script>