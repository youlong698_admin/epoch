@var list = tag.list;
@var onchange = tag.onchange;
@var name = tag.name;
@var tip = tag.tip;
@var tipValue = tag.tipValue;
@var sty = tag.sty;
@var value = tag.value;
@var id = tag.id;
@var data_rule = tag.data_rule;
@var data_tip = tag.data_tip;
@var data_msg = tag.data_msg;
@var disab= tag.disabled;
@var first = (tag.first == null || tag.first == "")?"true":"false";
<select class="${(sty == null || sty == '')?'form-control':sty} form-control-sm" name="${name}" onchange="${onchange}" id="${id}"
  ${data_rule != null ? 'data-rule="' + data_rule + '"' : ''} data-tip="${data_tip}" data-msg="${data_msg}" 
  @if(disab != null && disab != ''){
    disabled="disabled"
  @}
  >
@if(first == "true"){
    <option value="${(tipValue != null && tipValue != '')?tipValue:''}" >${(tip != null && tip != '')?tip:'-请选择-'}</option>
@}
@if(list != null && list != ''){
	@for(bean in list){
		@var keyTemp = bean.key+"";
		@if(keyTemp == value+""){
			<option value="${bean.key}" selected>${bean.value}</option>
		@}else{
			<option value="${bean.key}" >${bean.value}</option>
		@}
	@}
@}
</select>
