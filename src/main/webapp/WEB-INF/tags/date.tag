@var name = tag.name!"";
@var id = tag.id!"common_date_id";
@var value = tag.value!"";
@var showBottom = tag.showBottom!"true";
@var classCls = tag.classCls!"form-control form-control-sm";
@var type = tag.type!"date";
@var format = tag.format!'yyyy-MM-dd';
@if(value!=""){
    @value = lisFun.formatDate(value,format);
@}
@var min = tag.min!'';
@var max = tag.max!'';
@var lang = tag.lang!lisFun.getCurLocale();
@var readonly = tag.readonly!'false';
@var dateEvent = tag.dateEvent!'undefined';
@var done = tag.done!'undefined';
<input type="text" class="${classCls}"  id="${id}" name="${name}"
       value="${value}"
@if(readonly == "true"){
readonly="readonly"
@}
@if(dateEvent != null && dateEvent != 'undefined' && dateEvent != ''){
	${dateEvent}
@}
>
@if(readonly == "false"){
<script>
        $(function(){
            laydate.render({
                elem: '#${id}',
                showBottom:${showBottom},
                type:'${type}',
                @if(min!=""){
                min:'${min}',
                @}
                @if(max!=""){
                max:'${max}',
                @}
                @if(readonly == "true"){
                trigger: '',
                @}
                @if(done!="undefined"){
                done: function(value, date, endDate){
                    var done = ${done};
                    done(value, date, endDate,'${id}');
                },
                @}
                format:'${format}',
                value:'${value}',
                lang:'${lang}'
            });
        })

</script>
@}

