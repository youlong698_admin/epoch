@var id = tag.id!'common_fm_radio_id';
@var name = tag.name!'common_fm_radio';
@var list = tag.list!{};
@var checked = tag.checked!'';
@var sty = tag.sty!'';
@var classValue = tag.classValue!'';
@var click = tag.click!'undefined';
@for(item in list){
<label id="${id}${itemLP.index}" style="${sty}" class="${classValue}"><input name="${name}" type="radio" ${checked==item.key!?'checked':''} value="${item.key!}" onclick="${click}"/>${item.value!}</label>
@}