@var id = tag.id;
@var tabs = tag.children;
@var isControl = tag.isControl!'true';
@var isParent = tag.isParent!'true';
<div class="row table_padding">
    <div class="col-sm-12">
        <div id="${id}_main" class="tabs-container main_tabs">
            @if(tabs != null){
            <ul class="nav nav-tabs">
                @for(tab in tabs){
                <li${tab.active == 'true' ? ' class="active"' : ''} style="display: ${tab.hide == 'true' ? 'none' : 'block'};">
                    <a data-toggle="tab"
                       href="#${tab.id}"
                       @if(tab.isClick!'true' == "true"){
                            style="border-radius:0;"
                            @//如果是内嵌tabs需要不清空页面tab信息
                            onclick='_tabs.showPage("${tab.id}",null,null,null,${isControl},${isParent == 'true'?true:false})'
                        @}else{
                            style="border-radius:0;pointer-events:none;"
                        @}
                    data-tab-active="${tab.active == 'true' ? tab.active : 'false'}">${tab.name!i18n('cmn_label_noTitle')}</a></li>
                @}
            </ul>
            <div class="tab-content">
                @for(tab in tabs){
                <div id="${tab.id}" class="tab-pane${tab.active == 'true' ? ' active' : ''}"
                     defaultUrl="${tab.url!'default.html'}"></div>
                @}
            </div>
            @}
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        var temp = $('#${id}_main>ul>li>a[data-tab-active="true"]');
        temp.click();
    });
</script>
