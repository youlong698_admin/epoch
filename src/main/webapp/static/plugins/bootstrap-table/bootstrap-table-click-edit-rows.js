/**
 * Bootstrap Table extension: Click Edit Rows
 * @author Royce Wang
 * @version: v1.1.0
 */
(function($){
    "use strict";
    $.extend($.fn.bootstrapTable.defaults,{
        editable:false,
        editTrigger:"check",
        showCancelEdit:true,
        saveEditOnly:false,
        saveSelectOnly:false,
        onRowEditing:function(row,idx){
            return false;
        },
        onRowConfirmEdit:function(row,idx){
            return false;
        },
        onRowCancelEdit:function(row,idx){
            return false;
        }
    });
    var PROP_DYNA_ADD = "_dynamic_add";
    var EDIT_ROW_HIGHLIGHT = "edit-row-highlight";
    var DEF_DATETIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    var sprintf = function(str){
        var args = arguments,
            flag = true,
            i = 1;
        str = str.replace(/%s/g,function(){
            var arg = args[i++];
            if(typeof arg === "undefined"){
                flag = false;
                return "";
            }
            return arg;
        });
        return flag ? str : "";
    };
    var initSelect2 = function(bsTable,$sel,optData){
        var opts = {
            placeholder:"",
            allowClear:true,
            data:optData,
            dropdownParent:bsTable.$tableBody.find(">table")
        };
        $sel.select2(opts);
        $sel.on("select2:unselecting",function(e){
            e.preventDefault();
            $sel.val(null).trigger("change");
        });
    };
    var getVisibleCols = function(cols){
        var visibleCols = [];
        if(cols != null){
            cols.forEach(function(col,i){
                if(col.visible){
                    visibleCols.push(col);
                }
            });
        }
        return visibleCols;
    };
    var fillEditRow = function(cols,$tr,row,editRow){
        var visibleCols = getVisibleCols(cols);
        var colCnt = visibleCols.length;
        $tr.find(">td").each(function(i,td){
            if(i < colCnt){
                var col = visibleCols[i];
                if(col.editor){
                    var editorType = col.editor.type;
                    var $td = $(td);
                    switch(editorType){
                    case "input":
                        editRow[col.field] = $td.find("input").val();
                        break;
                    case "date":
                        editRow[col.field] = _tag_table.formatDatetimeStr($td.find("input").val(),col.editor.dateFormat,DEF_DATETIME_PATTERN);
                        break;
                    case "select":
                        editRow[col.field] = $td.find("select").val();
                        break;
                    case "custom":
                        var getData = $td.find(">.editable-custom").attr("get-data");
                        if(getData != null){
                            var getDataFunc = eval(getData);
                            editRow[col.field] = getDataFunc(row,$td);
                        }
                        break;
                    case "custom-simple":
                        var $custom = $td.find(">.editable-custom");
                        var $ctnts = $custom.contents();
                        var $tempForm = $("<form></form>");
                        $ctnts.appendTo($tempForm);
                        var editData = $tempForm.serializeArray();
                        $ctnts.appendTo($custom);
                        if(editData != null){
                            var len = editData.length;
                            var i = 0;
                            var editRowDatas = {};
                            for(;i < len;i++){
                                var field = editData[i].name;
                                if(!isEmpty(field)){
                                    field = field.toLowerCase();
                                    var vals = editRowDatas[field];
                                    if(vals == null){
                                        vals = [];
                                        editRowDatas[field] = vals;
                                    }
                                    vals.push(editData[i].value);
                                }
                            }
                            $.each(editRowDatas,function(key,values){
                                editRow[key] = values.join(",");
                            });
                        }
                        break;
                    default:
                    }
                }
            }
        });
    };
    var BootstrapTable = $.fn.bootstrapTable.Constructor;
    var _initData = BootstrapTable.prototype.initData;
    var _initBody = BootstrapTable.prototype.initBody;
    var _insertRow = BootstrapTable.prototype.insertRow;
    var _remove = BootstrapTable.prototype.remove;
    $.extend(BootstrapTable.EVENTS,{
        "row-editing.bs.table":"onRowEditing",
        "row-confirm-edit.bs.table":"onRowConfirmEdit",
        "row-cancel-edit.bs.table":"onRowCancelEdit"
    });
    BootstrapTable.prototype.cer_editRows = function(params,silent,highlightRowIdx){
        if(!this.options.editable){
            return;
        }
        var that = this;
        var paramsIsArray = $.isArray(params);
        var allParams = paramsIsArray ? params : [params];
        if(highlightRowIdx == null && params != null && !paramsIsArray){
            highlightRowIdx = params.index;
        }
        var lang = "zh_CN";
        if(that.options.locale == "en_US" || that.options.locale == "zh_HK"){
            lang = that.options.locale;
        }
        $.each(allParams,function(i,param){
            if(!param.hasOwnProperty("index")){
                return;
            }
            var rowIdx = param.index;
            var row = that.data[rowIdx];
            if(param.hasOwnProperty("row")){
                row = param.row;
            }
            var $tr = that.$tableBody.find(">table>tbody>tr[data-index='" + rowIdx + "']");
            if($tr.data("cer_editOn") != "true"){
                $tr.data("cer_editOn","true");
                var visibleCols = getVisibleCols(that.columns);
                var visibleColCnt = visibleCols.length;
                $tr.find(">td").each(function(i,td){
                    if(i < visibleColCnt){
                        var col = visibleCols[i];
                        if(col.editor){
                            var editorType = col.editor.type;
                            if(!editorType){
                                return;
                            }
                            var $td = $(td);
                            var initVal = row[col.field];
                            switch(editorType){
                            case "input":
                                var $div = $('<div class="editable-input"/>');
                                var $input = $('<input type="text" name="' + col.field + '" class="form-control form-control-sm"/>');
                                var $clear = $('<span class="clear"><i class="fa fa-remove" aria-hidden="true"></i></span>');
                                $div.append($input);
                                $div.append($clear);
                                $td.empty().append($div);
                                $clear.on("click",function(){
                                    var $input = $(this).parent().find("input");
                                    $input.val("");
                                    $input.focus();
                                });
                                $input.val(initVal);
                                break;
                            case "select":
                                var $sel = $('<select name="' + col.field + '" class="form-control input-sm"/>');
                                $td.empty().append($sel);
                                initSelect2(that,$sel,col.editor.data);
                                $sel.val(initVal).trigger("change");
                                break;
                            case "date":
                                var $div = $('<div class="editable-input"/>');
                                var $input = $('<input type="text" name="' + col.field + '" class="form-control form-control-sm"/>');
                                $div.append($input);
                                $td.empty().append($div);
                                var initValStr = _tag_table.formatDatetimeStr(initVal,DEF_DATETIME_PATTERN,col.editor.dateFormat);
                                $input.val(initValStr);
                                laydate.render({
                                    elem: $input[0],
                                    showBottom: true,
                                    type: col.editor.dateType,
                                    format: col.editor.dateFormat,
                                    lang: lang,
                                });
                                break;
                            case "custom":
                                var $custom = $td.find(">.editable-custom");
                                $custom.detach();
                                $td.empty().append($custom);
                                var setData = $custom.attr("set-data");
                                if(setData != null){
                                    var setDataFunc = eval(setData);
                                    setDataFunc(row,$td,initVal);
                                }
                                $custom.show();
                                break;
                            case "custom-simple":
                                var $custom = $td.find(">.editable-custom");
                                $custom.detach();
                                var $tempForm = $("<form></form>");
                                $custom.appendTo($tempForm);
                                var editElmts = $tempForm.prop("elements");
                                $.each(editElmts,function(i,elmt){
                                    var $elmt = $(elmt);
                                    var name = $elmt.attr("name");
                                    if(name != null && name != ""){
                                        var val = row[name];
                                        if(elmt.type == "radio"){
                                            $elmt.prop("checked",elmt.value == String(val));
                                        }else if(elmt.type == "checkbox"){
                                            $elmt.prop("checked",val != null && $.inArray(elmt.value,String(val).split(",")) >= 0);
                                        }else{
                                            $elmt.val(row[name]);
                                        }
                                    }
                                });
                                $custom.appendTo($td.empty());
                                $custom.show();
                                break;
                            default:
                                console.info(col.fieldIndex + " " + editorType);
                            }
                        }
                    }
                });
                if(that.options.editTrigger == "check" && that._hasCheckOrRadioCol()){
                    that._extCheckBy_(true,{
                        field:that.options.idField,
                        values:[row[that.options.idField]]
                    },true);
                }else{
                    var $tooling = $('<div class="editable-buttons"/>');
                    if(that.options.showCancelEdit){
                        var $cancel = $('<button type="button" class="btn visa-btn-icon editable-cancel"><i class="glyphicon glyphicon-remove"></i></button>');
                        $cancel.on("click",function(){
                            that.cer_cancelEditRows([rowIdx]);
                            return false;
                        });
                        $tooling.append($cancel);
                    }
                    $tr.find(">td").last().append($tooling);
                }
            }
            if(rowIdx == highlightRowIdx){
                that.ext_highlightRow(rowIdx,true);
            }
            if(silent != true){
                that.trigger("row-editing",row,rowIdx);
            }
        });
    };
    BootstrapTable.prototype.cer_confirmEditRows = function(params,silent){
        return this._confirmEditRows(params,false,silent);
    };
    BootstrapTable.prototype.cer_confirmEditAllRows = function(silent){
        return this._confirmEditRows([],true,silent);
    };
    BootstrapTable.prototype.cer_cancelEditRows = function(indexes,silent){
        this._cancelEditRows(indexes,false,silent);
    };
    BootstrapTable.prototype.cer_cancelEditAllRows = function(silent){
        this._cancelEditRows([],true,silent);
    };
    BootstrapTable.prototype.cer_insertRows = function(params,autoEdit,silent){
        if(!this.options.editable){
            return;
        }
        var allParams = $.isArray(params) ? params : [params];
        var idField = this.options.idField;
        var PRE = "_auto_sys_gen_";
        var idNum = 0;
        var len = this.data.length;
        for (var i = 0;i < len;i++) {
            var rowData = this.data[i];
            var rowId = rowData[idField];
            if (rowId != null && (typeof rowId === "string") && rowId.indexOf(PRE) == 0) {
                var genNum = parseInt(rowId.substr(PRE.length));
                if (idNum <= genNum) {
                    idNum = genNum + 1;
                }
            }
        }
        var that = this;
        var result = [];
        $.each(allParams,function(i,param){
            if(!param.hasOwnProperty("index")){
                return;
            }
            var row = {};
            if(param.hasOwnProperty("row")){
                $.extend(row,param.row);
            }
            row[idField] = PRE + (idNum + i).toString();
            row[PROP_DYNA_ADD] = true;
            that.insertRow({
                index:param.index,
                row:row
            },autoEdit,silent);
            result.push(row);
        });
        return result;
    };
    BootstrapTable.prototype.cer_appendRows = function(rows,autoEdit,silent){
        if(!this.options.editable){
            return;
        }
        var allRows = $.isArray(rows) ? rows : [rows];
        var len = this.data.length;
        var params = $.map(allRows,function(row,i){
            return {
                index:len + i,
                row:row
            };
        });
        return this.cer_insertRows(params,autoEdit,silent);
    };
    BootstrapTable.prototype.cer_updateRows = function(params,editOff){
        if(!this.options.editable){
            return;
        }
        var editData = this._obtainEditData();
        var that = this;
        var allParams = $.isArray(params) ? params : [params];
        var updData = {};
        $.each(allParams,function(i,param){
            if(!param.hasOwnProperty("index") || !param.hasOwnProperty("row")){
                return;
            }
            updData[param.index] = param.row;
        });
        $.each(editData,function(id,editParam){
            var updRow = updData[editParam.index];
            if(updRow != null){
                $.extend(editParam.row,updRow);
            }
        });
        this.cer_editDataCache = editData;
        this.updateRow(params);
        if(editOff == true){
            var indexes = $.map(allParams,function(param,i){
                return Number(param.index);
            });
            this.cer_cancelEditRows(indexes);
        }
    };
    BootstrapTable.prototype.cer_getEditingData = function(allData){
        if(!this.options.editable){
            return null;
        }
        var result = [];
        var stateField = this.header.stateField;
        var editData = this._obtainEditData(allData);
        $.each(editData,function(key,val){
            val["selected"] = val.row[stateField] ? true : false;
            val["newAdd"] = val.row[PROP_DYNA_ADD] ? true : false;
            val["onEdit"] = val.onEdit;
            result.push(val);
        });
        result.sort(function(a,b){
            return parseInt(a.index) - parseInt(b.index);
        });
        return result;
    };
    BootstrapTable.prototype._confirmEditRows = function(params,isAll,silent){
        if(!this.options.editable){
            return null;
        }
        var existData = [];
        var that = this;
        var editParams = $.isArray(params) ? params : [params];
        var data = this.options.data;
        var cols = this.columns;
        var stateField = this.header.stateField;
        var saveEditOnly = this.options.saveEditOnly;
        var saveSelOnly = this.options.saveSelectOnly;
        var editParamIndexes = $.map(editParams,function(editParam,i){
            var idx;
            if(isFinite(editParams)){
                idx = editParams;
            }else{
                idx = editParam["index"];
            }
            return idx;
        });
        this.$tableBody.find(">table>tbody>tr").each(function(i,tr){
            var $tr = $(tr);
            var rowIdx = $tr.data("index");
            if(rowIdx == null){
                return;
            }
            var paramIdx = $.inArray(rowIdx,editParamIndexes);
            if(isAll || paramIdx >= 0){
                var onEdit = $tr.data("cer_editOn");
                var rowData = data[rowIdx];
                var selected = rowData[stateField];
                if(onEdit){
                    var row = {};
                    fillEditRow(cols,$tr,rowData,row);
                    $.extend(rowData,row);
                    var editParam = {};
                    if(paramIdx >= 0){
                        editParam = editParams[paramIdx];
                    }
                    var keepEdit = editParam["keepEdit"];
                    if(that.options.editTrigger == "check" && that._hasCheckOrRadioCol() && !keepEdit){
                        that._extCheckBy_(false,{
                            field:that.options.idField,
                            values:[rowData[that.options.idField]]
                        },true);
                    }
                    if(!keepEdit){
                        $tr.removeData("cer_editOn");
                    }
                    if(silent != true){
                        that.trigger("row-confirm-edit",rowData,rowIdx);
                    }
                }
                if((!saveEditOnly || onEdit) && (!saveSelOnly || selected)){
                    existData.push(rowData);
                }
            }
        });
        this.updateRow([]);
        var result = {};
        var newRows = [];
        var editRows = [];
        var len = existData.length;
        var i,existRow;
        for(i = 0;i < len;i++){
            existRow = existData[i];
            if(existRow[PROP_DYNA_ADD]){
                newRows.push(existRow);
            }else{
                editRows.push(existRow);
            }
        }
        result["newRows"] = newRows;
        result["editRows"] = editRows;
        result["delRows"] = this.delData;
        return result;
    };
    BootstrapTable.prototype._cancelEditRows = function(indexes,isAll,silent){
        if(!this.options.editable){
            return;
        }
        var that = this;
        var idxes = $.isArray(indexes) ? indexes : [indexes];
        this.$tableBody.find(">table>tbody>tr").each(function(i,tr){
            var $tr = $(tr);
            var rowIdx = $tr.data("index");
            if(rowIdx == null){
                return;
            }
            if(isAll || $.inArray(rowIdx,idxes) >= 0){
                var onEdit = $tr.data("cer_editOn");
                var rowData = that.options.data[rowIdx];
                if(onEdit){
                    if(that.options.editTrigger == "check" && that._hasCheckOrRadioCol()){
                        that._extCheckBy_(false,{
                            field:that.options.idField,
                            values:[rowData[that.options.idField]]
                        },true);
                    }
                    $tr.removeData("cer_editOn");
                    if(silent != true){
                        that.trigger("row-cancel-edit",rowData,rowIdx);
                    }
                }
            }
        });
        this.updateRow([]);
    };
    BootstrapTable.prototype._obtainEditData = function(withNotEditData){
        if(!this.options.editable){
            return null;
        }
        var editData = {};
        var that = this;
        var cols = this.columns;
        this.$tableBody.find(">table>tbody>tr").each(function(i,tr){
            var $tr = $(tr);
            var rowIdx = $tr.data("index");
            if(rowIdx == null || rowIdx >= that.data.length){
                return;
            }
            var row = that.data[rowIdx];
            var rowId = row[that.options.idField];
            var trRowId = $tr.data("uniqueid");
            var highlight = $tr.hasClass(EDIT_ROW_HIGHLIGHT);
            var onEdit = $tr.data("cer_editOn");
            if(onEdit && rowId == trRowId){ //若行数据中的id与tr单元属性中的不一样，则说明发生了翻页，编辑数据将被忽略
                var editRow = $.extend({},row);
                fillEditRow(cols,$tr,row,editRow);
                editData[rowId] = {
                    index:rowIdx,
                    highlight:highlight,
                    row:editRow,
                    onEdit:true
                };
            }else if(withNotEditData){
                var origRow = $.extend({},row);
                editData[rowId] = {
                    index:rowIdx,
                    highlight:highlight,
                    row:origRow,
                    onEdit:false
                };
            }
        });
        return editData;
    };
    BootstrapTable.prototype._hasCheckOrRadioCol = function(){
        var result = false;
        $.each(this.columns,function(i,col){
            if((col.checkbox || col.radio) && (col.switchable || col.visible)){
                result = true;
                return false;
            }
        });
        return result;
    };
    BootstrapTable.prototype._onEditRowClick = function(e,row,$tr,field){
        var that = e.data;
        var rowIdx = $tr.data("index");
        if(rowIdx == null){
            return;
        }
        that.cer_editRows({
            index:rowIdx,
            row:row
        },false,rowIdx);
    };
    BootstrapTable.prototype._onEditRowCheck = function(e,row,$elmt){
        var that = e.data;
        var rowIdx = $elmt.data("index");
        if(rowIdx == null){
            return;
        }
        if(that.options.editTrigger == "check" && that._hasCheckOrRadioCol() && $elmt.attr("type") == "radio"){
            that._cancelEditRows([],true,false);
        }
        that.cer_editRows({
            index:rowIdx,
            row:row
        },false,rowIdx);
    };
    BootstrapTable.prototype._onEditRowUncheck = function(e,row,$elmt){
        var that = e.data;
        var rowIdx = $elmt.data("index");
        if(rowIdx == null){
            return;
        }
        if(that.options.applyEditWhenUncheck){
            that._confirmEditRows([rowIdx],false);
        }else{
            that._cancelEditRows([rowIdx],false);
        }
    };
    BootstrapTable.prototype._onEditRowCheckAll = function(e,rows){
        var that = e.data;
        that.$tableBody.find(">table>tbody>tr").each(function(i,tr){
            var $tr = $(tr);
            var $elmt = $tr.find(">td.bs-checkbox:first>input[type='checkbox']:first");
            var rowIdx = $elmt.data("index");
            if(rowIdx != null){
                var row = that.data[rowIdx];
                that._onEditRowCheck(e,row,$elmt);
            }
        });
    };
    BootstrapTable.prototype._onEditRowUncheckAll = function(e,rows){
        var that = e.data;
        that.$tableBody.find(">table>tbody>tr").each(function(i,tr){
            var $tr = $(tr);
            var $elmt = $tr.find(">td.bs-checkbox:first>input[type='checkbox']:first");
            var rowIdx = $elmt.data("index");
            if(rowIdx != null){
                var row = that.data[rowIdx];
                that._onEditRowUncheck(e,row,$elmt);
            }
        });
    };
    BootstrapTable.prototype._extCheckBy_ = function(checked,param,silent){
        if(!param.hasOwnProperty("field") || !param.hasOwnProperty("values")){
            return;
        }
        var that = this,
            rows = [];
        $.each(this.options.data,function(index,row){
            if(!row.hasOwnProperty(param.field)){
                return false;
            }
            if($.inArray(row[param.field],param.values) >= 0){
                var $el = that.$selectItem.filter(":enabled").filter(sprintf('[data-index="%s"]',index)).prop("checked",checked);
                row[that.header.stateField] = checked;
                rows.push(row);
                if(silent != true){
                    that.trigger(checked ? "check" : "uncheck",row,$el);
                }
            }
        });
        this.updateSelected();
        if(silent != true){
            this.trigger(checked ? "check-some" : "uncheck-some",rows);
        }
    };
    BootstrapTable.prototype.ext_highlightRow = function(rowIdx,highlight){
        if(rowIdx == null){
            return;
        }
        if(highlight){
            this.$tableBody.find(">table>tbody>tr").removeClass("edit-row-highlight");
            this.$tableBody.find(">table>tbody>tr[data-index='" + rowIdx + "']").addClass("edit-row-highlight");
        }else{
            this.$tableBody.find(">table>tbody>tr[data-index='" + rowIdx + "']").removeClass("edit-row-highlight");
        }
    };
    BootstrapTable.prototype.initData = function(data,type){
        _initData.apply(this,Array.prototype.slice.apply(arguments));
        if(!this.options.editable){
            return;
        }
        this.delData = [];
    };
    BootstrapTable.prototype.initBody = function(){
        var editData = null;
        if(this.cer_editDataCache != null){
            editData = this.cer_editDataCache;
            this.cer_editDataCache = null;
        }else{
            editData = this._obtainEditData();
        }
        _initBody.apply(this,Array.prototype.slice.apply(arguments));
        if(!this.options.editable){
            return;
        }
        var that = this;
        var $table = this.$tableBody.find(">table");
        if(this.options.editTrigger == "check" && this._hasCheckOrRadioCol()){
            $table.off("check.bs.table",this._onEditRowCheck).on("check.bs.table",null,this,this._onEditRowCheck);
            $table.off("uncheck.bs.table",this._onEditRowUncheck).on("uncheck.bs.table",null,this,this._onEditRowUncheck);
            $table.off("check-all.bs.table",this._onEditRowCheckAll).on("check-all.bs.table",null,this,this._onEditRowCheckAll);
            $table.off("uncheck-all.bs.table",this._onEditRowUncheckAll).on("uncheck-all.bs.table",null,this,this._onEditRowUncheckAll);
        }else{
            $table.off("click-row.bs.table",this._onEditRowClick).on("click-row.bs.table",null,this,this._onEditRowClick);
        }
        var oldEditParams = [];
        var highlightRowIdx = null;
        $table.find(">tbody>tr").each(function(i,tr){
            var $tr = $(tr);
            var rowIdx = $tr.data("index");
            if(rowIdx == null){
                return;
            }
            var row = that.data[rowIdx];
            var rowId = row[that.options.idField];
            var editParam = editData[rowId];
            if(editParam != null){
                oldEditParams.push({
                    index:rowIdx,
                    row:editParam["row"]
                });
                if(editParam.highlight){
                    highlightRowIdx = rowIdx;
                }
            }
        });
        this.cer_editRows(oldEditParams,true,highlightRowIdx);
    };
    BootstrapTable.prototype.insertRow = function(params,autoEdit,silent){
        if(this.options.editable){
            this.cer_editDataCache = this._obtainEditData();
        }
        _insertRow.apply(this,Array.prototype.slice.apply(arguments));
        if(!this.options.editable){
            return;
        }
        if(!params.hasOwnProperty("index") || !params.hasOwnProperty("row")){
            return;
        }
        if(autoEdit != true){
            return;
        }
        var that = this;
        this.$tableBody.find(">table>tbody>tr").each(function(i,tr){
            var $tr = $(tr);
            var rowIdx = $tr.data("index");
            if(rowIdx == null){
                return;
            }
            if(rowIdx == params.index){
                if(that.options.editTrigger == "check" && that._hasCheckOrRadioCol() && $tr.find(">td:first>input").attr("type") == "radio"){
                    that._cancelEditRows([],true,silent);
                }
                that.cer_editRows({
                    index:rowIdx,
                    row:params.row
                },silent,rowIdx);
            }
        });
    };
    BootstrapTable.prototype.remove = function(params){
        if(this.options.editable){
            if(!params.hasOwnProperty("field") || !params.hasOwnProperty("values")){
                return;
            }
            var len = this.options.data.length;
            var i,row;
            for(i = len - 1;i >= 0;i--){
                row = this.options.data[i];
                if(!row.hasOwnProperty(params.field)){
                    continue;
                }
                if($.inArray(row[params.field],params.values) >= 0 && !row[PROP_DYNA_ADD]){
                    this.delData.push(row);
                }
            }
            this.cer_editDataCache = this._obtainEditData();
        }
        _remove.apply(this,Array.prototype.slice.apply(arguments));
    };
})(jQuery);
