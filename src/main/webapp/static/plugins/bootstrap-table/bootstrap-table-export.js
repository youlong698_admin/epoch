/**
 * @author zhixin wen <wenzhixin2010@gmail.com>
 * extensions: https://github.com/kayalshri/tableExport.jquery.plugin
 */

(function ($) {
    'use strict';
    var sprintf = $.fn.bootstrapTable.utils.sprintf;

    var TYPE_NAME = {
        json: 'JSON',
        xml: 'XML',
        png: 'PNG',
        csv: 'CSV',
        txt: 'TXT',
        sql: 'SQL',
        doc: 'MS-Word',
        excel: 'MS-Excel',
        xlsx: 'MS-Excel (OpenXML)',
        powerpoint: 'MS-Powerpoint',
        pdf: 'PDF'
    };

    $.extend($.fn.bootstrapTable.defaults, {
        showExport: false,
        // ==start== 增加后台导出功能
        exportMode: "auto",
        onBeforeExport: undefined,
        // ===end=== 增加后台导出功能
        exportDataType: 'basic', // basic, all, selected
        // 'json', 'xml', 'png', 'csv', 'txt', 'sql', 'doc', 'excel', 'powerpoint', 'pdf'
        exportTypes: ['json', 'xml', 'csv', 'txt', 'sql', 'excel'],
        exportOptions: {}
        // ==start== 加入选取导出范围功能
        ,exportBasicText: "Export Current Page"
        ,exportAllText: "Export All"
        ,exportSelectedText: "Export Selected"
        // ===end=== 加入选取导出范围功能
    });

    $.extend($.fn.bootstrapTable.defaults.icons, {
        export: 'glyphicon-export icon-share'
    });

    $.extend($.fn.bootstrapTable.locales, {
        formatExport: function () {
            return 'Export data';
        }
        // ==start== 加入选取导出范围功能
        ,formatExportBasic:function(){
            return "Export Current Page";
        }
        ,formatExportAll: function(){
            return "Export All";
        }
        ,formatExportSelected: function(){
            return "Export Selected";
        }
        // ===end=== 加入选取导出范围功能
    });
    $.extend($.fn.bootstrapTable.defaults, $.fn.bootstrapTable.locales);

    var BootstrapTable = $.fn.bootstrapTable.Constructor,
        _initToolbar = BootstrapTable.prototype.initToolbar;

    BootstrapTable.prototype.initToolbar = function () {
        this.showToolbar = this.options.showExport;

        _initToolbar.apply(this, Array.prototype.slice.apply(arguments));

        if (this.options.showExport) {
            var that = this,
                $btnGroup = this.$toolbar.find('>.btn-group'),
                $export = $btnGroup.find('div.export');

            var allSelJsonHideId = that.$el[0].id + "_select_rows_json";
            var allSelIdHideId = that.$el[0].id + "_select_ids";
            if (!$export.length) {
                $export = $([
                    '<div class="export btn-group">',
                        '<button class="btn' +
                            sprintf(' btn-%s', this.options.buttonsClass) +
                            sprintf(' btn-%s', this.options.iconSize) +
                            ' dropdown-toggle" aria-label="export type" ' +
                            'title="' + this.options.formatExport() + '" ' +
                            'data-toggle="dropdown" type="button">',
                            sprintf('<i class="%s %s"></i> ', this.options.iconsPrefix, this.options.icons.export),
                            '<span class="caret"></span>',
                        '</button>',
                        // ==start== 调整导出下拉菜单样式
                        // '<ul class="dropdown-menu" role="menu">',
                        '<ul class="dropdown-menu impact-menu" role="menu">',
                        // ===end=== 调整导出下拉菜单样式
                        '</ul>',
                    '</div>'].join('')).appendTo($btnGroup);

                var $menu = $export.find('.dropdown-menu'),
                    exportTypes = this.options.exportTypes;
                // ==start== 加入选取导出范围功能
                var exportRanges = this.options.exportDataRange;
                var rangeSelData = [];
                var rangeOptions = "";
                $.each(exportRanges,function(i,range){
                    switch(range){
                    case "basic":
                        rangeSelData.push({id:range,text:that.options.exportBasicText});
                        rangeOptions += '<option value="' + range + '">' + that.options.exportBasicText + '</option>';
                        break;
                    case "all":
                        rangeSelData.push({id:range,text:that.options.exportAllText});
                        rangeOptions += '<option value="' + range + '">' + that.options.exportAllText + '</option>';
                        break;
                    case "selected":
                        rangeSelData.push({id:range,text:that.options.exportSelectedText});
                        rangeOptions += '<option value="' + range + '">' + that.options.exportSelectedText + '</option>';
                        break;
                    default:
                        break;
                    }
                });
                $menu.append('<li role="menuitem"><a class="export-select-range" href="javascript:void(0)"><select class="temp-select-for-export"></select></a></li>');
                var $rangeSel = $menu.find(">li>a>select");
                $rangeSel.append(rangeOptions);
                var $help = $rangeSel.clone();
                $help.attr("style","position: absolute !important;top:-9999px !important;").appendTo("body");
                $rangeSel.css("width",$help.width());
                $help.remove();
                $rangeSel.select2({
                    data:rangeSelData,
                    dropdownParent:$export,
                    minimumResultsForSearch:10,
                    width:"style"
                });
                // ===end=== 加入选取导出范围功能

                if (typeof this.options.exportTypes === 'string') {
                    var types = this.options.exportTypes.slice(1, -1).replace(/ /g, '').split(',');

                    exportTypes = [];
                    $.each(types, function (i, value) {
                        exportTypes.push(value.slice(1, -1));
                    });
                }
                $.each(exportTypes, function (i, type) {
                    if (TYPE_NAME.hasOwnProperty(type)) {
                        $menu.append(['<li role="menuitem" data-type="' + type + '">',
                                '<a href="javascript:void(0)">',
                                    TYPE_NAME[type],
                                '</a>',
                            '</li>'].join(''));
                    }
                });

                $menu.find('li').click(function () {
                    // ==start== 加入选取导出范围功能
                	debugger
                    var dataTypeAttr = $(this).attr("data-type");
                    if(dataTypeAttr == null || dataTypeAttr == ""){
                        arguments[0].preventDefault();
                        arguments[0].stopPropagation();
                        return;
                    }
                    // ===end=== 加入选取导出范围功能
                    var type = $(this).data('type'),
                        doExport = function () {
                            // ==start== 将table header中的过滤组件内容去掉不导出
                            var tableCtnt = that.$el.html();
                            var dummyTable = $('<table data-tableexport-display="always"></table>');
                            dummyTable.append(tableCtnt);
                            dummyTable.find(">thead>tr>th>div.fht-cell").remove();
                            // ==start== 将table cell中的隐藏编辑域去掉不导出
                            dummyTable.find(">tbody>tr>td>.editable-custom").remove();
                            // ===end=== 将table cell中的隐藏编辑域去掉不导出
                            // ==start== 将table无记录时的提示消息行去掉不导出
                            dummyTable.find(">tbody>tr.no-records-found").remove();
                            // ===end=== 将table无记录时的提示消息行去掉不导出
                            // ==start== 将table中tr上定义的列导出控制参数合并到ignoreColumn参数中
                            var ignoreCols = that.options.exportOptions.ignoreColumn;
                            var finalIgnoreCols = $.merge([],ignoreCols);
                            $.extend(finalIgnoreCols,that.options.exportOptions.ignoreColumn);
                            var visibleCols = that.getVisibleColumns();
                            $.each(visibleCols,function(i,col){
                                if(!col.enableExport && ignoreCols.indexOf(i) < 0){
                                    finalIgnoreCols.push(i);
                                }
                            });
                            // ===end=== 将table中tr上定义的列导出控制参数合并到ignoreColumn参数中
                            dummyTable.tableExport($.extend({}, that.options.exportOptions, {
                                type: type,
                                escape: false
                                // ==start== 将table中tr上定义的列导出控制参数合并到ignoreColumn参数中
                                ,ignoreColumn: finalIgnoreCols
                                // ===end=== 将table中tr上定义的列导出控制参数合并到ignoreColumn参数中
                            }));
                            // that.$el.tableExport($.extend({}, that.options.exportOptions, {
                            //     type: type,
                            //     escape: false
                            // }));
                            // ===end=== 将table header中的过滤组件内容去掉不导出
                        };
                    // ==start== 加入选取导出范围功能
                    var exportRange = $menu.find(">li>a>select").val();
                    that.options.exportDataType = exportRange;
                    // ===end=== 加入选取导出范围功能
                    // ==start== 增加后台导出功能
                    var onBeforeCbk = that.options.onBeforeExport;
                    if(onBeforeCbk){
                        var result = onBeforeCbk.apply(that,[exportRange,type]);
                        if(result == false){
                            return;
                        }
                    }
                    var _exportActMode = null;
                    if(that.options.exportMode == "client" || (that.options.exportMode == "auto" && exportRange != "all")){
                        _exportActMode = "client";
                    }else if(that.options.exportMode == "server" || (that.options.exportMode == "auto" && exportRange == "all")){
                        _exportActMode = "server";
                    }
                    if(_exportActMode == "client"){
                    // ===end=== 增加后台导出功能

                    if (that.options.exportDataType === 'all' && that.options.pagination) {
                        var oldQueryParams = that.options.queryParams;
                        that.$el.one(that.options.sidePagination === 'server' ? 'post-body.bs.table' : 'page-change.bs.table', function () {
                            doExport();
                            that.options.queryParams = oldQueryParams;
                            that.togglePagination();
                        });
                        that.options.queryParams = function(params){
                            var result = params;
                            if(typeof oldQueryParams === "function"){
                                result = oldQueryParams.apply(this,[params]);
                            }
                            if(result == null){
                                result = {};
                            }
                            result._noPagination = true;
                            return result;
                        };
                        that.togglePagination();
                    } else if (that.options.exportDataType === 'selected') {
                        var data = that.getData(),
                            selectedData = that.getAllSelections();

                        // Quick fix #2220
                        if (that.options.sidePagination === 'server') {
                            data = {total: that.options.totalRows};
                            data[that.options.dataField] = that.getData();

                            selectedData = {total: that.options.totalRows};
                            selectedData[that.options.dataField] = that.getAllSelections();
                            // ==start== 加入选取导出范围功能
                            var allSelJson = $("#" + allSelJsonHideId).val();
                            if(allSelJson != null && allSelJson != "" && allSelJson != "[]"){
                                var allSel = JSON.parse(allSelJson);
                                selectedData[that.options.dataField] = allSel;
                            }
                            // ===end=== 加入选取导出范围功能
                        }

                        that.load(selectedData);
                        doExport();
                        that.load(data);
                    } else {
                        doExport();
                    }
                    // ==start== 增加后台导出功能
                    }else if(_exportActMode == "server"){
                        var oldQueryParams = that.options.queryParams;
                        var isColIgnore = function(idx,$th){
                            var result = false;
                            var ignoreCols = that.options.exportOptions.ignoreColumn;
                            if($.isArray(ignoreCols)){
                                var i = 0;
                                var len = ignoreCols.length;
                                for(;i < len;i++){
                                    var col = ignoreCols[i];
                                    if((typeof col === "number" && col === idx) || (typeof col === "string" && col === $th.attr("data-field"))){
                                        result = true;
                                        break;
                                    }
                                }
                            }
                            return result;
                        };
                        that.options.queryParams = function(params){
                            var result = params;
                            if(typeof oldQueryParams === "function"){
                                result = oldQueryParams.apply(this,[params]);
                            }
                            if(result == null){
                                result = {};
                            }
                            result._serverExport = true;
                            result._exportRange = that.options.exportDataType;
                            result._exportFileType = type;
                            result._exportFileName = that.options.exportOptions.fileName;
                            result._exportSheetName = that.options.exportOptions.worksheetName;
                            result._exportTableName = that.options.exportOptions.tableName;
                            // 增加表头参数
                            var headCtnt = that.$header.html();
                            var dummyTable = $('<table></table>');
                            dummyTable.append('<thead>' + headCtnt + '</thead>');
                            dummyTable.find(">thead>tr>th>div.fht-cell").remove();
                            var headMeta = [];
                            var visibleCols = that.getVisibleColumns();
                            dummyTable.find(">thead>tr>th").each(function(i,th){
                                var $th = $(th);
                                if(visibleCols[i].enableExport && !isColIgnore(i,$th)){
                                    var meta = {
                                        field:$th.attr("data-field"),
                                        title:$th.text(),
                                    };
                                    meta.exportCvt = visibleCols[i].exportCvt;
                                    meta.cvtInitParam = visibleCols[i].cvtInitParam;
                                    headMeta.push(meta);
                                }
                            });
                            result._exportHeadMeta = JSON.stringify(headMeta);
                            // 根据导出范围调整分页参数及增加选中行id参数
                            if(that.options.exportDataType === 'all'){
                                result.pageNumber = undefined;
                                result.pageSize = undefined;
                                result._noPagination = true;
                            }else if(that.options.exportDataType === 'selected'){
                                var selData = that.getAllSelections();
                                var selectIds = $.map(selData,function(row,i){
                                    return row[that.options.idField];
                                });
                                var allSelectIds = $("#" + allSelIdHideId).val();
                                if(allSelectIds != null && allSelectIds != ""){
                                    selectIds = allSelectIds.split(",");
                                }
                                if(selectIds == null){
                                    selectIds = [];
                                }
                                result._selectIds = selectIds;
                                result.pageNumber = undefined;
                                result.pageSize = undefined;
                                result._noPagination = true;
                            }
                            return result;
                        };
                        that.initServer(true);
                        that.options.queryParams = oldQueryParams;
                    }else{
                        console.error("unsupported export mode '" + that.options.exportMode + "'.");
                    }
                    // ===end=== 增加后台导出功能
                });
            }
        }
    };
})(jQuery);
