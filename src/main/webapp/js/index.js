$("body").on("click",".epochMenu",function(){
		var $this = $(this);
		var href = $(this).data("href");
		var code = $(this).data("code");
		$(".epochMenuParent").parent().removeClass("active");
		$(".epochMenu").parent().removeClass("active");
		$this.parent().addClass("active");
		href = href+"?menu_code="+code;
		window.location.hash = href;
		window.main.location.href = getRootPath()+href;
	});
//iframe自适应
$(window).on('resize', function() {
	var $content = $('.epochContent');
	$content.height($(this).height() - 50);
	$content.find('iframe').each(function() {
		$(this).height($content.height());
	});
}).resize();

$(function() {
	var href = window.location.hash;
	if(href){
		href = href.replace('#', '');
		var leng = href.indexOf("?");
		var hrefHash = href.substr(0,leng);
		$(".epochMenuParent").parent().removeClass("active");
		$(".epochMenu").parent().removeClass("active");
		//这行代码先去掉，加上会导致一个样式问题
		//$("a[data-href='"+hrefHash+"']").parents("li").addClass("active");
		window.main.location.href = getRootPath()+href;
	}
});

//获取项目根路径
function getRootPath() {
    // 当前网址
    var curWwwPath = window.document.location.href;
    // 主机地址之后的目录
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    // 获取主机地址
    var localhostPath = curWwwPath.substring(0, pos);
    // 获取带“/”的项目名
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    
    return (localhostPath + projectName);
}


