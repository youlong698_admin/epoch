-- --------------------------------------------------------
-- 主机:                           127.0.0.1
-- 服务器版本:                        5.7.24-log - MySQL Community Server (GPL)
-- 服务器操作系统:                      Win64
-- HeidiSQL 版本:                  9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- 导出 epoch-open 的数据库结构
CREATE DATABASE IF NOT EXISTS `epoch-open` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `epoch-open`;

-- 导出  表 epoch-open.sys_attachment_files 结构
CREATE TABLE IF NOT EXISTS `sys_attachment_files` (
  `ATTACHMENT_FILE_ID` varchar(50) NOT NULL,
  `BUSINESS_ID` varchar(50) NOT NULL COMMENT '业务ID',
  `BUSINESS_TYPE` varchar(50) DEFAULT NULL COMMENT '业务子类型',
  `LOCAL_UPLOAD_PATH` varchar(255) DEFAULT NULL COMMENT '本地上传路径',
  `SERVER_UPLOAD_PATH` varchar(255) DEFAULT NULL COMMENT '服务器上传路径',
  `FILE_ORIGINAL_NAME` varchar(255) DEFAULT NULL COMMENT '原始文件名',
  `FILE_NEW_NAME` varchar(255) DEFAULT NULL COMMENT '新文件名',
  `FILE_SIZE` bigint(20) DEFAULT NULL COMMENT '文件大小',
  `FILE_EXT_NAME` varchar(50) DEFAULT NULL COMMENT '文件扩展名',
  `DESCRIPTION` varchar(50) DEFAULT NULL COMMENT '描述',
  `CUST_TEXT01` varchar(50) DEFAULT NULL COMMENT '扩展字段1',
  `CUST_TEXT02` varchar(50) DEFAULT NULL,
  `CUST_TEXT03` varchar(50) DEFAULT NULL,
  `CUST_TEXT04` varchar(50) DEFAULT NULL,
  `CUST_TEXT05` varchar(50) DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ATTACHMENT_FILE_ID`),
  KEY `ATTACHMENT_FILE_ID` (`ATTACHMENT_FILE_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_attachment_files 的数据：~0 rows (大约)
DELETE FROM `sys_attachment_files`;
/*!40000 ALTER TABLE `sys_attachment_files` DISABLE KEYS */;
INSERT INTO `sys_attachment_files` (`ATTACHMENT_FILE_ID`, `BUSINESS_ID`, `BUSINESS_TYPE`, `LOCAL_UPLOAD_PATH`, `SERVER_UPLOAD_PATH`, `FILE_ORIGINAL_NAME`, `FILE_NEW_NAME`, `FILE_SIZE`, `FILE_EXT_NAME`, `DESCRIPTION`, `CUST_TEXT01`, `CUST_TEXT02`, `CUST_TEXT03`, `CUST_TEXT04`, `CUST_TEXT05`, `CREATE_BY`, `CREATE_DATE`, `UPDATE_BY`, `UPDATE_DATE`) VALUES
	('9e2808bec41c45dcbd17901e1ba6e068', '99001', NULL, 'license.lic', '/99001/1529640312264license_1529640313678.lic', 'license.lic', '1529640312264license_1529640313678.lic', 3138, 'lic', '', NULL, NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-22 12:05:13', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-22 12:05:13');
/*!40000 ALTER TABLE `sys_attachment_files` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_company 结构
CREATE TABLE IF NOT EXISTS `sys_company` (
  `COMPANY_ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPANY_NAME` varchar(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`COMPANY_ID`),
  KEY `COMPANY_ID` (`COMPANY_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_company 的数据：~0 rows (大约)
DELETE FROM `sys_company`;
/*!40000 ALTER TABLE `sys_company` DISABLE KEYS */;
INSERT INTO `sys_company` (`COMPANY_ID`, `COMPANY_NAME`) VALUES
	(1, 'EPOCH');
/*!40000 ALTER TABLE `sys_company` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_dict 结构
CREATE TABLE IF NOT EXISTS `sys_dict` (
  `ID` varchar(50) NOT NULL,
  `HEAD_FLAG` char(1) DEFAULT NULL,
  `DICT_CODE` varchar(255) DEFAULT NULL COMMENT '字典编码',
  `DICT_NAME` varchar(255) DEFAULT NULL COMMENT '字典名称',
  `DICT_COMMENTS` varchar(255) DEFAULT NULL COMMENT '字典描述',
  `DICT_FLAG` char(1) DEFAULT NULL,
  `ITEM_FLAG` char(1) DEFAULT NULL COMMENT '是否启用',
  `ITEM_CODE` varchar(50) DEFAULT NULL COMMENT '字典项 key',
  `ITEM_VALUE` varchar(50) DEFAULT NULL COMMENT '字典项 值',
  `ITEM_SEQ` int(11) DEFAULT NULL COMMENT '排序',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '最后一次修改人',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '最后一次修改时间',
  `DEPT_ID` varchar(50) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_dict 的数据：~84 rows (大约)
DELETE FROM `sys_dict`;
/*!40000 ALTER TABLE `sys_dict` DISABLE KEYS */;
INSERT INTO `sys_dict` (`ID`, `HEAD_FLAG`, `DICT_CODE`, `DICT_NAME`, `DICT_COMMENTS`, `DICT_FLAG`, `ITEM_FLAG`, `ITEM_CODE`, `ITEM_VALUE`, `ITEM_SEQ`, `CREATE_BY`, `CREATE_DATE`, `UPDATE_BY`, `UPDATE_DATE`, `DEPT_ID`) VALUES
	('003d8a62c1c54876b205f2ede9a31906', 'N', 'BS_GENDER', '性别', NULL, NULL, 'Y', '0', '未知', 3, '496dbbfb68bf11e8b2c9005056c00001', '2018-09-01 02:46:02', '496dbbfb68bf11e8b2c9005056c00001', '2018-09-01 02:46:02', NULL),
	('01891ec48913442cb0f5027c2de894e9', 'Y', 'SYS_INFO_TYPE', '消息类型', '消息类型', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:20:32', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:20:32', NULL),
	('01c48fa3439b4f42ba2f166a4e785351', 'Y', 'PUB_SYS_MODULE', '公共模块', '模块', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:09:21', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:14:47', NULL),
	('169cea126c5211e8ba01005056c00001', 'N', 'PUB_Y_N', '是否', '是否', NULL, 'Y', 'N', '否', 2, '496dbbfb68bf11e8b2c9005056c00001', NULL, '1', '2018-01-17 01:25:50', NULL),
	('16cb8712274e4073bff7ff18e727c022', 'N', 'SYS_INTFC_TYPE', '员工类型', NULL, NULL, 'Y', 'INTERNAL', '内部', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:16:55', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:16:55', NULL),
	('1ab07afa97274c859a46159200c61364', 'N', 'SYS_MENU_TYPE', '菜单类型', NULL, NULL, 'Y', 'OUTSIDE', '外部URL', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-11 21:08:08', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:21:12', NULL),
	('1e529c4b99a54ddfade0386fb12b89a2', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '5', '5月', 5, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', NULL),
	('23f1f13a1eb449cdbf8b5a3b80ee1e55', 'N', 'SYS_SERIAL_NUMBER', '流水号重置规则', NULL, NULL, 'Y', 'SECOND', '按秒重置', 7, '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', NULL),
	('29edaa6c09c54bdda20ae5816fa7a8d7', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '7', '7月', 7, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', NULL),
	('2a58796290134c23a63e6653996e4c52', 'N', 'SYS_JOB_STATUS', '定时任务状态', NULL, NULL, 'Y', '1', '暂停', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:17:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:17:36', NULL),
	('2c2492921e3e4f72ac17d2989b3991c7', 'N', 'PUB_QUARTER', '季度', NULL, NULL, 'Y', '3', '3季度', 3, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:43', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:43', NULL),
	('322e605048084aaab215e8ea34e4c593', 'N', 'SYS_JOB_STATUS', '定时任务状态', NULL, NULL, 'Y', '0', '启用', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:17:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:17:36', NULL),
	('38a6259fe0984c589e7dd8a56fdc79bd', 'N', 'SYS_SERIAL_NUMBER', 'SYS_SERIAL_NUMBER', NULL, NULL, 'Y', 'YEAR', '按年重置', 4, '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', NULL),
	('39357fc527f140f58f5ad8ca5aa3c774', 'N', 'SYS_ROLE_TYPE', '角色类型', NULL, NULL, 'Y', 'SYSTEM', '系统角色', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-12 13:35:41', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-12 16:46:43', NULL),
	('3a52c1d715ba4be78f7f3f6ef039194c', 'Y', 'SYS_MENU_TYPE', '菜单类型', '内部和外部菜单俩种类型', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-11 21:08:08', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:21:12', NULL),
	('3b7c99c4e963449291dda57394ee77d0', 'N', 'SYS_DEPT_WF_ROLE', '部门流程角色', NULL, NULL, 'Y', 'DIVISION_HEAD', '处长', 4, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:37:59', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:40:07', NULL),
	('3bc64122ca394b73b541f18af5d7e1ea', 'N', 'SYS_INFO_TYPE', '消息类型', NULL, NULL, 'Y', '0', '公告', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:20:32', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:20:32', NULL),
	('43f197f94b3a462e83386da1de0de645', 'N', 'PUB_SUCESS_FAIL', '成功失败', NULL, NULL, 'Y', 'FAIL', '失败', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-24 13:49:49', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-24 13:49:49', NULL),
	('48d5117d6bf74e27a834226900992eb9', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '6', '6月', 6, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', NULL),
	('4e338384f74f4132ab692fe06763cf89', 'N', 'PUB_ENABLE_DISABLE', '启用禁用', NULL, NULL, 'Y', 'ENABLE', '启用', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-10 19:21:09', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-10 19:21:09', NULL),
	('4e7c8c30436c471f8f58066536b22caa', 'N', 'SYS_ROLE_TYPE', '角色类型', NULL, NULL, 'Y', 'WORKFLOW', '流程角色', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-12 13:35:41', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-12 16:46:43', NULL),
	('52065b855440405085df213d69fd302a', 'N', 'BS_GENDER', '性别', NULL, NULL, 'Y', '2', '女', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-09-01 02:46:02', '496dbbfb68bf11e8b2c9005056c00001', '2018-09-01 02:46:02', NULL),
	('54187ce04f364774966196bab6f23c82', 'N', 'PUB_VALID_INVALID', '生效失效', NULL, NULL, 'Y', 'VALID', '生效', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:18:30', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:18:30', NULL),
	('55ab5153cded4a939d61feda598d4061', 'N', 'SYS_LOG_TYPE', '系统操作日志类型', NULL, NULL, NULL, 'OPERATE', '操作', 5, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', NULL),
	('58126249cccf43329e15e0a67f00b9a7', 'N', 'PUB_QUARTER', '季度', NULL, NULL, 'Y', '2', '2季度', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:42', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:42', NULL),
	('5a4a27ad6bac47aca6d488a65f762f99', 'N', 'SYS_USER_STATUS', '用户状态', NULL, NULL, 'Y', 'LOCKING', '锁定', 3, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:13:56', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:13:56', NULL),
	('5b2073579b0c4a2bbfbd6ea59e5c65f5', 'N', 'PUB_QUARTER', '季度', NULL, NULL, 'Y', '1', '1季度', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:42', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:42', NULL),
	('5e31f71ebab04d129ce8ceebdca6f5de', 'N', 'SYS_DEPT_WF_ROLE', '部门流程角色', NULL, NULL, 'Y', 'SECTION_MANAGER', '科长', 5, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:38:25', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:38:25', NULL),
	('5eb25f8d1fb74dcd808d7d7d6bc76649', 'Y', 'PUB_QUARTER', '季度', '季度', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:42', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:42', NULL),
	('63b8f702fc0f47c8b8c8532676ebfc8b', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '8', '8月', 8, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', NULL),
	('649a1e16949846ce917bd5c699d1c3ab', 'N', 'SYS_SERIAL_NUMBER', '流水号重置规则', NULL, NULL, 'Y', 'NEVER', '不重置', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', NULL),
	('6562319f72884f9bbca8cb3c2cff8986', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '2', '2月', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', NULL),
	('67c0155c41cc4a10baa0b1d89a723d27', 'Y', 'PUB_ENABLE_DISABLE', '启用禁用', '启用禁用', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-10 19:21:09', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-13 14:09:45', NULL),
	('6b2902d8e08f46178c912695f8ce83a5', 'N', 'SYS_LOG_TYPE', '系统操作日志类型', NULL, NULL, NULL, 'LOGIN', '登陆', 5, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', NULL),
	('6c28cb61f15740acb5fc72914953192d', 'N', 'PUB_SUCESS_FAIL', '成功失败', NULL, NULL, 'Y', 'SUCESS', '成功', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-24 13:49:48', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-24 13:49:48', NULL),
	('6dbc75739c1b400bbee0b2d6cc9c3658', 'N', 'SYS_INFO_TYPE', '消息类型', NULL, NULL, 'Y', '1', '统一维护通知', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:20:32', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:20:32', NULL),
	('741a295f2e0149afa614325459cb7bd7', 'N', 'PUB_QUARTER', '季度', NULL, NULL, 'Y', '4', '4季度', 4, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:43', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:15:43', NULL),
	('75caa12ccced459bb4e1af7b60cca567', 'N', 'SYS_INTFC_TYPE', '员工类型', NULL, NULL, 'Y', 'OUTERNAL', '外部', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:16:55', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:16:55', NULL),
	('77db48072c1644148389102f65a767ef', 'N', 'SYS_ORG_TYPE', '组织类型', NULL, NULL, 'Y', 'COMPANY', '公司', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-14 11:12:49', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-14 11:12:49', NULL),
	('7a9e6375ab2340c3b68d5039582de5e1', 'N', 'PUB_VALID_INVALID', '生效失效', NULL, NULL, 'Y', 'INVALID', '失效', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:18:30', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:18:30', NULL),
	('7d671535164e44d3992e3a394d4ecc81', 'N', 'SYS_USER_STATUS', '用户状态', NULL, NULL, 'Y', 'ENABLE', '启用', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:09:13', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:09:13', NULL),
	('827be9bf5aa5428aa6e99878c687ebcb', 'N', 'SYS_MENU_TYPE', '菜单类型', NULL, NULL, 'Y', 'INSIDE', '系统URL', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-11 21:08:08', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:21:12', NULL),
	('84bfc9c8c79041e0b65cca3c2220d6a9', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '12', '12月', 12, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', NULL),
	('8777ae4463f14b1995117567ec1079e1', 'Y', 'SYS_DEPT_WF_ROLE', '部门流程角色', '部门流程角色', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:36:06', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:40:07', NULL),
	('8844b2f811c64376825843a4b7cc520b', 'Y', 'BS_GENDER', '性别', '性别', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-09-01 02:46:02', '496dbbfb68bf11e8b2c9005056c00001', '2018-09-01 02:46:02', NULL),
	('886cc774a51443e68084e4827a09d3f2', 'N', 'SYS_SERIAL_NUMBER', '流水号重置规则', NULL, NULL, 'Y', 'HOUR', '按小时重置', 5, '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', NULL),
	('8dd5d4b0884a4de291a00613dc65e4c2', 'Y', 'PUB_MONTH', '月份', '月份', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:37', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:37', NULL),
	('8e8506886c5111e8ba01005056c00001', 'Y', 'PUB_Y_N', '是否', '是否是', 'Y', 'Y', NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', NULL, '1', '2018-01-17 01:25:50', NULL),
	('8efaaaf0a1ad454a83d85a3704a81fc1', 'Y', 'SYS_JOB_STATUS', '定时任务状态', '定时任务状态', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:17:35', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:17:35', NULL),
	('8ffc60a58e5745c18fca45849db71e28', 'Y', 'SYS_INTFC_TYPE', '员工类型', '员工类型', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:16:55', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:16:55', NULL),
	('904baf32f1bd48dd9aa3905afef693f5', 'N', 'SYS_LOG_TYPE', '系统操作日志类型', NULL, NULL, NULL, 'WF', '流程', 7, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', NULL),
	('92d676f36c5111e8ba01005056c00001', 'Y', 'SYS_ORG_TYPE', '组织类型', '组织类型', 'Y', 'Y', NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-14 11:12:49', NULL),
	('9558caf911a24edb8a1b0f829e1517e6', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '4', '4月', 4, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', NULL),
	('9b7b8fffa83a49a7af27cf990911e9c9', 'N', 'SYS_LOG_TYPE', '系统操作日志类型', NULL, NULL, NULL, 'DELETE', '删除', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', NULL),
	('9db5edf66c5111e8ba01005056c00001', 'Y', 'SYS_USER_STATUS', '用户状态', '用户状态', 'Y', 'Y', NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-01-20 14:22:52', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:13:55', NULL),
	('9f433db0c6e2421e9d14907e6f4e9d72', 'N', 'PUB_ENABLE_DISABLE', '启用禁用', NULL, NULL, 'Y', 'DISABLE', '禁用', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-10 19:21:09', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-10 19:21:09', NULL),
	('a21a18fa6c5111e8ba01005056c00001', 'Y', 'SYS_ROLE_TYPE', '角色类型', '角色类型', 'Y', 'Y', NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-01-21 15:33:41', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-12 16:46:43', NULL),
	('a66e193c6c5111e8ba01005056c00001', 'Y', 'PUB_SYS_MODULE', '系统模块类别', '系统模块类别', 'Y', 'Y', NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-01-21 15:46:37', '1', '2018-01-21 15:46:37', NULL),
	('ac448befc3694148b6b001ce530d5113', 'N', 'SYS_LOG_TYPE', '系统操作日志类型', NULL, NULL, NULL, 'LOGOUT', '登出', 6, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', NULL),
	('ade7af796c5111e8ba01005056c00001', 'Y', 'PUB_GENDER', '性别', '性别', 'Y', 'Y', NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-01-21 15:48:26', '1', '2018-01-21 15:48:26', NULL),
	('aeb16ae6e32f404eb4cb8c999a9df61a', 'N', 'SYS_JOB_STATUS', '定时任务状态', NULL, NULL, 'Y', '2', '禁用', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:17:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:17:36', NULL),
	('b03f9245feda4dd6994e0967d31462ae', 'Y', 'SYS_LOG_TYPE', '系统操作日志类型', '系统操作日志类型', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:35', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:35', NULL),
	('b1bd1ccf6c5111e8ba01005056c00001', 'Y', 'PUB_ON_OFF', '开启关闭', '开启关闭', 'Y', 'Y', NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-01-21 15:50:02', '1', '2018-01-21 15:50:02', NULL),
	('b435fbb8ef20463fbf26b999db97dca4', 'N', 'SYS_DEPT_WF_ROLE', '部门流程角色', NULL, NULL, 'Y', 'GENERAL_STAFF', '普通员工', 6, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:39:06', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:39:06', NULL),
	('bb4e4d3cdf2444a89250db6988dc8955', 'Y', 'SYS_SERIAL_NUMBER', '流水号重置规则', '流水号重置规则', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', NULL),
	('bf9143d8104d497c838dfb6a6021bc83', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '3', '3月', 3, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', NULL),
	('c52e307da97f4f36b300053f0883021d', 'Y', 'PUB_SUCESS_FAIL', '成功失败', '成功失败', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-24 13:49:48', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-24 13:49:48', NULL),
	('c9484d15062d4a78b91cd3cade120ca6', 'N', 'SYS_LOG_TYPE', '系统操作日志类型', NULL, NULL, NULL, 'UPDATE', '修改', 3, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', NULL),
	('d01cb10b774e4a51961c109fcc66ef5c', 'N', 'SYS_LOG_TYPE', '系统操作日志类型', NULL, NULL, NULL, 'QUERY', '查询', 4, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:36', NULL),
	('d486003c1fb94e3eaa08802c32de9eb6', 'N', 'PUB_SYS_MODULE', '公共模块', NULL, NULL, 'Y', 'SYS', '系统模块', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:15:03', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-23 00:15:03', NULL),
	('d4b77b0dd1fb4a55a3db5df6e5f461ad', 'N', 'SYS_SERIAL_NUMBER', '流水号重置规则', NULL, NULL, 'Y', 'DAY', '按日重置', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', NULL),
	('d5cb7df0b9df44b8972d0bb58af6da0f', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '9', '9月', 9, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', NULL),
	('e07382b8aede474086a13296d7e385e0', 'N', 'SYS_SERIAL_NUMBER', '流水号重置规则', NULL, NULL, 'Y', 'MINUTE', '按分重置', 6, '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', NULL),
	('e240f3debe6d4099a82666b6065a5892', 'N', 'SYS_DEPT_WF_ROLE', '部门流程角色', NULL, NULL, 'Y', 'DEPARTMENT_MANAGER', '经理', 3, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:37:59', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:40:07', NULL),
	('e3c0a94f60ed4d7ca428f7c27c4a5673', 'N', 'SYS_LOG_TYPE', '系统操作日志类型', NULL, NULL, NULL, 'NEW', '新增', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:35', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:20:35', NULL),
	('eab6634214bb4ffc861cb0a62bc82d60', 'N', 'SYS_SERIAL_NUMBER', '流水号重置规则', NULL, NULL, 'Y', 'MONTH', '按月重置', 3, '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', '496dbbfb68bf11e8b2c9005056c00001', '2018-10-07 19:42:26', NULL),
	('eb7a324640a74d8598b03a58c7b6c9c6', 'N', 'SYS_USER_STATUS', '用户状态', NULL, NULL, 'Y', 'INVALID', '失效', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:09:40', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:13:56', NULL),
	('ed5863796c5111e8ba01005056c00001', 'N', 'PUB_Y_N', '是否', '是否', NULL, 'Y', 'Y', '是', 1, '496dbbfb68bf11e8b2c9005056c00001', NULL, '1', '2018-01-17 01:25:50', NULL),
	('f29ee749f10f4d28b38474505bcf6e29', 'Y', 'PUB_VALID_INVALID', '生效失效', '生效失效', 'Y', NULL, NULL, NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:18:30', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:18:30', NULL),
	('f595d9d8a3af47a09a452ae95553f6dd', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '1', '1月', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:38', NULL),
	('f7cb902bb020457499528e9d52538953', 'N', 'BS_GENDER', '性别', NULL, NULL, 'Y', '1', '男', 1, '496dbbfb68bf11e8b2c9005056c00001', '2018-09-01 02:46:02', '496dbbfb68bf11e8b2c9005056c00001', '2018-09-01 02:46:02', NULL),
	('f8d814041f1643589cc2aa1ca597380e', 'N', 'SYS_ORG_TYPE', '组织类型', NULL, NULL, 'Y', 'DEPT', '部门', 2, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-14 11:12:49', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-14 11:12:49', NULL),
	('fae328fa07f2474db93e772b0db6647b', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '10', '10月', 10, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', NULL),
	('fe2c0b68e3ad4d43bb08250112c8ffb0', 'N', 'PUB_MONTH', '月份', NULL, NULL, 'Y', '11', '11月', 11, '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-27 00:14:39', NULL);
/*!40000 ALTER TABLE `sys_dict` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_info 结构
CREATE TABLE IF NOT EXISTS `sys_info` (
  `ID` varchar(50) NOT NULL,
  `INFO_TITLE` varchar(255) NOT NULL COMMENT '消息标题',
  `INFO_TYPE` varchar(20) NOT NULL COMMENT '消息类型',
  `INFO_DESC` text NOT NULL COMMENT '消息内容',
  `STATUS` varchar(10) NOT NULL COMMENT '状态',
  `ATTRI_TEXT01` varchar(50) DEFAULT NULL,
  `ATTRI_TEXT02` varchar(50) DEFAULT NULL,
  `ATTRI_TEXT03` varchar(50) DEFAULT NULL,
  `ATTRI_NUMBER01` double DEFAULT NULL,
  `ATTRI_NUMBER02` double DEFAULT NULL,
  `ATTRI_NUMBER03` double DEFAULT NULL,
  `CREATE_BY` varchar(50) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `CREATE_DEPT` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_info 的数据：~0 rows (大约)
DELETE FROM `sys_info`;
/*!40000 ALTER TABLE `sys_info` DISABLE KEYS */;
INSERT INTO `sys_info` (`ID`, `INFO_TITLE`, `INFO_TYPE`, `INFO_DESC`, `STATUS`, `ATTRI_TEXT01`, `ATTRI_TEXT02`, `ATTRI_TEXT03`, `ATTRI_NUMBER01`, `ATTRI_NUMBER02`, `ATTRI_NUMBER03`, `CREATE_BY`, `CREATE_DATE`, `UPDATE_BY`, `UPDATE_DATE`, `CREATE_DEPT`) VALUES
	('1', '测试版本发布', '1', '测试版本发布了', 'ENABLE', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-10-07 19:47:23', NULL, '2018-10-07 19:47:36', NULL);
/*!40000 ALTER TABLE `sys_info` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_job 结构
CREATE TABLE IF NOT EXISTS `sys_job` (
  `ID` int(11) NOT NULL,
  `JOB_NAME` varchar(50) NOT NULL COMMENT '任务名称',
  `JOB_GROUP` varchar(50) NOT NULL COMMENT '任务属组',
  `STATUS` varchar(10) NOT NULL COMMENT '任务状态',
  `JOB_CLASS` varchar(200) NOT NULL COMMENT '目标类',
  `CRON_EXPRESSION` varchar(200) NOT NULL COMMENT 'cron表达式',
  `DESCRIPTION` varchar(200) NOT NULL COMMENT '任务描述',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_job 的数据：~0 rows (大约)
DELETE FROM `sys_job`;
/*!40000 ALTER TABLE `sys_job` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_job` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_log 结构
CREATE TABLE IF NOT EXISTS `sys_log` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` varchar(50) NOT NULL COMMENT '用户ID',
  `USER_NAME` varchar(50) NOT NULL,
  `LOGIN_IP` varchar(20) DEFAULT NULL COMMENT '登录IP',
  `LOGIN_DATE` datetime DEFAULT NULL COMMENT '登录时间',
  `LOOUT_DATE` datetime DEFAULT NULL COMMENT '等出时间',
  `DEPT_NAME` varchar(50) DEFAULT NULL COMMENT '用户部门',
  `BUSSINESS_NUMBER` varchar(50) DEFAULT NULL COMMENT '业务单据ID',
  `OPERATION_TYPE` varchar(50) DEFAULT NULL COMMENT '操作类型',
  `OPERATION_NOTE` varchar(200) DEFAULT NULL COMMENT '操作描述',
  `OPERATION_STATUS` varchar(10) DEFAULT NULL COMMENT '操作状态，成功失败',
  `OPERATION_FAILED_DESC` text COMMENT '操作失败信息，记录异常日志',
  `MENU_ID` int(11) DEFAULT NULL COMMENT '操作菜单',
  `MENU_NAME` varchar(50) DEFAULT NULL COMMENT '菜单名称',
  `MODULE_CODE` varchar(50) DEFAULT NULL COMMENT '模块code',
  `OPERATION_ROLE_ID` varchar(50) DEFAULT NULL COMMENT '角色ID',
  `OPERATION_ROLE_DESC` varchar(50) DEFAULT NULL COMMENT '角色信息',
  `CREATE_BY` int(11) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `UPDATE_BY` int(11) DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  `CREATE_DEPT` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=utf8;

-- 导出  表 epoch-open.sys_log_manage 结构
CREATE TABLE IF NOT EXISTS `sys_log_manage` (
  `id` varchar(50) NOT NULL,
  `menu_id` varchar(50) DEFAULT NULL,
  `new_btn` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_log_manage 的数据：~0 rows (大约)
DELETE FROM `sys_log_manage`;
/*!40000 ALTER TABLE `sys_log_manage` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_log_manage` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_menu 结构
CREATE TABLE IF NOT EXISTS `sys_menu` (
  `ID` varchar(50) NOT NULL COMMENT '菜单ID',
  `MENU_CODE` varchar(50) DEFAULT NULL COMMENT '菜单编码',
  `MENU_TYPE` tinyint(4) DEFAULT NULL,
  `MENU_NAME` varchar(50) NOT NULL COMMENT '菜单名称',
  `MENU_LEVEL` int(2) DEFAULT '1',
  `PARENT_ID` varchar(50) DEFAULT NULL COMMENT '父ID',
  `COMMENTS` varchar(255) DEFAULT NULL COMMENT '菜单说明',
  `MENU_URL` varchar(50) DEFAULT NULL COMMENT '菜单地址',
  `MENU_ORDER` int(11) DEFAULT NULL,
  `MENU_ICON` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `MENU_URL_TYPE` varchar(50) DEFAULT NULL COMMENT '菜单类型',
  `STATUS` varchar(10) NOT NULL COMMENT '是否启用',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '修改人',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '修改时间',
  `CREATE_DEPT` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_menu 的数据：~15 rows (大约)
DELETE FROM `sys_menu`;
/*!40000 ALTER TABLE `sys_menu` DISABLE KEYS */;
INSERT INTO `sys_menu` (`ID`, `MENU_CODE`, `MENU_TYPE`, `MENU_NAME`, `MENU_LEVEL`, `PARENT_ID`, `COMMENTS`, `MENU_URL`, `MENU_ORDER`, `MENU_ICON`, `MENU_URL_TYPE`, `STATUS`, `CREATE_BY`, `CREATE_DATE`, `UPDATE_BY`, `UPDATE_DATE`, `CREATE_DEPT`) VALUES
	('0', 'systemModule', 0, '系统管理', 1, NULL, '系统模块', '/sys/syUser/index', 1, 'fa fa-cogs', 'INSIDE', 'ENABLE', NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 12:02:27', NULL),
	('0b6d1af8762f4554b67d9a4508611514', 'infoManage', 0, '消息管理', 1, '0', '消息管理', '/sys/info/index', 9, 'fa fa-bandcamp', 'INSIDE', 'ENABLE', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 03:00:41', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 12:28:46', NULL),
	('1', 'systemModule', 0, '测试界面', 1, NULL, '测试界面', '/sys/test/index', 5, 'fa fa-file-text-o', 'INSIDE', 'ENABLE', NULL, NULL, '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 12:02:27', NULL),
	('11ed376f505711e88d20000c2950dab5', 'orgManage', 0, '部门管理', 1, '0', '部门管理', '/sys/org/index', 6, 'fa fa-sitemap', 'INSIDE', 'ENABLE', NULL, NULL, NULL, NULL, NULL),
	('402880f25b18eaca015b18eaca4f0000', 'dataDictionary', 0, '数据字典', 1, '0', '数据字典', '/sys/dict/index', 7, 'fa fa-file-text', 'INSIDE', 'ENABLE', NULL, NULL, NULL, NULL, NULL),
	('4028f6815af3ce54015af3d1ad6100016', 'systemControl', 0, '系统监控', 0, '0', '系统监控', '/druid/index.html', 10, 'fa fa-file-text', 'INSIDE', 'ENABLE', NULL, NULL, NULL, NULL, NULL),
	('46a2246f14b84e5eb313e8c98d71a7be', 'logManage', 0, '日志管理', 1, '0', '日志管理', '/sys/log/index', 11, 'fa fa-leaf', 'INSIDE', 'ENABLE', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:54:40', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:54:40', NULL),
	('512e99a1b45311e89c780235d2b38928', 'dataDictionary', 0, '流水号生成规则', 1, '0', '编号生成规则', '/sys/serialnumber/index', 4, 'fa fa-file-text', 'INSIDE', 'ENABLE', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 02:56:49', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 03:09:10', NULL),
	('8a8ab0b246dc81120146dc8180d9001d', 'userManage', 0, '用户管理', 1, '0', '用户管理', '/sys/user/index', 1, 'fa fa-users', 'INSIDE', 'ENABLE', NULL, NULL, NULL, NULL, NULL),
	('9d09f8cfb68811e89c780235d2b38928', 'codeMenu', 0, '代码生成器', 1, '0', '代码生成器', '/sys/generator/index', 22, 'fa fa-file-text', 'INSIDE', 'ENABLE', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 02:56:49', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 03:09:10', NULL),
	('a3f2825152e911e88d20000c2950dab5', 'menuManage', 0, '菜单管理', 1, '0', '菜单管理', '/sys/menu/index', 4, 'fa fa-file-text', 'INSIDE', 'ENABLE', NULL, NULL, NULL, NULL, NULL),
	('a99562cf6eb54510b47040b36d58ea00', 'userrolrManage', 0, '用户角色分配', 1, '0', '用户角色分配', '/sys/userrole/index', 3, 'fa fa-user', 'INSIDE', 'ENABLE', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 02:58:49', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 02:58:49', NULL),
	('ad20c526820f4b40a3354a0430f4016e', 'jobManage', 0, '定时任务', 1, '0', '定时任务', '/sys/job/index', 8, 'fa fa-file-text', 'INSIDE', 'ENABLE', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:53:00', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 11:53:00', NULL),
	('d092ddef560c11e88d20000c2950dab5', 'roleManage', 0, '角色管理', 1, '0', '角色管理', '/sys/role/index', 2, 'fa fa-user-md', 'INSIDE', 'ENABLE', NULL, NULL, NULL, NULL, NULL),
	('ff2cd5364d96442bbb552ede74450d1e', 'roleMenu', 0, '角色菜单分配', 1, '0', '角色菜单分配', '/sys/rolemenu/index', 5, 'fa fa-file-text', 'INSIDE', 'ENABLE', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 02:56:49', '496dbbfb68bf11e8b2c9005056c00001', '2018-06-18 03:09:10', NULL);
/*!40000 ALTER TABLE `sys_menu` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_org 结构
CREATE TABLE IF NOT EXISTS `sys_org` (
  `ID` varchar(50) NOT NULL,
  `ORG_CODE` varchar(50) DEFAULT NULL COMMENT '组织编码',
  `ORG_NAME` varchar(50) DEFAULT NULL COMMENT '组织名称',
  `PARENT_ID` varchar(50) DEFAULT NULL,
  `PARENT_NAME` varchar(50) DEFAULT NULL,
  `ORG_TYPE` varchar(50) DEFAULT NULL COMMENT '组织类型',
  `SHORT_NAME` varchar(50) DEFAULT NULL COMMENT '组织简称',
  `LONG_NAME` varchar(255) DEFAULT NULL COMMENT '组织全称',
  `STATE` varchar(10) DEFAULT NULL,
  `STATUS` varchar(10) DEFAULT NULL COMMENT '组织状态',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '最后一次更改人',
  `ORDER` int(11) DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '最后一次更改时间',
  `COMPANY_NAME` varchar(255) DEFAULT NULL COMMENT '所属公司',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_org 的数据：~9 rows (大约)
DELETE FROM `sys_org`;
/*!40000 ALTER TABLE `sys_org` DISABLE KEYS */;
INSERT INTO `sys_org` (`ID`, `ORG_CODE`, `ORG_NAME`, `PARENT_ID`, `PARENT_NAME`, `ORG_TYPE`, `SHORT_NAME`, `LONG_NAME`, `STATE`, `STATUS`, `CREATE_BY`, `CREATE_DATE`, `UPDATE_BY`, `ORDER`, `UPDATE_DATE`, `COMPANY_NAME`) VALUES
	('0', 'epoch', 'epoch责任有限公司', NULL, NULL, 'COMPANY', 'epoch责任有限公司', 'epoch责任有限公司', 'closed', 'ENABLE', NULL, NULL, '1', NULL, '2018-01-15 23:46:50', 'epoch有限责任公司'),
	('015af32f68c211e8b2c9005056c00001', 'shichang3', '市场规划3', 'f17b385068c111e8b2c9005056c00001', '市场部', 'DEPT', '市场规划3', '市场规划3', 'open', 'ENABLE', NULL, NULL, NULL, NULL, NULL, 'epoch有限责任公司'),
	('da14e0ba68c111e8b2c9005056c00001', 'renliziyuan', '人力资源部', '0', 'epoch责任有限公司', 'DEPT', '人力资源部', '人力资源部', 'closed', 'ENABLE', NULL, NULL, NULL, NULL, NULL, 'epoch有限责任公司'),
	('e291cc2c68c111e8b2c9005056c00001', 'renli', '人力资源', 'da14e0ba68c111e8b2c9005056c00001', '人力资源部', 'DEPT', '人力资源', '人力资源', 'open', 'ENABLE', NULL, NULL, NULL, NULL, NULL, 'epoch有限责任公司'),
	('e5bd685668c111e8b2c9005056c00001', 'yuangongnengli', '员工能力发展', 'da14e0ba68c111e8b2c9005056c00001', '人力资源部', 'DEPT', '员工能力发展', '员工能力发展', 'open', 'ENABLE', NULL, NULL, NULL, NULL, NULL, 'epoch有限责任公司'),
	('f17b385068c111e8b2c9005056c00001', 'shichang', '市场部', '0', 'epoch责任有限公司', 'DEPT', '市场部', '市场部', 'closed', 'ENABLE', NULL, NULL, NULL, NULL, NULL, 'epoch有限责任公司'),
	('f781eddc68c111e8b2c9005056c00001', 'shangyefenxi', '商业分析调研', 'f17b385068c111e8b2c9005056c00001', '市场部', 'DEPT', '商业分析调研', '商业分析调研', 'open', 'ENABLE', NULL, NULL, NULL, NULL, NULL, 'epoch有限责任公司'),
	('fba0d93768c111e8b2c9005056c00001', 'shichang1', '市场规划1', 'f17b385068c111e8b2c9005056c00001', '市场部', 'DEPT', '市场规划1', '市场规划1', 'open', 'ENABLE', NULL, NULL, NULL, NULL, NULL, 'epoch有限责任公司'),
	('fe6f35f568c111e8b2c9005056c00001', 'shichang2', '市场规划2', 'f17b385068c111e8b2c9005056c00001', '市场部', 'DEPT', '市场规划2', '市场规划2', 'open', 'ENABLE', NULL, NULL, NULL, NULL, NULL, 'epoch有限责任公司');
/*!40000 ALTER TABLE `sys_org` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_oss 结构
CREATE TABLE IF NOT EXISTS `sys_oss` (
  `id` varchar(50) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `CREATE_BY` varchar(255) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_oss 的数据：~0 rows (大约)
DELETE FROM `sys_oss`;
/*!40000 ALTER TABLE `sys_oss` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_oss` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_role 结构
CREATE TABLE IF NOT EXISTS `sys_role` (
  `ID` varchar(50) NOT NULL COMMENT 'ID',
  `ROLE_CODE` varchar(50) DEFAULT NULL COMMENT '角色编码',
  `ROLE_NAME` varchar(50) DEFAULT NULL COMMENT '角色名称',
  `COMMENTS` varchar(50) DEFAULT NULL COMMENT '角色描述',
  `ROLE_TYPE` varchar(20) DEFAULT NULL COMMENT '角色类型，系统角色和流程角色',
  `STATUS` varchar(10) DEFAULT NULL COMMENT '角色状态',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '最后一次更改人',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '最后一次更改时间',
  `CREATE_DEPT` varchar(50) DEFAULT NULL COMMENT '创建人部门ID',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_role 的数据：~8 rows (大约)
DELETE FROM `sys_role`;
/*!40000 ALTER TABLE `sys_role` DISABLE KEYS */;
INSERT INTO `sys_role` (`ID`, `ROLE_CODE`, `ROLE_NAME`, `COMMENTS`, `ROLE_TYPE`, `STATUS`, `CREATE_BY`, `CREATE_DATE`, `UPDATE_BY`, `UPDATE_TIME`, `CREATE_DEPT`) VALUES
	('d2545eedc30f4ab0ae0025f26d1932ad', 'let', '菜鸟裹裹', '果果果', 'SYSTEM', 'ENABLE', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-16 09:54:32', '496dbbfb68bf11e8b2c9005056c00001', NULL, 'e291cc2c68c111e8b2c9005056c00001'),
	('d37f7ead6dfe11e88310005056c00001', 'adminManager', '超级管理员', '超级管理员', 'SYSTEM', 'ENABLE', '1', '2018-01-21 16:15:53', '496dbbfb68bf11e8b2c9005056c00001', '2018-02-11 13:47:26', NULL),
	('dc0219e36dfe11e88310005056c00001', NULL, '业务管理员', '业务管理员', 'SYSTEM', 'ENABLE', '1', '2018-01-21 11:05:53', '496dbbfb68bf11e8b2c9005056c00001', '2018-02-11 13:47:27', NULL),
	('e0b8bb9b6dfe11e88310005056c00001', NULL, '总经理', '总经理', 'SYSTEM', 'ENABLE', '1', '2018-01-21 16:05:52', '1', '2018-02-11 13:47:38', NULL),
	('e45eaf406dfe11e88310005056c00001', NULL, '部门经理', '部门经理', 'SYSTEM', 'ENABLE', '1', '2018-01-21 16:05:58', '496dbbfb68bf11e8b2c9005056c00001', '2018-02-11 13:47:39', NULL),
	('e84ef75c6dfe11e88310005056c00001', 'workflowManager', '流程角色管理', '流程角色管理', 'SYSTEM', 'ENABLE', '1', '2018-01-21 15:57:00', '1', '2018-02-11 13:47:40', NULL),
	('eba674c86dfe11e88310005056c00001', 'deptpartment', '部门经理', '部门经理', 'WORKFLOW', 'ENABLE', '1', '2018-01-21 15:57:38', '1', '2018-02-11 13:47:40', NULL),
	('ef0eb1386dfe11e88310005056c00001', NULL, '普通用户', '普通用户', 'SYSTEM', 'ENABLE', '1', '2018-01-21 16:01:54', '496dbbfb68bf11e8b2c9005056c00001', '2018-02-11 13:47:25', NULL);
/*!40000 ALTER TABLE `sys_role` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_role_menu 结构
CREATE TABLE IF NOT EXISTS `sys_role_menu` (
  `ID` varchar(50) NOT NULL,
  `ROLE_ID` varchar(50) NOT NULL,
  `MENU_ID` varchar(50) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_role_menu 的数据：~19 rows (大约)
DELETE FROM `sys_role_menu`;
/*!40000 ALTER TABLE `sys_role_menu` DISABLE KEYS */;
INSERT INTO `sys_role_menu` (`ID`, `ROLE_ID`, `MENU_ID`) VALUES
	('12c69038d39d4b4f95b703c1de5c31af', 'e0b8bb9b6dfe11e88310005056c00001', '283da1ab6dbf4ddeae2644c0ded61631'),
	('30ba7d3e42804f8db884db3269884c5b', 'dc0219e36dfe11e88310005056c00001', 'a3f2825152e911e88d20000c2950dab5'),
	('51a2fc0225374028abcef127b079b646', 'dc0219e36dfe11e88310005056c00001', 'a99562cf6eb54510b47040b36d58ea00'),
	('5f4aa2d60e7c4568b9267710e9ac0fea', 'eba674c86dfe11e88310005056c00001', '283da1ab6dbf4ddeae2644c0ded61631'),
	('6042634993e34be0a401a9d28b391634', 'ef0eb1386dfe11e88310005056c00001', 'a99562cf6eb54510b47040b36d58ea00'),
	('6c3544c71fb0402ab96074d0f2d0671d', 'd2545eedc30f4ab0ae0025f26d1932ad', 'ff2cd5364d96442bbb552ede74450d1e'),
	('884ad1436ff04eeab05f1e599a102358', 'd37f7ead6dfe11e88310005056c00001', 'ff2cd5364d96442bbb552ede74450d1e'),
	('8bdc8c6b34574f1395f2aa938958a7ff', 'ef0eb1386dfe11e88310005056c00001', 'd092ddef560c11e88d20000c2950dab5'),
	('91ea47fad8374feeaedf9498b1f906ab', 'd2545eedc30f4ab0ae0025f26d1932ad', 'a99562cf6eb54510b47040b36d58ea00'),
	('99d38572d1414984aad05d11c365bcfa', 'ef0eb1386dfe11e88310005056c00001', '8a8ab0b246dc81120146dc8180d9001d'),
	('b2a4caa7263f4014a39b3793b76b6853', 'd37f7ead6dfe11e88310005056c00001', '11ed376f505711e88d20000c2950dab5'),
	('b84db39d563b487d8714e7de33c156ff', 'd2545eedc30f4ab0ae0025f26d1932ad', 'a3f2825152e911e88d20000c2950dab5'),
	('c261b116b8ad444f9d73eb522397e578', 'd37f7ead6dfe11e88310005056c00001', 'a3f2825152e911e88d20000c2950dab5'),
	('c9bdef9122c1452ea06d5053140ce8d0', 'dc0219e36dfe11e88310005056c00001', 'ff2cd5364d96442bbb552ede74450d1e'),
	('ce0f7506181f420caf4b0a490cc51319', 'dc0219e36dfe11e88310005056c00001', '46a2246f14b84e5eb313e8c98d71a7be'),
	('df0fb82748314a00a4f476105b723f7e', 'dc0219e36dfe11e88310005056c00001', '11ed376f505711e88d20000c2950dab5'),
	('f0136ec46aa943309ac1ebfecf3b7b1a', 'e84ef75c6dfe11e88310005056c00001', '283da1ab6dbf4ddeae2644c0ded61631'),
	('f960008743b24b0eb86bfa4adf975564', 'd37f7ead6dfe11e88310005056c00001', 'a99562cf6eb54510b47040b36d58ea00'),
	('fc02dcfc3977433d8087fe960979b209', 'd2545eedc30f4ab0ae0025f26d1932ad', '11ed376f505711e88d20000c2950dab5');
/*!40000 ALTER TABLE `sys_role_menu` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_schedule_job 结构
CREATE TABLE IF NOT EXISTS `sys_schedule_job` (
  `id` varchar(50) NOT NULL,
  `job_name` varchar(255) DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(255) DEFAULT NULL COMMENT '任务组',
  `job_status` varchar(255) DEFAULT NULL COMMENT '状态',
  `target_class` varchar(255) DEFAULT NULL COMMENT '执行目标类',
  `cron_expression` varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  `job_desc` varchar(255) DEFAULT NULL COMMENT '任务描述',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_schedule_job 的数据：~0 rows (大约)
DELETE FROM `sys_schedule_job`;
/*!40000 ALTER TABLE `sys_schedule_job` DISABLE KEYS */;
INSERT INTO `sys_schedule_job` (`id`, `job_name`, `job_group`, `job_status`, `target_class`, `cron_expression`, `job_desc`) VALUES
	('611583bef1c841e68c268e76ce684eba', '测试', 'SYS', '0', 'com.epoch.platform.job.test.TestJob', '0 */1 * * * ?', NULL);
/*!40000 ALTER TABLE `sys_schedule_job` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_schedule_job_log 结构
CREATE TABLE IF NOT EXISTS `sys_schedule_job_log` (
  `id` varchar(50) NOT NULL,
  `last_success_plan_time` datetime DEFAULT NULL COMMENT '上次成功执行的计划时间',
  `last_success_act_time` datetime DEFAULT NULL COMMENT '上次成功执行的触发时间',
  `create_by` varchar(50) DEFAULT NULL,
  `CREATE_DATE` datetime DEFAULT NULL,
  `UPDATE_BY` varchar(50) DEFAULT NULL,
  `UPDATE_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_schedule_job_log 的数据：~0 rows (大约)
DELETE FROM `sys_schedule_job_log`;
/*!40000 ALTER TABLE `sys_schedule_job_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_schedule_job_log` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_serial_number 结构
CREATE TABLE IF NOT EXISTS `sys_serial_number` (
  `ID` varchar(50) NOT NULL COMMENT '规则编码',
  `RULE_CODE` varchar(50) DEFAULT NULL COMMENT '规则编码',
  `RULE_NAME` varchar(50) DEFAULT NULL COMMENT '规则名称',
  `RULE_TYPE` varchar(50) DEFAULT NULL COMMENT '流水号重置规则0,永远都不重置1,按年重置,2,按月重置,3按日重置（使用率最高，放在最前）,4按小时重置,5按分重置,6按秒重置',
  `CUR_TIME` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '数据库中当前时间',
  `SERIAL_LENGTH` bigint(20) DEFAULT NULL COMMENT '流水号长度',
  `CURRENT_VALUE` varchar(50) DEFAULT NULL COMMENT '当前值',
  `INIT_VALUE` bigint(20) DEFAULT NULL COMMENT '初始值',
  `STEP` bigint(20) DEFAULT NULL COMMENT '步长，增长幅度',
  `SYSTEM_TIME` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '系统当前时间',
  `PREFIX` varchar(50) DEFAULT NULL COMMENT '前缀',
  `SPLIT` varchar(50) DEFAULT NULL COMMENT '分割符',
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_serial_number 的数据：~0 rows (大约)
DELETE FROM `sys_serial_number`;
/*!40000 ALTER TABLE `sys_serial_number` DISABLE KEYS */;
/*!40000 ALTER TABLE `sys_serial_number` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_user 结构
CREATE TABLE IF NOT EXISTS `sys_user` (
  `ID` varchar(50) NOT NULL,
  `ACCOUNT` varchar(50) DEFAULT NULL COMMENT '账号',
  `PASSWORD` varchar(255) DEFAULT NULL COMMENT '密码',
  `USER_NAME` varchar(50) DEFAULT NULL COMMENT '姓名',
  `EMAIL` varchar(50) DEFAULT NULL COMMENT '邮件',
  `PHONE_NUMBER` varchar(50) DEFAULT NULL COMMENT '手机号',
  `BIRTHDAY` date DEFAULT NULL COMMENT '生日',
  `EMPLOYEE_NUMBER` varchar(50) DEFAULT NULL COMMENT '工号',
  `POSITION_LEVEL` varchar(50) DEFAULT NULL COMMENT '职级，在数据字典中定义',
  `TITLE` varchar(50) DEFAULT NULL COMMENT '职位',
  `COMPANY_ID` varchar(50) DEFAULT NULL COMMENT '所属公司',
  `STATUS` varchar(10) DEFAULT NULL COMMENT '状态',
  `CREATE_BY` varchar(50) DEFAULT NULL COMMENT '创建人',
  `CREATE_DATE` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_BY` varchar(50) DEFAULT NULL COMMENT '最后修改人',
  `UPDATE_DATE` datetime DEFAULT NULL COMMENT '最后更新时间',
  `DEPT_ID` varchar(50) DEFAULT NULL COMMENT '部门ID',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_user 的数据：~13 rows (大约)
DELETE FROM `sys_user`;
/*!40000 ALTER TABLE `sys_user` DISABLE KEYS */;
INSERT INTO `sys_user` (`ID`, `ACCOUNT`, `PASSWORD`, `USER_NAME`, `EMAIL`, `PHONE_NUMBER`, `BIRTHDAY`, `EMPLOYEE_NUMBER`, `POSITION_LEVEL`, `TITLE`, `COMPANY_ID`, `STATUS`, `CREATE_BY`, `CREATE_DATE`, `UPDATE_BY`, `UPDATE_DATE`, `DEPT_ID`) VALUES
	('496dbbfb68bf11e8b2c9005056c00001', 'admin', 'e10adc3949ba59abbe56e057f20f883e', '孙大海', '544188838@qq.com', '14678909887', '1992-11-16', '10000', '1', NULL, 'e6c7301df8134b36801864d372ebca03', 'ENABLE', '1', '2017-11-16 20:29:18', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 11:58:55', 'e6c7301df8134b36801864d372ebca03'),
	('5ece6057a0a611e8b453060400ef5315', 'leizimiao', 'e10adc3949ba59abbe56e057f20f883e', '蕾紫喵', '12321321@qq.com', '13567899876', '2018-02-01', '3333', '1', NULL, '0', 'ENABLE', '1', '2018-02-01 16:07:15', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:53:41', '0'),
	('88808167a30a11e8b453060400ef5315', 'leizimiao2', 'e10adc3949ba59abbe56e057f20f883e', '蕾紫喵2', '12321321@qq.com', '23567899876', '2018-02-01', '2670120001', '1', NULL, '0', 'ENABLE', '1', '2018-02-01 16:07:15', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:53:41', '0'),
	('9117e725a30a11e8b453060400ef5315', 'leizimiao3', 'e10adc3949ba59abbe56e057f20f883e', '蕾紫喵3', '12321321@qq.com', '23567899876', '2018-02-01', '2670120002', '1', NULL, '0', 'ENABLE', '1', '2018-02-01 16:07:15', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:53:41', '0'),
	('9895f554a30a11e8b453060400ef5315', 'leizimiao4', 'e10adc3949ba59abbe56e057f20f883e', '蕾紫喵4', '12321321@qq.com', '23567899876', '2018-02-01', '2670120003', '1', NULL, '0', 'ENABLE', '1', '2018-02-01 16:07:15', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:53:41', '0'),
	('a19b7fd4a30a11e8b453060400ef5315', 'leizimiao5', 'e10adc3949ba59abbe56e057f20f883e', '蕾紫喵5', '12321321@qq.com', '23567899876', '2018-02-01', '267010005', '1', NULL, '0', 'ENABLE', '1', '2018-02-01 16:07:15', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:53:40', '0'),
	('aad9241ea30a11e8b453060400ef5315', 'leizimiao6', 'e10adc3949ba59abbe56e057f20f883e', '蕾紫喵6', '12321321@qq.com', '23567899876', '2018-02-01', '2670120006', '1', NULL, 'e6c7301df8134b36801864d372ebca03', 'ENABLE', '1', '2018-02-01 16:07:15', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:54:16', 'e6c7301df8134b36801864d372ebca03'),
	('b19fd876a30a11e8b453060400ef5315', 'leizimiao7', 'e10adc3949ba59abbe56e057f20f883e', '蕾紫喵7', '12321321@qq.com', '23567899876', '2018-02-01', '2670120008', '1', NULL, '0', 'ENABLE', '1', '2018-02-01 16:07:15', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:54:34', '0'),
	('b3b6c8e1aea54e65a87645b689ec6853', 'zhangsanfeng', 'e10adc3949ba59abbe56e057f20f883e', '张三峰', NULL, NULL, NULL, 'P98761', NULL, NULL, NULL, 'ENABLE', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-20 22:03:48', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-20 22:03:48', '0'),
	('cf6c34f868bf11e8b2c9005056c000201', 'test', 'e10adc3949ba59abbe56e057f20f883e', '刘三字', '123456@qq.com', '1234567', '2018-01-17', 'P123456', '1', NULL, 'e6c7301df8134b36801864d372ebca03', 'ENABLE', '1', '2018-01-06 14:01:29', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:53:53', 'e6c7301df8134b36801864d372ebca03'),
	('d963a08868bf11e8b2c9005056c00001', 'ceshi1', 'e10adc3949ba59abbe56e057f20f883e', '李二', '1234@qq.com', '123456', '2018-01-04', 'p12346', '1', NULL, '0', 'ENABLE', '1', '2018-01-21 03:56:54', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:53:41', '0'),
	('dcd9148768bf11e8b2c9005056c00001', 'weiyajie', 'e10adc3949ba59abbe56e057f20f883e', '为要', '1221@qq.com', '12212121', '2018-01-03', 'P3298', '1', '211221', '0', 'ENABLE', '1', '2018-01-21 20:06:30', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:53:41', '0'),
	('dfb06b2668bf11e8b2c9005056c00001', 'leizimiao', 'e10adc3949ba59abbe56e057f20f883e', '蕾紫喵', '12321321@qq.com', '13567899876', '2018-02-01', '2670121697', '1', NULL, '0', 'ENABLE', '1', '2018-02-01 16:07:15', '496dbbfb68bf11e8b2c9005056c00001', '2018-08-19 01:53:41', '0');
/*!40000 ALTER TABLE `sys_user` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_user_org 结构
CREATE TABLE IF NOT EXISTS `sys_user_org` (
  `ID` varchar(50) NOT NULL,
  `USER_ID` varchar(50) DEFAULT NULL,
  `ORG_ID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_user_org 的数据：~4 rows (大约)
DELETE FROM `sys_user_org`;
/*!40000 ALTER TABLE `sys_user_org` DISABLE KEYS */;
INSERT INTO `sys_user_org` (`ID`, `USER_ID`, `ORG_ID`) VALUES
	('31fae3e0e66347b8bcfb62b8b1e2926b', 'aad9241ea30a11e8b453060400ef5315', 'e6c7301df8134b36801864d372ebca03'),
	('41445a54d1534103916275df88a0bbd8', 'cf6c34f868bf11e8b2c9005056c000201', 'e6c7301df8134b36801864d372ebca03'),
	('9b83faddaf6b4d968e0260ade931bb72', '496dbbfb68bf11e8b2c9005056c00001', 'e6c7301df8134b36801864d372ebca03'),
	('a52a45531d4f4579bfbabb5df38846dc', 'b3b6c8e1aea54e65a87645b689ec6853', '0');
/*!40000 ALTER TABLE `sys_user_org` ENABLE KEYS */;

-- 导出  表 epoch-open.sys_user_role 结构
CREATE TABLE IF NOT EXISTS `sys_user_role` (
  `ID` varchar(50) NOT NULL,
  `USER_ID` varchar(50) DEFAULT NULL,
  `ROLE_ID` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 正在导出表  epoch-open.sys_user_role 的数据：~9 rows (大约)
DELETE FROM `sys_user_role`;
/*!40000 ALTER TABLE `sys_user_role` DISABLE KEYS */;
INSERT INTO `sys_user_role` (`ID`, `USER_ID`, `ROLE_ID`) VALUES
	('0aa1036b84f74daf9adf3877260cc1c6', '496dbbfb68bf11e8b2c9005056c00001', 'd37f7ead6dfe11e88310005056c00001'),
	('243c347943774a95a8155e83ffbbccfd', '5ece6057a0a611e8b453060400ef5315', 'd2545eedc30f4ab0ae0025f26d1932ad'),
	('254d6c4f081f470bbe4381bfa32db3dd', 'dcd9148768bf11e8b2c9005056c00001', 'e84ef75c6dfe11e88310005056c00001'),
	('39b0a741f7774e3eaed9f1d127f0a291', '9117e725a30a11e8b453060400ef5315', 'dc0219e36dfe11e88310005056c00001'),
	('3c715c9700e644d79772154711ea04df', 'aad9241ea30a11e8b453060400ef5315', 'd37f7ead6dfe11e88310005056c00001'),
	('7663c51902a84ce698f006d4afcf0ca0', 'dcd9148768bf11e8b2c9005056c00001', 'e45eaf406dfe11e88310005056c00001'),
	('bb4b8c48ab57437cb271735b1422ca3d', 'd963a08868bf11e8b2c9005056c00001', 'dc0219e36dfe11e88310005056c00001'),
	('ebb40a99332448d696414aa62f411a7a', '496dbbfb68bf11e8b2c9005056c00001', 'd2545eedc30f4ab0ae0025f26d1932ad'),
	('f5cb3e0a67f24fba98210f90b0111c41', 'b19fd876a30a11e8b453060400ef5315', 'd37f7ead6dfe11e88310005056c00001');
/*!40000 ALTER TABLE `sys_user_role` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
